"""Script for generating figures in the thesis document"""

from matplotlib import pyplot as plt
import scipy.io

from python import const
from python import utils

def gen_mat():
    bmax_test = utils.load_bmax_test()
    bmax_datum = bmax_test[78]
    img = bmax_datum['img']

    # plt.imshow(img)
    # plt.show()

    # The following for loop was made to determine which segmentation is the bird
    # for i in range(1, 22 + 1):
    #     print('segment:', i)
    #     seg = bmax_datum['seg'][:, :, 0] == i
    #     plt.imshow(seg)
    #     plt.show()

    # in segmentation 0, segment 4 is the bird
    bird_mask = (bmax_datum['seg'][:, :, 0] == 4).nonzero()
    # for y, x in zip(*bird_mask):
    #     img[y, x, :] = (0, 0, 0)
    ys, xs = bird_mask
    img[ys, xs, :] = (0, 0, 0)
    plt.imshow(img)
    plt.show()
    pass

def main():
    gen_mat()

if __name__ == '__main__':
    main()
