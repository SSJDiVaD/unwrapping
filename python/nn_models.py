"""Neural Network models for use with train_cnn and eval_cnn"""

# import math
import sys

# import matplotlib
# matplotlib.use('Agg')
# import hiddenlayer as hl
import numpy as np
import torch
from torch import nn

from python import const


# ACTIVATION = nn.ReLU
ACTIVATION = nn.LeakyReLU

# ABSTRACT UTILS------------------------------------------------------------------------------------

class NnModuleParamCounter(nn.Module):
    """A nn.Module that can print the number of params it has."""
    def print_param_count(self):
        print('model has', sum(p.numel() for p in self.parameters() if p.requires_grad), 'params')


class AbstractNetwork(NnModuleParamCounter):
    """A network whose forward method is simply the run through of self.network"""
    def forward(self, x):
        return self.network(x)


class InputChecker(nn.Module):
    """Assert that the input is
        -32-bit float
        -between 0 and 1
        -has four dimensions, where dimension 1 == 3 (if not in grayscale mode), or == 1 (if in
        grayscale mode)
        -dimensions 2 and 3 have size 8, 32 respectively
    """
    def forward(self, x):
        assert x.dtype == torch.float32
        assert (0 - const.EPS_BIG <= x).all().item()
        assert (x <= 1 + const.EPS_BIG).all().item()
        assert len(x.shape) == 4
        if const.CNN_GRAYSCALE:
            assert x.shape[1] == 1
        else:
            assert x.shape[1] == 3
        assert x.shape[2:] == (8, 32)
        return x


# GENERAL CNNs--------------------------------------------------------------------------------------
class CnnGeneral(AbstractNetwork):
    """A cnn that consists of an InputChecker, ConvBlockNs, followed by FcLayers, followed by output
    neurons. One can specify:
        -number of ConvBlockNs, and the n of each block
        -number of filters for the first conv layer. The number of filters in consecutive subsequent
         conv blocks will double, as it does with VGG.
        -number of FcLayers, and the number of neurons of each layer. Every layer will have the same
         number.
        -number of output neurons
    """
    def __init__(self, conv_blocks, filters_1, fc_layers, output_neurons, final_activation=None):
        """
        :param conv_blocks: an iterable. The length will be the number of ConvBlockNs. Each item
        will be the n of the corresponding ConvBlock2dN.
        :param filters_1:
        :param fc_layers: an iterable. The length will be the number of FcLayers. Each item will be
        the number of neurons of the corresponding FcLayer.
        :param output_neurons:
        :param final_activation: None (default) or an instance of a subclass of nn.Module which will
        be used as the final activation function
        """
        super(CnnGeneral, self).__init__()
        layers = [InputChecker()]

        # handle conv layers
        if const.CNN_GRAYSCALE:
            num_filters_out = 1  # number of channels of the input image
        else:
            num_filters_out = 3  # number of channels of the input image
        i = -1
        for i, num_convs in enumerate(conv_blocks):
            num_filters_in = num_filters_out
            if i == 0:
                num_filters_out = filters_1
            else:
                num_filters_out *= 2
            layers.append(ConvBlock2dN(num_convs, num_filters_in, num_filters_out,
                                       dropout_p=const.CNN_CONV_DROPOUT_P))
        if i < 0:
            print('warning: no convolutional blocks processed. Going straight from input to fc '
                  'layers')
        fc_neurons_out = (np.prod(const.UNWRAP_SHAPE, dtype=np.int) * num_filters_out //
                          (4 ** (i + 1)))
        print(fc_neurons_out, 'neurons before first FC layer')

        # handle fc layers
        layers.append(FlattenFeatureMap())
        i = -1
        for i, num_neurons in enumerate(fc_layers):
            fc_neurons_in = fc_neurons_out
            fc_neurons_out = num_neurons
            layers.append(FcLayer(fc_neurons_in, fc_neurons_out, const.CNN_FC_DROPOUT_P))
        if i < 0:
            print('warning: no fc layers processed. Going straight from last feature map to the '
                  'output linear layer.')

        # output layer
        layers.append(nn.Linear(fc_neurons_out, output_neurons))
        if final_activation is not None:
            layers.append(final_activation)
        self.network = nn.Sequential(*layers)
        self.print_param_count()


class ConvBlock2dN(AbstractNetwork):
    """A bunch of n-1 Conv2dActRegs and then one Conv2dActPoolReg. The first layer takes
    num_channels_in. All layers have the same number of output channels/filters, which is
    num_channels_out. n >= 1 is checked at initialization.
    """
    def __init__(self, n, num_channels_in, num_channels_out, dropout_p=None):
        super(ConvBlock2dN, self).__init__()
        assert n >= 1
        if n == 1:
            layers = [Conv2dActPoolReg(num_channels_in, num_channels_out, dropout_p)]
        else:
            layers = ([Conv2dActReg(num_channels_in, num_channels_out, dropout_p)] +
                      [Conv2dActReg(num_channels_out, num_channels_out, dropout_p)
                       for _ in range(n - 2)] +
                      [Conv2dActPoolReg(num_channels_out, num_channels_out, dropout_p)])
        self.network = nn.Sequential(*layers)


class Conv2dActReg(AbstractNetwork):
    """Conv2d with wrap padding, relu, bnorm, dropout"""
    def __init__(self, num_channels_in, num_channels_out, dropout_p=None, kernel_width=3):
        super(Conv2dActReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        padding_w = (kernel_width - 1) // 2
        self.network = nn.Sequential(
            Conv2dWrapPaddingHorizontal(num_channels_in, num_channels_out, kernel_size=(3, kernel_width),
                                        stride=1, padding=(1, padding_w)),
            ACTIVATION(),
            nn.BatchNorm2d(num_channels_out),
            nn.Dropout2d(p=dropout_p),
        )


class Conv2dActPoolReg(AbstractNetwork):
    """Conv with wrap padding, relu, max pool, bnorm, dropout"""
    def __init__(self, num_channels_in, num_channels_out, dropout_p=None, kernel_width=3):
        super(Conv2dActPoolReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        padding_w = (kernel_width - 1) // 2
        self.network = nn.Sequential(
            Conv2dWrapPaddingHorizontal(num_channels_in, num_channels_out, kernel_size=(3, kernel_width),
                                        stride=1, padding=(1, padding_w)),
            ACTIVATION(),
            nn.MaxPool2d(2),
            nn.BatchNorm2d(num_channels_out),
            nn.Dropout2d(p=dropout_p),
        )


class Conv2dWrapPaddingHorizontal(nn.Module):
    """A convolution where the horizontal padding is wrap padding instead of zero padding. The
    vertical is still zero padding.
    The horizontal padding must not be larger than half the feature map size. This is checked at
    run-time.
    """
    def __init__(self, num_channels_in, num_channels_out, kernel_size, stride=1, padding=(0, 0)):
        super(Conv2dWrapPaddingHorizontal, self).__init__()
        if isinstance(padding, tuple):
            assert len(padding) == 2
            self.padding_h, self.padding_w = padding
        elif isinstance(padding, int):
            self.padding_h = self.padding_w = padding
        else:
            assert False
        self.conv = nn.Conv2d(num_channels_in, num_channels_out, kernel_size, stride,
                              (self.padding_h, 0))

    def forward(self, x):
        """Apply a wrap-around padding to the horizontal dimension and then run the convolution"""
        # Taken from: https://github.com/pytorch/pytorch/issues/3858
        batch_size, c, h, w = x.shape
        assert self.padding_w <= w // 2

        # # old strategy: expand x horizontally by repeating it three times and then crop only the
        # # part we need
        # x = x.repeat(1, 1, 1, 3)[:, :, :, (w - self.padding_w):(2 * w + self.padding_w)]

        # new strategy: expand x horizontally by repeating it twice. This will lead to a sideways
        # shift (ie. the original features won't be centered in the new map anymore), but I think
        # that's ok since the feature map wraps around anyway.
        x = x.repeat(1, 1, 1, 2)[:, :, :, 0:(w + 2 * self.padding_w)]
        assert x.shape == (batch_size, c, h, w + 2 * self.padding_w)
        # print(x.is_cuda)
        # print(x.device)
        # print(x.dtype)
        # print(x.requires_grad)
        x = self.conv(x)
        return x


class FlattenFeatureMap(nn.Module):
    """Take a minibatch of 3-dimensional inputs (n x c x w x h) and flatten volume into a vector"""
    def forward(self, x):
        assert len(x.shape) == 4  # minibatches of 3 dimensional data
        return x.view(x.size(0), -1)


class FcLayer(AbstractNetwork):
    """Linear, relu, bnorm, dropout"""
    def __init__(self, neurons_in, neurons_out, dropout_p=None):
        super(FcLayer, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_FC_DROPOUT_P
        self.network = nn.Sequential(
            nn.Linear(neurons_in, neurons_out),
            ACTIVATION(),
            nn.BatchNorm1d(neurons_out),
            nn.Dropout(p=dropout_p),
        )


# NETWORKS WHICH CONV ALL THE WAY DOWN TO 1x1-------------------------------------------------------
class FeatureMapToFeatureVector(nn.Module):
    """Take a minibatch of 3-dimensional feature maps (n x c x h x w) where h == 1 and convert it
    into a minibatch of 2-dimensional feature vectors (n x c x w) to prepare it for 1-dimensional
    convolutions
    """
    def forward(self, x):
        n, c, h, w = x.shape
        assert h == 1
        return x.view(n, c, w)


class Conv1dWrapPadding(nn.Module):
    """A 1-dimensional convolution a la nn.module.Conv1d, with wrap-around padding"""
    def __init__(self, num_channels_in, num_channels_out, kernel_size, stride=1, padding=0):
        super(Conv1dWrapPadding, self).__init__()
        self.padding = padding
        self.conv = nn.Conv1d(num_channels_in, num_channels_out, kernel_size, stride, 0)

    def forward(self, x):
        """Apply a wrap-around padding to the horizontal dimension and then run the convolution"""
        batch_size, c, w = x.shape
        assert self.padding <= w // 2
        x = x.repeat(1, 1, 2)[:, :, 0:(w + 2 * self.padding)]
        assert x.shape == (batch_size, c, w + 2 * self.padding)
        x = self.conv(x)
        return x


class Conv1dActReg(AbstractNetwork):
    """Conv1d with wrap padding, relu, bnorm, dropout"""
    def __init__(self, num_channels_in, num_channels_out, dropout_p=None, kernel_size=3):
        super(Conv1dActReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        padding = (kernel_size - 1) // 2
        self.network = nn.Sequential(
            Conv1dWrapPadding(num_channels_in, num_channels_out, kernel_size=kernel_size, stride=1,
                              padding=padding),
            ACTIVATION(),
            nn.BatchNorm1d(num_channels_out),
            nn.Dropout(p=dropout_p),
        )


class Conv1dActPoolReg(AbstractNetwork):
    """Conv with wrap padding, relu, max pool, bnorm, dropout"""
    def __init__(self, num_channels_in, num_channels_out, dropout_p=None, kernel_size=3):
        super(Conv1dActPoolReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        padding = (kernel_size - 1) // 2
        self.network = nn.Sequential(
            Conv1dWrapPadding(num_channels_in, num_channels_out, kernel_size=kernel_size, stride=1,
                              padding=padding),
            ACTIVATION(),
            nn.MaxPool1d(2),
            nn.BatchNorm1d(num_channels_out),
            nn.Dropout(p=dropout_p),
        )
        

class ConvBlock1dN(AbstractNetwork):
    """A bunch of n-1 Conv1dActRegs and then one Conv1dActPoolReg. The first layer takes
    num_channels_in. All layers have the same number of output channels/filters, which is
    num_channels_out. n >= 1 is checked at initialization.
    """
    def __init__(self, n, num_channels_in, num_channels_out, dropout_p=None):
        super(ConvBlock1dN, self).__init__()
        assert n >= 1
        if n == 1:
            layers = [Conv1dActPoolReg(num_channels_in, num_channels_out, dropout_p)]
        else:
            layers = ([Conv1dActReg(num_channels_in, num_channels_out, dropout_p)] +
                      [Conv1dActReg(num_channels_out, num_channels_out, dropout_p)
                       for _ in range(n - 2)] +
                      [Conv1dActPoolReg(num_channels_out, num_channels_out, dropout_p)])
        self.network = nn.Sequential(*layers)


class FeatureVectorToFeatureScalar(nn.Module):
    """Squeezes a vector with one element to a scalar by getting rid of the unnecessary dimension.
    """
    def forward(self, x):
        """Takes a minibatch of 2-dimensional feature maps (n x c x w) where w == 1 and converts it
        into a minibatch of 1-dimensional feature scalars (n x c) to prepare it for linear layers
        """
        n, c, w = x.shape
        assert w == 1
        return x.view(n, c)


class CnnHorizontalInvariant(AbstractNetwork):
    """A CNN which is invariant to horizontal shifts (assuming wraparound) in the input image"""
    def __init__(self, conv_2d_blocks, conv_1d_blocks, filters_1, fc_layers, output_neurons,
                 final_activation=None):
        """
        :param conv_2d_blocks: an iterable. The length will be the number of ConvBlock2dNs. Each
        item will be the n of the corresponding ConvBlock2dN.
        :param conv_1d_blocks: same as conv_2d_blocks but for the ConvBlock1dNs after the 2d ones.
        :param filters_1:
        :param fc_layers: an iterable. The length will be the number of FcLayers. Each item will be
        the number of neurons of the corresponding FcLayer.
        :param output_neurons:
        :param final_activation: None (default) or an instance of a subclass of nn.Module which will
        be used as the final activation function
        """
        super(CnnHorizontalInvariant, self).__init__()
        layers = [InputChecker()]

        # handle conv 2d layers
        if const.CNN_GRAYSCALE:
            num_filters_out = 1  # number of channels of the input image
        else:
            num_filters_out = 3  # number of channels of the input image
        i = -1
        for i, num_convs in enumerate(conv_2d_blocks):
            num_filters_in = num_filters_out
            if i == 0:
                num_filters_out = filters_1
            else:
                num_filters_out *= 2
            layers.append(ConvBlock2dN(num_convs, num_filters_in, num_filters_out,
                                       const.CNN_CONV_DROPOUT_P))
        if i < 0:
            print('error: no convolutional 2d blocks processed')
            assert False

        # handle conv 1d layers
        layers.append(FeatureMapToFeatureVector())
        conv1d_processed = False
        for num_convs in conv_1d_blocks:
            num_filters_in = num_filters_out
            num_filters_out *= 2
            layers.append(ConvBlock1dN(num_convs, num_filters_in, num_filters_out,
                                       const.CNN_CONV_DROPOUT_P))
            conv1d_processed = True
        if not conv1d_processed:
            print('error: no convolutional 1d blocks processed')
            assert False

        # handle fc layers
        layers.append(FeatureVectorToFeatureScalar())
        fc_neurons_out = num_filters_out
        print(fc_neurons_out, 'neurons before first FC layer')
        i = -1
        for i, num_convs in enumerate(fc_layers):
            fc_neurons_in = fc_neurons_out
            fc_neurons_out = num_convs
            layers.append(FcLayer(fc_neurons_in, fc_neurons_out, const.CNN_FC_DROPOUT_P))
        if i < 0:
            print('warning: no fc layers processed. Going straight from last feature map to the '
                  'output linear layer.')

        # output layer
        layers.append(nn.Linear(fc_neurons_out, output_neurons))
        if final_activation is not None:
            layers.append(final_activation)
        self.network = nn.Sequential(*layers)
        self.print_param_count()


# Network-in-Network--------------------------------------------------------------------------------
class Conv2d1by1ActReg(AbstractNetwork):
    """A 1x1 convolution with ReLU, bnorm, dropout"""
    def __init__(self, channels_in, channels_out, dropout_p=None):
        super(Conv2d1by1ActReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        self.network = nn.Sequential(
            nn.Conv2d(channels_in, channels_out, 1),
            ACTIVATION(),
            nn.BatchNorm2d(channels_out),
            nn.Dropout2d(dropout_p),
        )


class Conv2d1by1ActPoolReg(AbstractNetwork):
    """A 1x1 convolution with ReLU, pool, bnorm, dropout"""
    def __init__(self, channels_in, channels_out, dropout_p=None):
        super(Conv2d1by1ActPoolReg, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        self.network = nn.Sequential(
            nn.Conv2d(channels_in, channels_out, 1),
            ACTIVATION(),
            nn.MaxPool2d(2),
            nn.BatchNorm2d(channels_out),
            nn.Dropout2d(dropout_p),
        )


class MlpConvPool(AbstractNetwork):
    """An MlpConvPool layer as described by the NIN paper"""
    def __init__(self, channels_in, channels_out):
        super(MlpConvPool, self).__init__()
        self.network = nn.Sequential(
            Conv2dActReg(channels_in, channels_out, const.CNN_CONV_DROPOUT_P),
            Conv2d1by1ActReg(channels_out, channels_out, const.CNN_CONV_DROPOUT_P),
            Conv2d1by1ActPoolReg(channels_out, channels_out, const.CNN_CONV_DROPOUT_P),
        )


class MlpConvNoPool(AbstractNetwork):
    """An MlpConvPool layer without pooling"""
    def __init__(self, channels_in, channels_out):
        super(MlpConvNoPool, self).__init__()
        self.network = nn.Sequential(
            Conv2dActReg(channels_in, channels_out, const.CNN_CONV_DROPOUT_P),
            Conv2d1by1ActReg(channels_out, channels_out, const.CNN_CONV_DROPOUT_P),
            Conv2d1by1ActReg(channels_out, channels_out, const.CNN_CONV_DROPOUT_P),
        )


class NinHorizontalInvariant(AbstractNetwork):
    """A Network-in-network"""
    def __init__(self, num_convs, filters_1, extra_mlpconvs=0):
        """
        :param num_convs: the number of convolutions
        :param filters_1: the number of filters on the first conv layer
        """
        super(NinHorizontalInvariant, self).__init__()
        if const.CNN_GRAYSCALE:
            filters_in = 1  # number of channels of the input image
        else:
            filters_in = 3  # number of channels of the input image
        filters_out = filters_1
        layers = [MlpConvPool(filters_in, filters_out)]
        for i in range(num_convs):
            filters_in = filters_out
            filters_out *= 2
            if extra_mlpconvs != 0:
                for i in range(extra_mlpconvs):
                    layers.append(MlpConvNoPool(filters_in, filters_out))
                layers.append(MlpConvPool(filters_out, filters_out))
            else:
                layers.append(MlpConvPool(filters_in, filters_out))

        # the final layer
        filters_in, filters_out = filters_out, 3
        layers.append(nn.Conv2d(filters_in, filters_out, 1))
        layers.append(nn.AdaptiveAvgPool2d(1))
        layers.append(FlattenFeatureMap())
        self.network = nn.Sequential(*layers)
        self.print_param_count()


# Horizontally invariant network, version 2---------------------------------------------------------
class CnnHorizontalInvariantV2(AbstractNetwork):
    def __init__(self, conv_2d_blocks, conv_1d_block, filters_1, fc_layers, output_neurons,
                 final_activation=None):
        """
        :param conv_2d_blocks: an iterable. The length will be the number of ConvBlock2dNV2s. Each
        item will be the n of the corresponding ConvBlock2dNV2.
        :param conv_1d_block: the n of the ConvBlock1dNV2
        :param filters_1: number of input filters
        :param fc_layers: an iterable. The length will be the number of FcLayers. Each item will be
        the number of neurons of the corresponding FcLayer.
        :param output_neurons: number of output neurons. If doing 3-class classification, this will
        be 3.
        :param final_activation: None (default) or an instance of a subclass of nn.Module which will
        be used as the final activation function
        """
        super(CnnHorizontalInvariantV2, self).__init__()
        layers = [InputChecker()]

        # handle conv 2d layers
        if const.CNN_GRAYSCALE:
            num_filters_out = 1  # number of channels of the input image
        else:
            num_filters_out = 3  # number of channels of the input image
        i = -1
        for i, num_convs in enumerate(conv_2d_blocks):
            num_filters_in = num_filters_out
            if i == 0:
                num_filters_out = filters_1
            else:
                num_filters_out *= 2
            layers.append(ConvBlock2dNV2(num_convs, num_filters_in, num_filters_out,
                                         const.CNN_CONV_DROPOUT_P))
        if i < 0:
            print('error: no convolutional 2d blocks processed')
            assert False

        # handle conv 1d layers
        layers.append(FeatureMapToFeatureVector())
        # conv1d_processed = False
        # for num_convs in conv_1d_blocks:
        #     num_filters_in = num_filters_out
        #     num_filters_out *= 2
        #     layers.append(ConvBlock1dN(num_convs, num_filters_in, num_filters_out,
        #                                const.CNN_CONV_DROPOUT_P))
        #     conv1d_processed = True
        # if not conv1d_processed:
        #     print('error: no convolutional 1d blocks processed')
        #     assert False
        assert conv_1d_block >= 1
        layers.append(ConvBlock1dNV2(conv_1d_block, num_filters_out, num_filters_out,
                                     const.CNN_CONV_DROPOUT_P))

        # handle fc layers
        layers.append(nn.AdaptiveMaxPool1d(1))
        layers.append(FeatureVectorToFeatureScalar())
        fc_neurons_out = num_filters_out
        print(fc_neurons_out, 'neurons before first FC layer')
        i = -1
        for i, num_convs in enumerate(fc_layers):
            fc_neurons_in = fc_neurons_out
            fc_neurons_out = num_convs
            layers.append(FcLayer(fc_neurons_in, fc_neurons_out, const.CNN_FC_DROPOUT_P))
        if i < 0:
            print('warning: no fc layers processed. Going straight from last feature map to the '
                  'output linear layer.')

        # output layer
        layers.append(nn.Linear(fc_neurons_out, output_neurons))
        if final_activation is not None:
            layers.append(final_activation)
        self.network = nn.Sequential(*layers)
        self.print_param_count()


class ConvBlock2dNV2(AbstractNetwork):
    """A bunch of n-1 Conv2dActRegs and then one Conv2dActPoolRegV2. The first layer takes
    num_channels_in. All layers have the same number of output channels/filters, which is
    num_channels_out. n >= 1 is checked at initialization.
    """
    def __init__(self, n, num_channels_in, num_channels_out, dropout_p=None, kernel_width=3):
        super(ConvBlock2dNV2, self).__init__()
        assert n >= 1
        if n == 1:
            layers = [Conv2dActPoolRegV2(num_channels_in, num_channels_out, dropout_p,
                                         kernel_width)]
        else:
            layers = ([Conv2dActReg(num_channels_in, num_channels_out, dropout_p, kernel_width)] +
                      [Conv2dActReg(num_channels_out, num_channels_out, dropout_p, kernel_width)
                       for _ in range(n - 2)] +
                      [Conv2dActPoolRegV2(num_channels_out, num_channels_out, dropout_p,
                                          kernel_width)])
        self.network = nn.Sequential(*layers)


class Conv2dActPoolRegV2(AbstractNetwork):
    """Conv with wrap padding, relu, max pool, bnorm, dropout. However, the max pooling only shrinks
    the feature map in the vertical direction and NOT the horizontal one."""
    def __init__(self, num_channels_in, num_channels_out, dropout_p=None, kernel_width=3):
        super(Conv2dActPoolRegV2, self).__init__()
        if dropout_p is None:
            dropout_p = const.CNN_CONV_DROPOUT_P
        padding_w = (kernel_width - 1) // 2
        self.network = nn.Sequential(
            Conv2dWrapPaddingHorizontal(
                num_channels_in, num_channels_out, kernel_size=(3, kernel_width), stride=1,
                padding=(1, padding_w)),
            ACTIVATION(),
            nn.MaxPool2d((2, 1)),
            nn.BatchNorm2d(num_channels_out),
            nn.Dropout2d(p=dropout_p),
        )


class ConvBlock1dNV2(AbstractNetwork):
    """A bunch of n Conv1dActRegs. The first layer takes num_channels_in. All layers have the same
    number of output channels/filters, which is num_channels_out.

    Note that we're not doing pooling. This is because we want to keep our network horizontally
    invariant.

    n >= 1 is checked at initialization.
    """
    def __init__(self, n, num_channels_in, num_channels_out, dropout_p=None, kernel_size=3):
        super(ConvBlock1dNV2, self).__init__()
        assert n >= 1
        layers = ([Conv1dActReg(num_channels_in, num_channels_out, dropout_p, kernel_size)] +
                  [Conv1dActReg(num_channels_out, num_channels_out, dropout_p, kernel_size)
                   for _ in range(n - 1)])
        self.network = nn.Sequential(*layers)


# Version 3! ---------------------------------------------------------------------------------------
# Designed for 8*32 unwrappings. Doesn't perform any 1d convolutions.
class CnnHorizontalInvariantV3(AbstractNetwork):
    def __init__(self, conv_blocks, filters_1, fc_layers, output_neurons, final_activation=None):
        """
        :param conv_blocks: an iterable. The length will be the number of ConvBlock2dNV2s. Each
        item will be the n of the corresponding ConvBlock2dNV2. Length must be <=3
        :param filters_1: number of input filters
        :param fc_layers: an iterable. The length will be the number of FcLayers. Each item will be
        the number of neurons of the corresponding FcLayer.
        :param output_neurons: number of output neurons. If doing 3-class classification, this will
        be 3.
        :param final_activation: None (default) or an instance of a subclass of nn.Module which will
        be used as the final activation function
        """
        super(CnnHorizontalInvariantV3, self).__init__()
        layers = [InputChecker()]

        # handle conv 2d layers
        if const.CNN_GRAYSCALE:
            num_filters_out = 1  # number of channels of the input image
        else:
            num_filters_out = 3  # number of channels of the input image
        i = -1
        for i, num_convs in enumerate(conv_blocks):
            num_filters_in = num_filters_out
            if i == 0:
                num_filters_out = filters_1
            else:
                num_filters_out *= 2
            layers.append(ConvBlock2dNV2(num_convs, num_filters_in, num_filters_out,
                                         const.CNN_CONV_DROPOUT_P))
        if i < 0:
            raise ValueError('error: no convolutional 2d blocks processed')
        elif i > 2:
            raise ValueError('Error: too many conv 2d blocks. Must be <=3')

        # handle fc layers
        layers.append(nn.AdaptiveMaxPool2d((1, 1)))
        layers.append(FeatureMapToFeatureScalar())
        fc_neurons_out = num_filters_out
        print(fc_neurons_out, 'neurons before first FC layer')
        i = -1
        for i, num_convs in enumerate(fc_layers):
            fc_neurons_in = fc_neurons_out
            fc_neurons_out = num_convs
            layers.append(FcLayer(fc_neurons_in, fc_neurons_out, const.CNN_FC_DROPOUT_P))
        if i < 0:
            print('warning: no fc layers processed. Going straight from last feature map to the '
                  'output linear layer.')

        # output layer
        layers.append(nn.Linear(fc_neurons_out, output_neurons))
        if final_activation is not None:
            layers.append(final_activation)
        self.network = nn.Sequential(*layers)
        self.print_param_count()


class FeatureMapToFeatureScalar(nn.Module):
    """Take a minibatch of 3-dimensional feature maps (n x c x h x w) where h == w == 1 and convert
    it into a minibatch of 1-dimensional feature scalars (n x c) to prepare it for fc layers
    """
    def forward(self, x):
        n, c, h, w = x.shape
        assert h == w == 1
        return x.view(n, c)


# LOSSES--------------------------------------------------------------------------------------------
# class WeightedBceLogitsLoss(nn.Module):
#     """A version of BCEWithLogitsLoss where we can assign weights on-the-fly instead of fixed."""
#     def __init__(self):
#         super(WeightedBceLogitsLoss, self).__init__()
#         self._loss = nn.BCEWithLogitsLoss(reduce=False)
#
#     def forward(self, preds, labels, weights):
#         """weights is a torch Tensor vector with the same length as outputs"""
#         batch_size = labels.shape[0]
#         assert preds.shape == weights.shape == labels.shape == (batch_size, 1)
#         losses = self._loss(preds, labels)
#         assert losses.shape == (batch_size, 1)
#         return torch.dot(torch.squeeze(losses, dim=1), torch.squeeze(weights, dim=1))
#
#
# class BalancedBceLogitsLoss(nn.Module):
#     """A version of BCEWithLogitsLoss where the minibatches will probably be unbalanced, so each training example's
#     update is done inversely proportionately to the proportion of each label. Only supports two-class atm,
#     with labels in set {0, 1}."""
#     def __init__(self):
#         super(BalancedBceLogitsLoss, self).__init__()
#         self._loss = nn.BCEWithLogitsLoss(reduce=False)
#
#     def forward(self, preds, labels):
#         """Same params as torch.nn.BCEWithLogitsLoss"""
#         batch_size = labels.shape[0]
#         assert preds.shape == labels.shape == (batch_size, 1)
#         true_count = torch.sum(labels).item()
#         false_count = torch.sum(1 - labels).item()
#         assert true_count + false_count == batch_size
#         assert batch_size != 0
#         if false_count > 0 and true_count > 0:
#             weights = np.zeros((batch_size,), dtype=np.float32)
#             weights[np.argwhere(labels)] = false_count / true_count / batch_size
#             weights[np.argwhere(1 - labels)] = true_count / false_count / batch_size
#         else:
#             # in semi-rare cases, the labels will all be of one type. In these cases, assign uniform weight
#             weights = np.ones((batch_size,), dtype=np.float32) / batch_size
#         assert np.all(weights != 0)
#         sum_weights = np.sum(weights)
#         # print('weights sum', sum_weights)
#         assert math.isclose(sum_weights, 1, rel_tol=0.05)  # weights sum to 1
#         weights = torch.from_numpy(weights)
#         weights.requires_grad_(False)
#         if const.GLOBAL_USE_CUDA:
#             weights = weights.cuda()
#         losses = self._loss(preds, labels)
#         assert losses.shape == (batch_size, 1)
#         return torch.dot(torch.squeeze(losses, dim=1), weights)


class ConfidencePenaltyCrossEntropyLoss(nn.Module):
    """A version of cross entropy loss with confidence penalty

    https://arxiv.org/pdf/1701.06548.pdf
    """
    def __init__(self, beta=0.1, reduction='sum'):
        super(ConfidencePenaltyCrossEntropyLoss, self).__init__()
        self._loss = nn.CrossEntropyLoss(reduction=reduction)
        self._log_softmax = nn.LogSoftmax(dim=1)
        self._softmax = nn.Softmax(dim=1)
        self.beta = beta
        self.reduction = reduction

    def forward(self, preds, labels):
        NUM_CHANNELS = 3  # 3 classes
        batch_size = labels.shape[0]
        assert preds.shape == (batch_size, NUM_CHANNELS)
        assert labels.shape == (batch_size,)
        base_term = self._loss(preds, labels)
        likelihoods, log_likelihoods = self._softmax(preds), self._log_softmax(preds)
        assert likelihoods.shape == log_likelihoods.shape == (batch_size, NUM_CHANNELS)
        minus_hs = torch.sum(torch.mul(likelihoods, log_likelihoods), dim=1)
        # We're handling the negative of H in order to eliminate one operation and theoretically
        # speed things up a tiny bit
        assert minus_hs.shape == (batch_size,)
        if self.reduction == 'sum':
            minus_hs = torch.sum(minus_hs)
        elif self.reduction == 'mean':
            minus_hs = torch.mean(minus_hs)
        else:
            minus_hs = minus_hs
        return base_term + self.beta * minus_hs


def get_model(model_name=None, no_gpu=False):
    """Depending on what the settings in const are, return a new instance of the appropriate
    model. It will be Cuda'd if const.GLOBAL_USE_CUDA"""
    models = {
        # Three-class models----------------------------------------------------
        'gen_c4_f16_fc512': lambda: CnnGeneral([4, 4, 4], 16, [512, 256], 3),
        'gen_c4_f32_fc512': lambda: CnnGeneral([4, 4, 4], 32, [512, 256], 3),
        'gen_c4_f32_fc256': lambda: CnnGeneral([4, 4, 4], 32, [256, 128], 3),

        # horizontally invariant models
        'invariant_conv1_filter8_fc128': lambda: CnnHorizontalInvariant(
            [1, 1, 1, 1], [1, 1], 8, [128, 64], 3),
        # 'invariant_conv3_filter16_fc256': lambda: CnnHorizontalInvariant(
        #     [3, 3, 3, 3], [3, 3], 16, [256, 128], 3),
        'invariant_conv3_filter32_fc256': lambda: CnnHorizontalInvariant(
            [3, 3, 3], [3, 3], 32, [256, 128], 3),
        'invariant_conv3_filter32_fc512': lambda: CnnHorizontalInvariant(
            [3, 3, 3], [3, 3], 32, [512, 256], 3),

        # Horizontally invariant, v3
        'iv3_c1_f8_fc64': lambda: CnnHorizontalInvariantV3(
            [1, 1, 1], 8, [64, 32], 3),
        'iv3_c3_f16_fc256': lambda: CnnHorizontalInvariantV3(
            [3, 3, 3], 16, [256, 128], 3),
        'iv3_c3_f32_fc256': lambda: CnnHorizontalInvariantV3(
            [3, 3, 3], 32, [256, 128], 3),
        'iv3_c4_f16_fc256': lambda: CnnHorizontalInvariantV3(
            [4, 4, 4], 16, [256, 128], 3),
        'iv3_c4_f32_fc256': lambda: CnnHorizontalInvariantV3(
            [4, 4, 4], 32, [256, 128], 3),
        'iv3_c4_f32_fc512': lambda: CnnHorizontalInvariantV3(
            [4, 4, 4], 32, [512, 256], 3),
        'iv3_c4_f64_fc256': lambda: CnnHorizontalInvariantV3(
            [4, 4, 4], 64, [256, 128], 3),
        'iv3_c5_f32_fc256': lambda: CnnHorizontalInvariantV3(
            [5, 5, 5], 32, [256, 128], 3),
    }
    if model_name is None:
        model_name = const.GLOBAL_MODEL
    print('grabbing model:', model_name)
    try:
        cnn = models[model_name]()
    except KeyError as exception:
        print('unknown NN topology:', model_name, '; need to provide --model',
              file=sys.stderr)
        raise exception
    # cnn.eval()
    # if const.CNN_GRAYSCALE:
    #     torch_zeros = (1, 1)
    # else:
    #     torch_zeros = (1, 3)
    # torch_zeros = torch.zeros(torch_zeros + const.UNWRAP_SHAPE)
    # graph = hl.build_graph(cnn, torch_zeros)
    # FILENAME = 'data/model_{0}.png'.format(model_name)
    # graph.save(FILENAME, 'png')
    # print('saved picture of model to {0}'.format(FILENAME))
    # cnn.train()
    if const.GLOBAL_USE_CUDA and not no_gpu:
        cnn = cnn.cuda()
    return cnn
