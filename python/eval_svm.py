"""Stuff for evaluating the performance of the CNN trained by train_cnn."""

import datetime
# import math
# from matplotlib import pyplot as plt
import multiprocessing
import os
from pathlib import Path
import queue
import shutil
import socket
import time

import imageio
import numpy as np
import scipy.io

from python import const
from python import eval_convnet
from python import utils


def collect_hogs_radius(img, radius, scaler):
    """Collects and returns all hogs of the image at the given radius.

    Applies scaling to the hogs using the given scaler

    Args:
        img: h * w * c ndarray representing the image we're trying to unwrap
        radius: the radius at which we'd like to unwrap.
        scaler: the second element returned by utils.load_svm_pretrained

    Returns: a tuple of ndarrays, each with the same number of elements n:
        -hogs: n * hog_length
        -ys: the y coordinate that each corresponding unwrap came from
        -xs: ditto, for x
    """
    unwraps, ys, xs = eval_convnet.collect_unwraps_radius(img, radius)
    del img, radius
    hogs = utils.unwraps_to_hogs(unwraps)
    del unwraps
    hogs = scaler.transform(hogs)
    return hogs, ys, xs


def eval_svm_radius(img, radius, svm_scaler):
    """Evaluate the convnet at the given radius on the given image, assigning the output to
    outputs_all_radiuses

    Args:
        img: h * w * 3 ndarray denoting an image
        radius: the radius over which to operate
        svm_scaler: the svm and scaler loaded by utils.load_svm_pretrained

    Returns: a tuple of
        -the responses as a h * w * 3 ndarray
        -the smoothed responses as a h * w * 3 ndarray
    """
    h, w, _ = img.shape
    svm, scaler = svm_scaler
    try:
        hogs, ys, xs = collect_hogs_radius(img, radius, scaler)
    except ValueError:
        return np.zeros((h, w, 3), dtype=np.float32), np.zeros((h, w, 3), dtype=np.float32)
    del svm_scaler, scaler
    responses = utils.svm_eval_likelihoods(svm, hogs)
    del svm, hogs
    collected = eval_convnet.collect_responses_radius(responses, ys, xs, (h, w))
    del responses, ys, xs
    smoothed = eval_convnet.post_process_radius(collected, const.EVAL_SVM_SMOOTH_SIGMA)
    # plt.imshow(smoothed)
    # plt.show()
    assert collected.shape == smoothed.shape == (h, w, 3)
    return collected, smoothed


def eval_svm_radius_threading(img, radius, radius_index, svm_scaler, outputs_queue):
    """Runs eval_svm_radius and then outputs the radius index and responses and smoothed responses
    to the given outputs queue as a tuple.

    Returns:
        None
    """
    responses, responses_processed = eval_svm_radius(img, radius, svm_scaler)
    outputs_queue.put((radius_index, responses, responses_processed))


def max_process_count():
    hostname = socket.gethostname()
    if hostname == 'OhDear':
        return 8
    elif hostname == 'viscomp4':
        return 32
    elif hostname == 'viscomp3':
        return 32
    else:
        process_count = multiprocessing.cpu_count()
        print('running {0} processes'.format(process_count))
        return process_count


def eval_svm_image_threading(img, radius_range, svm_scaler):
    DELAY_BETWEEN_CHECKS = 1.  # seconds in between checking threads for completion
    h, w, _ = img.shape
    max_threads = max_process_count()
    max_used_radius_index = -1
    responses_all_radiuses = np.zeros((len(radius_range), h, w, 3), dtype=np.float32)
    responses_all_radiuses_smoothed = np.zeros((len(radius_range), h, w, 3), dtype=np.float32)
    processes = [None for _ in range(max_threads)]
    outputs_queue = multiprocessing.Queue()
    all_done = False
    while not all_done:
        all_done = True
        work_done = False
        for i, process in enumerate(processes):
            if ((process is None or not process.is_alive())
                    and max_used_radius_index < len(radius_range) - 1):
                # If the thread is no longer working, we should kill it and create a new thread in
                # its place
                # We select the first available radius
                max_used_radius_index += 1
                radius_index = max_used_radius_index
                radius = radius_range[radius_index]
                # print('working on radius:', radius)
                process = multiprocessing.Process(
                    None, eval_svm_radius_threading, None,
                    (img, radius, radius_index, svm_scaler, outputs_queue))
                process.start()
                processes[i] = process
                all_done = False
                work_done = True
            elif process is not None and process.is_alive():
                all_done = False
        # clear the queue and add results to outputs_all_radiuses
        while True:
            try:
                radius_index, responses, responses_smoothed = outputs_queue.get_nowait()
                responses_all_radiuses[radius_index, :, :, :] = responses
                responses_all_radiuses_smoothed[radius_index, :, :, :] = responses_smoothed
                work_done = True
            except queue.Empty:
                break
        # If we've done any work, we don't need to delay because the work itself causes a delay.
        if not work_done:
            time.sleep(DELAY_BETWEEN_CHECKS)
    return responses_all_radiuses, responses_all_radiuses_smoothed


def eval_svm_image(img, svm_scaler, use_threading=False):
    """Evaluates the given convnet on the given image.

    Args:
        img: ndarray of h * w or h * w * c
        svm_scaler: as loaded by utils.load_svm_pretrained
        use_threading: whether or not to use multiprocessing

    Returns: a tuple of
        -a h * w binary (ie. 1 or 0) uint8 ndarray of whether a point was considered to be medial or
        not.
        -a h * w uint8 ndarray of the radius of each medial point (all other points have "radius" 0)
    """
    img = utils.gray2rgb(img)
    h, w, c = img.shape
    assert c == 3
    del c
    radius_range = range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE)
    if use_threading:
        responses_all_radiuses, responses_all_radiuses_smoothed = \
            eval_svm_image_threading(img, radius_range, svm_scaler)
    else:
        responses_all_radiuses, responses_all_radiuses_smoothed = [], []
        for radius in radius_range:
            responses, responses_processed = eval_svm_radius(img, radius, svm_scaler)
            responses_all_radiuses.append(responses)
            responses_all_radiuses_smoothed.append(responses_processed)
        responses_all_radiuses = np.stack(responses_all_radiuses, axis=0)
        responses_all_radiuses_smoothed = np.stack(responses_all_radiuses_smoothed, axis=0)
    # detections, radiuses = eval_convnet.skeletonize_all_radiuses(
    #     responses_all_radiuses_smoothed, radius_range)
    detections, radiuses = None, None
    return detections, radiuses, responses_all_radiuses, responses_all_radiuses_smoothed


def eval_one():
    """Evaluate a SVM on an entire image"""
    assert not const.GLOBAL_GOOGLE and not const.GLOBAL_SYNTH
    bmax_datum = utils.load_bmax_test()[const.GLOBAL_EVAL_IMAGE]
    img = utils.img_uint8_to_float32(bmax_datum['img'])
    img_rad_gt = bmax_datum['rad']
    img_skel_gt = bmax_datum['pts']
    print('loaded bmax image', const.GLOBAL_EVAL_IMAGE)
    image_suffix = 'bmax_{0}'.format(const.GLOBAL_EVAL_IMAGE)
    results_dir = Path('results/responses_bmax_svm_{0}/'.format(image_suffix))
    assert np.any(img > 1./255.)
    assert np.all(img <= 1.)
    assert np.all(img >= 0.)

    filename = str(results_dir / 'gt.png')
    imageio.imwrite(filename, np.max(img_skel_gt, axis=2) * 255)
    print('saved to {0} at {1}'.format(filename, datetime.datetime.now()))

    weights_filename = const.GLOBAL_EVAL_WEIGHTS
    svm_scaler = utils.load_svm_pretrained(weights_filename)
    medials, *_ = eval_svm_image(img, svm_scaler, True)
    filename = str(results_dir / 'outputs.png')
    imageio.imwrite(filename, medials * 255)
    print('saved to {0} at {1}'.format(filename, datetime.datetime.now()))
    # tps, fps, fns = compare_output_gt(medials, np.max(img_skel_gt, axis=2))
    # print('true positives:', tps)
    # print('false positives:', fps)
    # print('false negatives:', fns)
    precision, recall, f_score, *_ = eval_convnet.compute_image_stats(
        medials, np.transpose(img_skel_gt, (2, 0, 1)))
    print('prec: {0:.4f}; rec: {1:.4f}; f: {2:.4f}'.format(precision, recall, f_score))


def mk_dirs(image_range):
    """Deletes and makes the directories for saving results"""
    for image_index in image_range:
        results_dir = Path('results/responses_bmax_svm_{0}'.format(image_index))
        shutil.rmtree(str(results_dir), ignore_errors=True)
        os.mkdir(str(results_dir))


def eval_bmax(image_range=None):
    """Evaluates a convnet on the BMAX testing set.

    Args:
        image_range: an iterable of image indexes, where the indexes are from 0 to 99 (inclusive).
        Optional. If not provided, will run the whole BMAX.
    """
    if image_range is None:
        image_range = range(100)
    weights_filename = const.GLOBAL_EVAL_WEIGHTS
    svm_scaler = utils.load_svm_pretrained(weights_filename)
    bmax_test = utils.load_bmax_test()
    # sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision = 0, 0, 0, 0
    mk_dirs(image_range)
    for image_index in image_range:
        print('working with image:', image_index)
        results_dir = Path('results/responses_bmax_svm_{0}'.format(image_index))
        bmax_datum = bmax_test[image_index]
        img = utils.img_uint8_to_float32(bmax_datum['img'])
        filename_img = str(results_dir / 'img.png')
        imageio.imwrite(filename_img, utils.img_float32_to_uint8(img))
        img_gt = np.transpose(bmax_datum['pts'], (2, 0, 1))
        filename_gt = str(results_dir / 'gt.png')
        imageio.imwrite(filename_gt, np.max(img_gt, axis=0) * 255)
        detected, _, responses, responses_smoothed = eval_svm_image(img, svm_scaler, True)
        del img, _
        # filename_detected = str(results_dir / 'detected.png')
        # imageio.imwrite(filename_detected, detected * 255)
        del bmax_datum
        # prec_image, rec_image, f_image, tp_recall, gt_recall, tp_precision, d_precision = \
        #     eval_convnet.compute_image_stats(detected, img_gt)
        # del detected, img_gt
        # sum_tp_recall += tp_recall
        # sum_gt_recall += gt_recall
        # sum_tp_precision += tp_precision
        # sum_d_precision += d_precision
        #
        # print('image {0} got prec: {1:.4f}, rec: {2:.4f}, f: {3:.4f}'.
        #       format(image_index, prec_image, rec_image, f_image))
        # print('So far, numer prec: {0}, denom prec: {1}, numer rec: {2}, denom rec: {3}'.
        #       format(sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision))
        del detected, img_gt

        response_radius = None
        filename_raw_template = 'responses_{0}_raw.png'
        for i in range(responses.shape[0]):
            radius = const.EVAL_MIN_RAD + i
            response_radius = responses[i, :, :, :]
            filename_raw = str(results_dir / filename_raw_template.format(radius))
            imageio.imwrite(filename_raw, eval_convnet.transpose_responses(utils.img_float32_to_uint8(response_radius)))
        del responses, response_radius
        response_radius = None
        filename_smoothed_template = 'responses_{0}_smoothed.png'
        for i in range(responses_smoothed.shape[0]):
            radius = const.EVAL_MIN_RAD + i
            response_radius = responses_smoothed[i, :, :, :]
            filename_smoothed = str(results_dir / filename_smoothed_template.format(radius))
            imageio.imwrite(filename_smoothed, eval_convnet.transpose_responses(utils.img_float32_to_uint8(response_radius)))
        responses_amalgamated = np.max(responses_smoothed[:, :, :, 2], axis=0)
        filename_amalgamated = str(results_dir / 'responses_medial.png')
        imageio.imwrite(filename_amalgamated, utils.img_float32_to_uint8(responses_amalgamated))
        filename_amalgamated = str(results_dir / 'responses_medial.mat')
        scipy.io.savemat(filename_amalgamated, {'responses': responses_amalgamated})
        del responses_smoothed, response_radius
        print('saved to {0}, {1}, {2}, {3}, {4}'.
              format(filename_img, filename_gt, filename_raw, filename_smoothed,
                     filename_amalgamated))

    # precision = sum_tp_precision / max(const.EPS, sum_d_precision)
    # recall = sum_tp_recall / max(const.EPS, sum_gt_recall)
    # f_score = 2 * (recall * precision) / (recall + precision)
    # return precision, recall, f_score, sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision


def main():
    print('we\'re about to evaluate a SVM')
    text = input('enter an image start index, from 0 to 99 inclusive:')
    if not text.strip():
        text = '0'
    start_index = int(text)
    text = input('enter an image end index (which won\'t be included), from 1 to 100 inclusive:')
    if not text.strip():
        text = '100'
    end_index = int(text)
    assert end_index >= start_index
    image_range = range(start_index, end_index)
    # precision, recall, f_score, tp_recall, gt_recall, tp_precision, d_precision = eval_bmax(image_range)
    # print('prec: {0:.4f}; rec: {1:.4f}; f: {2:.4f}'.format(precision, recall, f_score))
    # print('true positives, recall: {0}; ground truth positives, recall: {1}'.format(tp_recall, gt_recall))
    # print('true positives, precision: {0}; detected positives, precision: {1}'.format(tp_precision, d_precision))
    eval_bmax(image_range)


if __name__ == "__main__":
    main()
