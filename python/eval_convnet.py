"""Stuff for evaluating the performance of the CNN trained by train_cnn."""

import datetime
import math
import multiprocessing
import os
from pathlib import Path
import queue
import shutil
import socket
import time

import hickle
import imageio
from matplotlib import pyplot as plt
import numpy as np
# from pathos import multiprocessing
# import random
import scipy.ndimage
import scipy.io
# from skimage import morphology
import torch
from torch.nn import functional

from python import const
from python import utils


# def calculate_pr_curve(responses, gts, thresholds=None):
#     """A calculation of the precision-recall curve for a medial point detector.
# 
#     I can't just use sklearn's pr-curve function because my medial point
#     detection algorithm involves:
#         probability maps for all radiuses->smooth->amalgamate radiuses->
#         threshold->skeletonize->compute precision and recall
#     Note that thresholding isn't the last step, and precision and recall require
#     a special calculation.
# 
#     Args:
#         responses: a sequence of ndarrays of all responses (smoothed, medial
#         channel only) at all radiuses for all BMAX500 test images. Each element
#         of the sequence corresponds to the responses over a test image. The
#         shape of each ndarray is h * w * r, where h and w are the image height
#         and width and r is the number of radiuses over which we evaluated
#         gts: a sequence of ndarrays of all ground truths. Each ndarray
#         corresponds to the (multiple) ground truth medial points of each BMAX
#         test image. Each ndarray is of shape s * h * w, where s is the number of
#         different ground truth segmentations of the test image. The value of s
#         typically varies between 5 and 7 for BMAX500.
#         thresholds: the ndarray arange of thresholds over which to calculate the
#         PR curve. Optional. Default is 1001 thresholds uniformly varied between
#         0 and 1.
# 
#     Returns:
#         a list of tuples, where each tuple is a precision, recall pair
#         corresponding to a threshold in arg thresholds
#     """
#     radius_range = range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE)
#     if thresholds is None:
#         thresholds = np.linspace(0., 1., 1001, endpoint=True)
#     result = []
#     for threshold in thresholds:
#         sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision = 0, 0, 0, 0
#         for responses_image, gts_image in zip(responses, gts):
#             detected, _ = skeletonize_all_radiuses(responses_image, radius_range, threshold)
#             *_, tp_recall, gt_recall, tp_precision, d_precision = \
#                 # compute_image_stats(detected, gts_image)
#             del detected, _
#             sum_tp_recall += tp_recall
#             sum_gt_recall += gt_recall
#             sum_tp_precision += tp_precision
#             sum_d_precision += d_precision
#         precision = sum_tp_precision / max(const.EPS, sum_d_precision)
#         recall = sum_tp_recall / max(const.EPS, sum_gt_recall)
#         result.append((precision, recall))
#     return result


# def skeletonize_all_radiuses(outputs_all_radiuses, radius_range, threshold=None):
#     """Flattens the outputs amalgamated from all radiuses.
#
#     Args:
#         outputs_all_radiuses: ndarray of the form n * h * w * 3, where n is the number of
#         different radiuses from radius_range
#         radius_range: a range (or similar object) of radiuses
#         threshold: the threshold above which a point is considered medial, range [0, 1]. Defaults to
#         const.EVAL_THRESHOLD
#
#     Returns: a tuple of:
#         -a binary image of shape h * w, denoting which points are medial
#         -a image of shape h * w, denoting the radius for each medial point. Non-medial points will have "radius" 0.
#     """
#     if threshold is None:
#         threshold = const.EVAL_THRESHOLD
#     MEDIAL_CHANNEL = 2
#     n, h, w, c = outputs_all_radiuses.shape
#     assert c == 3
#     outputs_medial_only = outputs_all_radiuses[:, :, :, MEDIAL_CHANNEL]
#     del outputs_all_radiuses
#     outputs_flattened = np.max(outputs_medial_only, axis=0) > threshold
#     assert outputs_flattened.shape == (h, w)
#     # print('outputs flattened dimensions:', outputs_flattened.shape)
#     outputs_skeletonized = morphology.skeletonize(outputs_flattened).astype(np.uint8)
#     # print('outputs skeletonized dimensions:', outputs_skeletonized.shape)
#     del outputs_flattened
#     # For each medial point, we keep the largest radius in outputs_all_radiuses that is medial
#     # (Just in case there are more than one medial radius for each medial point)
#     mask = np.tile(outputs_skeletonized[np.newaxis, :, :], (n, 1, 1))
#     outputs_medial_masked = np.min(np.stack((outputs_medial_only, mask), axis=0), axis=0)
#     del mask, outputs_medial_only
#     rads, ys, xs = np.nonzero(outputs_medial_masked)
#     del outputs_medial_masked
#     radiuses = np.zeros((h, w), np.uint8)
#     for rad_index, y, x in zip(rads, ys, xs):
#         radius = radius_range[rad_index]
#         if radius > radiuses[y, x]:
#             radiuses[y, x] = radius
#     return outputs_skeletonized, radiuses


def post_process_radius(collected, sigma):
    """Returns a post-processed version of the collected outputs of a given radius.

    Currently, we apply a gaussian smoothing.

    Args:
        collected: the type returned by collect_outputs_radius
        sigma: the sigma used for smoothing
    """
    h, w, c = collected.shape
    assert c == 3
    # collected = scipy.ndimage.gaussian_filter(collected, sigma, mode='constant')
    collected = utils.gaussian_smooth(collected, sigma)
    # smoothing has a habit of reducing peaks, and will probably change the "probability" given
    # to us by a softmax into something that doesn't sum to 1. So let's make it sum to 1 again.
    sums = np.sum(collected, axis=2)
    sums = np.tile(sums[:, :, np.newaxis], (1, 1, c))
    assert sums.shape == (h, w, c)
    np.divide(collected, sums, collected)
    # sums = np.sum(collected, axis=2)  # for debugging; comment out!
    return collected


def collect_responses_radius(responses, ys, xs, output_shape):
    """Returns a ndarray in the shape of output_shape (but with three channels)
    containing the convnet response information filled in. Return is of type
    np.float32.

    Args:
        responses: as returned by eval_convnet_unwraps
        ys: as returned by collect_unwraps_radius
        xs: ditto
        output_shape: 2-tuple of (h, w)
    """
    assert len(output_shape) == 2
    collected = np.concatenate((np.ones(output_shape + (1,), np.float32),
                                np.zeros(output_shape + (2,), np.float32)), axis=2)
    assert collected.shape == output_shape + (3,)
    collected[ys, xs, :] = responses
    return collected


def eval_convnet_minibatch(unwraps, convnet):
    unwraps = utils.unwraps_to_torch(unwraps)
    unwraps.requires_grad_(False)
    responses = convnet(unwraps)
    del unwraps
    responses = functional.softmax(responses, dim=1)
    responses = responses.cpu().detach().numpy()
    return responses


def eval_convnet_unwraps(convnet, unwraps, gpu_semaphores=None):
    """Evaluates the given convnet on the given unwraps and returns the results.

    Args:
        convnet: as returned by utils.load_convnet_pretrained
        unwraps: ndarray of n * unwrap_h * unwrap_w * c as returned by collect_unwraps_radius
        gpu_semaphores: optional. If provided, a tuple of:
            -an instance of multiprocessing.Semaphore used to control how many copies of the convnet
            are currently loaded to the GPU
            -an Semaphore used to control how many inferences are simultaneously being run on the
            GPU

    Returns:
        ndarray of n * 3 where the columns represent likelihood of (respectively):
            -non-tangent
            -uni-tangent
            -medial
    """
    NUM_CNN_OUTPUT_CHANNELS = 3
    num_unwraps = unwraps.shape[0]
    all_outputs = np.empty((num_unwraps, NUM_CNN_OUTPUT_CHANNELS))
    if gpu_semaphores is None:
        for indexes in utils.batch_generator(num_unwraps, const.EVAL_BATCH_SIZE):
            curr_unwraps = unwraps[indexes, :, :, :]
            outputs = eval_convnet_minibatch(curr_unwraps, convnet)
            all_outputs[indexes, :] = outputs
    else:
        loaded_count, inference_count = gpu_semaphores
        with loaded_count:
            if const.GLOBAL_USE_CUDA:
                convnet = convnet.cuda()
            with inference_count:
                for indexes in utils.batch_generator(num_unwraps, const.EVAL_BATCH_SIZE):
                    curr_unwraps = unwraps[indexes, :, :, :]
                    # If we try to avoid minibatches, we run out of memory. Even 12gb isn't enough.
                    outputs = eval_convnet_minibatch(curr_unwraps, convnet)
                    all_outputs[indexes, :] = outputs
                    del outputs
            del convnet
            torch.cuda.empty_cache()
    return all_outputs


def max_process_count():
    """Returns the process count to use"""
    hostname = socket.gethostname()
    if hostname == 'OhDear':
        MAX_NUM_PROCESSES = 4
    elif hostname == 'bolt1':
        MAX_NUM_PROCESSES = 6
    elif hostname == 'bolt2':
        MAX_NUM_PROCESSES = 8
    else:
        print('warning: unrecognized computer:', hostname)
        MAX_NUM_PROCESSES = multiprocessing.cpu_count()
    # print('using up to {0} processes'.format(MAX_NUM_PROCESSES))
    return MAX_NUM_PROCESSES


def max_gpu_simul():
    """Returns the maximum number of simultaneous convnets running on the gpu at a time"""
    # In practice, I've found that each GPU can handle up to two inferences at a time. A single
    # inference will only occupy ~60% of the GPU's power
    hostname = socket.gethostname()
    if hostname == 'OhDear':
        max_gpus = 1
    elif hostname == 'bolt1':
        max_gpus = 2
    elif hostname == 'bolt2':
        max_gpus = 2
    else:
        print('warning: unrecognized computer:', hostname)
        max_gpus = 1
    # print('running up to {0} convnets at a time'.format(max_gpus))
    return max_gpus


def max_convnet_loaded():
    """Returns the maximum number of convnets simultaneously loaded onto the GPU"""
    hostname = socket.gethostname()
    # Each non-running convnet takes up about 563 MiB of GPU RAM
    # Each running convnet takes about 1897MiB of GPU RAM
    if hostname == 'OhDear':
        max_gpus = 1
    elif hostname == 'bolt1':
        # Each bolt1 GPU has about 8GB
        max_gpus = 4
    elif hostname == 'bolt2':
        # Each bolt2 GPU has about 12GB
        max_gpus = 6
    else:
        print('warning: unrecognized computer:', hostname)
        max_gpus = 2
    return max_gpus


def collect_unwraps_radius(img, radius):
    """Collects and returns all unwraps of the image at the given radius.

    Args:
        img: h * w * c ndarray representing the image we're trying to unwrap
        radius: the radius at which we'd like to unwrap.

    Returns: a tuple of three ndarrays, each with the same number of elements n:
        -unwraps: n * unwrap_h * unwrap_w * c
        -ys: the y coordinate that each corresponding unwrap came from
        -xs: ditto, for x
    """
    h, w, _ = img.shape
    min_xy = max(math.floor(radius) - 1, 0)
    max_y = min(math.ceil(h - radius) + 1, h)
    max_x = min(math.ceil(w - radius) + 1, w)
    ys = np.arange(min_xy, max_y, const.EVAL_STRIDE, int)
    xs = np.arange(min_xy, max_x, const.EVAL_STRIDE, int)
    ys, xs = np.meshgrid(ys, xs)
    ys, xs = np.ravel(ys), np.ravel(xs)
    rads, angles = radius * np.ones(ys.shape), np.zeros(ys.shape)
    unwraps = utils.unwrap(img, ys, xs, rads, angles)
    num_unwraps = unwraps.shape[0]
    assert len(ys) == len(xs) == len(rads) == len(angles) == num_unwraps
    return unwraps, ys, xs


def eval_convnet_radius(img, radius, convnet, gpu_semaphores=None):
    """Evaluate the convnet at the given radius on the given image, assigning the output to
    outputs_all_radiuses

    Args:
        img: h * w * 3 ndarray denoting an image
        radius: the radius over which to operate
        convnet: the convnet loaded by utils.load_convnet_pretrained

    Returns: a tuple of
        -the responses as a h * w * 3 ndarray
        -the smoothed responses as a h * w * 3 ndarray
    """
    h, w, _ = img.shape
    unwraps, ys, xs = collect_unwraps_radius(img, radius)
    if unwraps.shape[0] == 0:
        return np.zeros((h, w, 3), dtype=np.float32), np.zeros((h, w, 3), dtype=np.float32)
    # print('unwraps.shape:', unwraps.shape)
    # print('starting evaluation of a radius')
    responses = eval_convnet_unwraps(convnet, unwraps, gpu_semaphores)
    del convnet, unwraps
    collected = collect_responses_radius(responses, ys, xs, (h, w))
    del responses, ys, xs
    smoothed = post_process_radius(collected, const.EVAL_CONVNET_SMOOTH_SIGMA)
    assert collected.shape == smoothed.shape == (h, w, 3)
    return collected, smoothed


def eval_convnet_radius_threading(img, radius, radius_index, convnet, outputs_queue,
                                  gpu_convnet_count):
    """Runs eval_convnet_radius and then outputs the radius index and responses and smoothed
    responses to the given outputs queue as a tuple.

    Returns:
        None
    """
    responses, responses_smoothed = eval_convnet_radius(img, radius, convnet, gpu_convnet_count)
    outputs_queue.put((radius_index, responses, responses_smoothed))


def eval_convnet_image_threading(img, radius_range, convnet):
    DELAY_BETWEEN_CHECKS = 1.  # seconds in between checking threads for completion
    NUM_CLASSES = 3
    h, w, _ = img.shape
    max_threads = max_process_count()
    max_used_radius_index = -1
    responses_all_radiuses = np.zeros((len(radius_range), h, w, NUM_CLASSES), dtype=np.float32)
    responses_all_radiuses_smoothed = np.zeros((len(radius_range), h, w, NUM_CLASSES),
                                               dtype=np.float32)
    processes = [None for _ in range(max_threads)]
    gpu_inference_count = multiprocessing.Semaphore(max_gpu_simul())
    gpu_convnet_count = multiprocessing.Semaphore(max_convnet_loaded())
    outputs_queue = multiprocessing.Queue()
    all_done = False
    while not all_done:
        all_done = True
        work_done = False
        for i, process in enumerate(processes):
            if ((process is None or not process.is_alive())
                    and max_used_radius_index < len(radius_range) - 1):
                # If the thread is no longer working, we should kill it and create a new thread in
                # its place
                # We select the first available radius
                max_used_radius_index += 1
                radius_index = max_used_radius_index
                radius = radius_range[radius_index]
                # print('working on radius:', radius)
                process = multiprocessing.Process(
                    None, eval_convnet_radius_threading, None,
                    (img, radius, radius_index, convnet, outputs_queue, (gpu_convnet_count,
                                                                         gpu_inference_count)))
                process.start()
                processes[i] = process
                all_done = False
                work_done = True
            elif process is not None and process.is_alive():
                all_done = False
        # clear the queue and add results to outputs_all_radiuses
        while True:
            try:
                radius_index, responses, responses_smoothed = outputs_queue.get_nowait()
                responses_all_radiuses[radius_index, :, :, :] = responses
                responses_all_radiuses_smoothed[radius_index, :, :, :] = responses_smoothed
                work_done = True
            except queue.Empty:
                break
        # If we've done any work, we don't need to delay because the work itself causes a delay.
        if not work_done:
            time.sleep(DELAY_BETWEEN_CHECKS)
    return responses_all_radiuses, responses_all_radiuses_smoothed


def eval_convnet_image(img, convnet, use_threading=False):
    """Evaluates the given convnet on the given image.

    Args:
        img: ndarray of h * w or h * w * c
        convnet: as loaded by utils.load_convnet_pretrained

    Returns: a tuple of
        -a h * w binary (ie. 1 or 0) uint8 ndarray of whether a point was considered to be medial or
        not.
        -a h * w uint8 ndarray of the radius of each medial point (all other points have "radius" 0)
    """
    img = utils.gray2rgb(img)
    h, w, c = img.shape
    assert c == 3
    del c
    radius_range = range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE)
    if use_threading:
        responses_all_radiuses, responses_all_radiuses_smoothed = \
            eval_convnet_image_threading(img, radius_range, convnet)
    else:
        if const.GLOBAL_USE_CUDA:
            convnet = convnet.cuda()
        responses_all_radiuses, responses_all_radiuses_smoothed = [], []
        for radius in radius_range:
            responses, responses_processed = eval_convnet_radius(img, radius, convnet)
            responses_all_radiuses.append(responses)
            responses_all_radiuses_smoothed.append(responses_processed)
        responses_all_radiuses = np.stack(responses_all_radiuses, axis=0)
        responses_all_radiuses_smoothed = np.stack(responses_all_radiuses_smoothed, axis=0)
    # detections, radiuses = skeletonize_all_radiuses(responses_all_radiuses_smoothed, radius_range)
    detections, radiuses = None, None
    return detections, radiuses, responses_all_radiuses, responses_all_radiuses_smoothed


def dist_points_target(points, target):
    """Returns the euclidean distance from the nearest point in points to the given target point

    Args:
        points: a num_points * 2 ndarray of (y, x) points
        target: array-like vector of 2 entries (y, x)
    """
    num_points = points.shape[0]
    assert points.shape == (num_points, 2)
    target = np.expand_dims(target, 0)
    assert target.shape == (1, 2)
    diffs = points - target  # broadcasts target from (1, 2) to (num_points, 2)
    assert diffs.shape == (num_points, 2)
    dists = np.linalg.norm(diffs, axis=1)
    assert dists.shape == (num_points,)
    return np.min(dists, initial=math.inf)


def transpose_responses(responses):
    """Tranposes and returns the responses.

    Args:
        responses: ndarray h * w * 3 denoting the responses of a classifier.
        Each channel represents respectively the likelihood of:
            -non-tangent
            -uni-tangent
            -medial

    Returns:
        responses, transposed such that:
            -non-tangent represents red
            -uni-tangent represents blue
            -medial represents green
    """
    h, w, c = responses.shape
    assert c == 3
    result = np.stack((responses[:, :, 0], responses[:, :, 2], responses[:, :, 1]), axis=2)
    assert result.shape == (h, w, c)
    return result


def mk_dirs(image_range, model_name):
    """Deletes and makes the directories for saving results. Returns a list of dirs created as Path
    objects.
    """
    paths = []
    for image_index in image_range:
        results_dir = Path('results/responses_bmax_convnet_{0}_{1}'.format(model_name, image_index))
        shutil.rmtree(str(results_dir), ignore_errors=True)
        os.mkdir(str(results_dir))
        paths.append(results_dir)
    return paths


def eval_bmax(image_range=None):
    """Evaluates a convnet on the BMAX testing set.

    Args:
        image_range: an iterable of image indexes, where the indexes are from 0 to 99 (inclusive).
        Optional. If not provided, will run the whole BMAX.
    """
    # Returns: a tuple of
    #     -precision
    #     -recall
    #     -f score
    #     -number of true positives, for calculating recall
    #     -number of ground truth positives, for calculating recall
    #     -number of true positives, for calculating precision
    #     -number of classified positives, for calculating precision
    #     -the precision-recall curve as returned by calculate_pr_curve
    if image_range is None:
        image_range = range(100)
    weights_filename = const.GLOBAL_EVAL_WEIGHTS
    convnet, model_name = utils.load_convnet_pretrained(weights_filename, no_gpu=True)
    bmax_test = utils.load_bmax_test()
    # sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision = 0, 0, 0, 0
    all_responses_smoothed, all_gts = [], []
    paths = mk_dirs(image_range, model_name)
    for results_dir, image_index in zip(paths, image_range):
        bmax_datum = bmax_test[image_index]
        img = bmax_datum['img']
        filename_img = str(results_dir / 'img.png')
        imageio.imwrite(filename_img, img)
        img = utils.img_uint8_to_float32(img)
        img_gt = np.transpose(bmax_datum['pts'], (2, 0, 1))
        all_gts.append(img_gt)
        filename_gt = str(results_dir / 'gt.png')
        imageio.imwrite(filename_gt, np.max(img_gt, axis=0) * 255)
        detected, _, responses, responses_smoothed = eval_convnet_image(img, convnet, True)
        all_responses_smoothed.append(responses_smoothed)
        del img, _
        del bmax_datum
        del detected, img_gt

        response_radius = None
        filename_raw_template = 'responses_{0}_raw.png'
        for i in range(responses.shape[0]):
            radius = const.EVAL_MIN_RAD + i
            response_radius = responses[i, :, :, :]
            filename_raw = str(results_dir / filename_raw_template.format(radius))
            imageio.imwrite(filename_raw, transpose_responses(utils.img_float32_to_uint8(response_radius)))
        del responses, response_radius
        response_radius = None
        filename_smoothed_template = 'responses_{0}_smoothed.png'
        for i in range(responses_smoothed.shape[0]):
            radius = const.EVAL_MIN_RAD + i
            response_radius = responses_smoothed[i, :, :, :]
            filename_smoothed = str(results_dir / filename_smoothed_template.format(radius))
            imageio.imwrite(filename_smoothed, transpose_responses(utils.img_float32_to_uint8(response_radius)))
        responses_amalgamated = np.max(responses_smoothed[:, :, :, 2], axis=0)
        filename_amalgamated = str(results_dir / 'responses_medial.mat')
        scipy.io.savemat(filename_amalgamated, {'responses': responses_amalgamated}, appendmat=False)
        imageio.imwrite(filename_amalgamated[:-4] + '.png', utils.img_float32_to_uint8(responses_amalgamated))
        del responses_smoothed, response_radius
        # print('saved to {0}, {1}, {2}, {3}, {4}, {5}'.
        #       format(filename_img, filename_gt, filename_detected, filename_raw, filename_smoothed,
        #              filename_amalgamated))
        print('saved to {0}, {1}, {2}, {3}, {4}'.
              format(filename_img, filename_gt, filename_raw, filename_smoothed,
                     filename_amalgamated))

    # precision = sum_tp_precision / max(const.EPS, sum_d_precision)
    # recall = sum_tp_recall / max(const.EPS, sum_gt_recall)
    # f_score = 2 * (recall * precision) / max(const.EPS, recall + precision)
    # print('bmax prec: {0:.4f}; rec: {1:.4f}; f: {2:.4f}'.format(precision, recall, f_score))
    # pr_curve = calculate_pr_curve(all_responses_smoothed, all_gts)
    # return precision, recall, f_score, sum_tp_recall, sum_gt_recall, sum_tp_precision, sum_d_precision, pr_curve


def eval_one():
    """Evaluate a CNN on an entire image"""
    if const.GLOBAL_GOOGLE:
        img, img_skel_gt, img_rad_gt = hickle.load(const.GOOGLE_CHROME_PROCESSED_FILENAME)
        img = img.astype(np.float32)
        print('loaded Google Chrome logo')
        image_suffix = 'google'
    elif const.GLOBAL_SYNTH:
        img, img_skel_gt, img_rad_gt = hickle.load(const.SYNTH_TRAIN['data/triangles.png'][0])
        img = np.clip(img, 0., 1.)
        image_suffix = 'triangles'
        print('loaded synthetic image', image_suffix)
    else:
        bmax_datum = utils.load_bmax_test()[const.GLOBAL_EVAL_IMAGE]
        img = utils.img_uint8_to_float32(bmax_datum['img'])
        img_rad_gt = bmax_datum['rad']
        img_skel_gt = bmax_datum['pts']
        print('loaded bmax image', const.GLOBAL_EVAL_IMAGE)
        image_suffix = 'bmax_{0}'.format(const.GLOBAL_EVAL_IMAGE)
    results_dir = Path('results/responses_bmax_convnet_{0}/'.format(image_suffix))
    assert np.any(img > 1./255.)
    assert np.all(img <= 1.)
    assert np.all(img >= 0.)

    filename = str(results_dir / 'gt.png')
    imageio.imwrite(filename, np.max(img_skel_gt, axis=2) * 255)
    print('saved to {0} at {1}'.format(filename, datetime.datetime.now()))

    weights_filename = const.GLOBAL_EVAL_WEIGHTS
    convnet = utils.load_convnet_pretrained(weights_filename, no_gpu=True)
    medials, *_ = eval_convnet_image(img, convnet, True)
    filename = str(results_dir / 'outputs.png')
    imageio.imwrite(filename, medials * 255)
    print('saved to {0} at {1}'.format(filename, datetime.datetime.now()))
    # tps, fps, fns = compare_output_gt(medials, np.max(img_skel_gt, axis=2))
    # print('true positives:', tps)
    # print('false positives:', fps)
    # print('false negatives:', fns)
    # precision, recall, f_score, *_ = compute_image_stats(medials, np.transpose(img_skel_gt, (2, 0, 1)))
    # print('prec: {0:.4f}; rec: {1:.4f}; f: {2:.4f}'.format(precision, recall, f_score))


def main():
    print('we\'re about to evaluate a convnet')
    text = input('enter an image start index, from 0 to 99 inclusive:')
    if not text:
        text = '0'
    start_index = int(text)
    text = input('enter an image end index, from 1 to 100 inclusive:')
    if not text:
        text = '100'
    end_index = int(text)
    assert end_index >= start_index
    image_range = range(start_index, end_index)
    # precision, recall, f_score, tp_recall, gt_recall, tp_precision, d_precision, pr_curve = \
    #     eval_bmax(image_range)
    # print('prec: {0:.4f}; rec: {1:.4f}; f: {2:.4f}'.format(precision, recall, f_score))
    # print('true positives, recall: {0}; ground truth positives, recall: {1}'.format(tp_recall, gt_recall))
    # print('true positives, precision: {0}; detected positives, precision: {1}'.format(tp_precision, d_precision))
    # filename = 'results/convnet_pr_curve.hkl'
    # print('saving pr curve to:', filename)
    # hickle.dump(pr_curve, filename)
    eval_bmax(image_range)


if __name__ == "__main__":
    multiprocessing.set_start_method('spawn')
    with utils.Timer('main function'):
        main()
