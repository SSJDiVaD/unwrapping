"""A script for analyzing how a model performs on a given dataset"""

from matplotlib import pyplot
import numpy as np
import sklearn.metrics
import statistics
import torch
from torch.nn import functional

from python import const
from python import load_unwrappings
from python import nn_models
from python import process_google
from python import train_cnn
from python import utils


def main():
    VAL_VOTES = False  # whether or not to evaluate the f1 for each number of votes
    PREC_REC_CURVE = True  # whether or not to generate a precision/recall curve
    WRONGEST_UNWRAPPINGS = False  # whether or not to show the unwrappings that the classifier got most wrong
    # for now, CNN only
    filename = const.GLOBAL_ARGS_OTHER[0]
    if const.GLOBAL_USE_CUDA:
        map_location = 'cuda:0'
    else:
        map_location = 'cpu'
    state_dict, experiment, _, _ = torch.load(filename, map_location)
    cnn = nn_models.get_model(experiment)
    cnn.load_state_dict(state_dict)
    cnn.eval()

    if VAL_VOTES:
        # determine how the classifier performs on each number of votes
        for votes_per_point_rad in [1, 2, 4, 8, 16, 32, 64]:
            if const.GLOBAL_GOOGLE:
                loader = process_google.GoogleSgdLoader(const.GOOGLE_UNWRAPPED_FILENAME.format(suffix='chrome_4096'),
                                                        votes_per_point_rad, False, True, False, False)
            else:
                suffix = 'val_' + str(const.GLOBAL_VAL_SIZE)
                filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
                loader = load_unwrappings.UnwrappingsLoader(filename, True, const.EVAL_BATCH_SIZE, votes_per_point_rad,
                                                            False, True, False, False)
            _, labels, likelihoods = eval_epoch_vote(cnn, loader)
            likelihoods = likelihoods >= 0.5
            f1 = sklearn.metrics.f1_score(labels, likelihoods,)
            print('with {0} votes, got f1 of {1:.6f}'.format(votes_per_point_rad, f1))
        _ = input('Press enter:')

    if PREC_REC_CURVE:
        if const.GLOBAL_GOOGLE:
            loader = process_google.GoogleSgdLoader(const.GOOGLE_UNWRAPPED_FILENAME.format(suffix='chrome_4096'),
                                                    const.TRAIN_FIXED_VOTES_PER_PIXEL, False, True, False, False)
        else:
            suffix = 'val_' + str(const.GLOBAL_VAL_SIZE)
            filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
            loader = load_unwrappings.UnwrappingsLoader(filename, const.GLOBAL_VAL_VOTE, const.TRAIN_BATCH_SIZE,
                                                        const.TRAIN_FIXED_VOTES_PER_PIXEL, False, True, False, False)
        unwraps, labels, likelihoods = eval_epoch_vote(cnn, loader)

        # for a given threshold, what precision/recall do we get?
        precs, recs, thresholds = sklearn.metrics.precision_recall_curve(labels, likelihoods)
        # all_f1s, all_thresholds = [], []
        for precision, recall, threshold in zip(precs, recs, thresholds):
            print('at threshold {0:.4f}, precision {1:.4f} and recall {2:.4f}'.format(threshold, precision, recall))
        # plot the prec-rec curve
        pyplot.step(recs, precs, color='b', alpha=0.2, where='post')
        pyplot.fill_between(recs, precs, step='post', alpha=0.2, color='b')

        pyplot.xlabel('Recall')
        pyplot.ylabel('Precision')
        pyplot.ylim([0.0, 1.00])
        pyplot.xlim([0.0, 1.00])
        pyplot.show()

        if WRONGEST_UNWRAPPINGS:
            # iterate through all unwrappings in decreasing order of wrongness
            wrongnesses = np.abs(labels - likelihoods)
            idxs = np.flip(np.argsort(wrongnesses, axis=0), axis=0)  # sort by decreasing order of how wrong the classifier was
            wrongs_sorted = wrongnesses[idxs]
            unwraps_sorted = unwraps[idxs]
            labels_sorted = labels[idxs]
            likelihoods_sorted = likelihoods[idxs]
            for unwrap, wrongness, label, likelihood in zip(unwraps_sorted, wrongs_sorted, labels_sorted, likelihoods_sorted):
                print('label: {0}; likelihood: {1:.4f}; wrongness: {2:.4f}'.format(label, likelihood, wrongness))
                pyplot.imshow(unwrap)
                pyplot.show()


def eval_epoch_vote(cnn, loader_val):
    cnn.eval()
    all_outputs, all_images, all_labels, all_idxs = [], [], [], []
    for i, (images, labels, idxs) in enumerate(loader_val):
        outputs = cnn(images)
        outputs = np.squeeze(functional.sigmoid(outputs.data).cpu().detach().numpy(), axis=1)  # >= 0 means true
        assert outputs.shape == labels.shape == idxs.shape
        all_outputs.extend(outputs)
        all_images.extend(utils.unwraps_torch_to_numpy(images))
        all_labels.extend(labels)
        all_idxs.extend(idxs)
        # print('done minibatch', i)
    # aggregate the responses
    idxs_to_outputs = {}
    idxs_to_images = {}
    for output, image, label, idx in zip(all_outputs, all_images, all_labels, all_idxs):
        if idx not in idxs_to_outputs:
            idxs_to_outputs[idx] = []
        idxs_to_outputs[idx].append(float(output))
        idxs_to_images[idx] = image
    all_images, all_labels, all_preds = [], [], []
    stds = []
    for idx, label in enumerate(loader_val.idxs_to_labels()):
        outputs = idxs_to_outputs[idx]
        pred = statistics.mean(outputs)
        if len(outputs) > 1:
            std = statistics.stdev(outputs, pred)
        else:
            std = 0
        stds.append(std)
        all_labels.append(label)
        all_preds.append(pred)
        all_images.append(idxs_to_images[idx])

    print('max likelihood std: {0:.4f}; mean std: {1:.4f}'.format(max(stds), statistics.mean(stds)))
    all_images, all_labels, all_preds = np.asarray(all_images, dtype=np.float32),\
        np.asarray(all_labels, dtype=np.int64), np.asarray(all_preds)
    return all_images, all_labels, all_preds


if __name__ == '__main__':
    main()
