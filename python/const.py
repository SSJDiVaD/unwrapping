"""just some constants"""

import argparse
import imgaug
import math
import multiprocessing
import numpy as np
import os
from pathlib import Path
import socket
import random
import torch

# Global params set on startup--------------------------------------------------
parser = argparse.ArgumentParser(description='CNN analysis of unwrappings')
parser.add_argument('--disable-cuda', action='store_true',
                    help='Disable CUDA')
parser.add_argument('--train-new', action='store_true', help='Generate new unwrappings every epoch')
parser.add_argument('--train-on-rand', action='store_true', help='Train on the random chops instead of fixed')
parser.add_argument('--train-from-scratch', action='store_true', help='Start training from scratch instead of '
                                                                      'loading from latest checkpoint')
# parser.add_argument('--hard-mining', action='store_true', help='use hard mining for training SVM or CNN')
parser.add_argument('--small-batch', action='store_true', help='train on small batches for CNN')
parser.add_argument('--model', type=str, help='Which model to use? See nn_models.py')
parser.add_argument('--train-size', type=int, help='Which size of training dataset to use?')
parser.add_argument('--val-size', type=int, help='Which size of validation dataset to use?')
parser.add_argument('--val-vote', action='store_true', help='Use a voting scheme for validation? Else just sample '
                                                            'randomly')
parser.add_argument('--google', action='store_true', help='Train the CNN on the Google images, not the BMAX')
parser.add_argument('--synth', action='store_true', help='Train and validate the model on the synthetic images')
parser.add_argument('--aug', action='store_true', help='Augment the dataset using basic transformations during '
                                                       'training?')
parser.add_argument('--eval-image', type=int, help='(0-99) which bmax image to evaluate on? Good eval images: 17, 28, '
                                                   '45, 51, 66, 78')
parser.add_argument('--train-angles', type=int, help='number of angles to use for training')
parser.add_argument('--train-01', action='store_true', help='in the file train_cnn_two, train the 0/1+ network')
parser.add_argument('--train-12', action='store_true', help='in the file train_cnn_two, train the 1/2+ network')
parser.add_argument('--eval-weights', type=str, help='Which weights to load for evaluation')
parser.add_argument('--label-smoothing', action='store_true', help='During training, whether to use label smoothing')
args, GLOBAL_ARGS_OTHER = parser.parse_known_args()
GLOBAL_USE_CUDA = not args.disable_cuda and torch.cuda.is_available()
# if GLOBAL_USE_CUDA:
#     print('using CUDA')
# else:
#     print('NOT using CUDA')
GLOBAL_TRAIN_NEW = args.train_new
GLOBAL_TRAIN_RAND = args.train_on_rand
GLOBAL_TRAIN_SCRATCH = args.train_from_scratch
# GLOBAL_SMALL_DATASET = args.small_dataset
# GLOBAL_USE_HARD_MINING = args.hard_mining
GLOBAL_SMALL_BATCH = args.small_batch
GLOBAL_MODEL = args.model
GLOBAL_TRAIN_SIZE = args.train_size
GLOBAL_VAL_SIZE = args.val_size
GLOBAL_VAL_VOTE = args.val_vote
GLOBAL_GOOGLE = args.google
GLOBAL_SYNTH = args.synth
GLOBAL_AUG = args.aug
GLOBAL_EVAL_IMAGE = args.eval_image
GLOBAL_TRAIN_ANGLES = args.train_angles
GLOBAL_TRAIN_01 = args.train_01
GLOBAL_TRAIN_12 = args.train_12
GLOBAL_EVAL_WEIGHTS = args.eval_weights
GLOBAL_LABEL_SMOOTHING = args.label_smoothing
PRINT_TIMES = True  # whether to time and print stuff

# unwrapping params-------------------------------------------------------------
UNWRAP_RADIUS_INNER_FACTOR = 1 / 2  # to compute inner radius, radius is multiplied by this number.
UNWRAP_RADIUS_OUTER_FACTOR = 3 / 2  # To compute outer radius, radius is multiplied by this number.
UNWRAP_SHAPE = (8, 32)  # num rows, columns
UNWRAP_C = 3
UNWRAP_SHAPE_C = UNWRAP_SHAPE + (UNWRAP_C,)


# params for all BMAX training--------------------------------------------------
BMAX_FILENAME = 'data/BMAX500-symcomp17.mat'
# BMAX_FILENAME = 'data/bmax_smoothed.mat'  # this is generated by matlab/smooth_bmax.m
# BMAX_FILENAME = 'data/bmax_smoothed_one_seg.mat'  # this is generated by matlab/smooth_bmax_one_seg.m
# BMAX_FILENAME = 'data/bmax_smoothed_grayscale.mat'  # this is generated by matlab/smooth_bmax_grayscale.m
BMAX_MIN_RAD = 3  # empirically determined
BMAX_MIN_RAD = 5
# BMAX_MAX_RAD = 160  # empirically determined
BMAX_MAX_RAD = 80  # for now, we should just not use the maximum radius because 160 is too big.
# in order to reject negative training examples which are too close to medials,
# we reject points which are approx 1% of the diagonal length of the volume of h, w, radiuses
# BMAX_MEDIAL_DIST_THRESHOLD = 0.01 * math.sqrt((350 ** 2) + (350 ** 2) + (BMAX_MAX_RAD - BMAX_MIN_RAD + 1) ** 2)
BMAX_MEDIAL_DIST_THRESHOLD = 1.5  # "wiggle room" of what's considered medial
BMAX_NON_MEDIAL_RAD_THRESHOLD = BMAX_MEDIAL_DIST_THRESHOLD  # the difference between a nonmedial
# and unitangent radius must be bigger than this number
BMAX_POS_FACTOR = 1. / (1. + 2. + 6.)  # rough proportion of positive training examples. Actual will be less.
BMAX_UNI_FACTOR = 2. / (1. + 2. + 6.)  # rough proportion of unitangent negative training examples.
BMAX_VAL_POS_FACTOR = 1. / (1. + 5. + 100.)
BMAX_VAL_UNI_FACTOR = 5. / (1. + 5. + 100.)
# Whatever isn't a positive or unitangent negative will be a random negative.
assert BMAX_POS_FACTOR + BMAX_UNI_FACTOR < 1.
BMAX_NUM_TRAIN = 176
BMAX_UNWRAPPED_DIR = 'data/unwraps'
BMAX_UNWRAPPED_FILENAME = BMAX_UNWRAPPED_DIR + '/bmax_{suffix}.{ext}'
BMAX_SPLIT_DIR = 'data/split'
BMAX_TRAIN_FILENAME = BMAX_SPLIT_DIR + '/bmax_train.hkl'
BMAX_VAL_FILENAME = BMAX_SPLIT_DIR + '/bmax_val.hkl'


# params for synthetic training and evaluation----------------------------------
SYNTH_MIN_RAD = 6
SYNTH_MAX_RAD = 30
# in order to reject negative training examples which are too close to medials,
# we reject points which are 1% of the diagonal length of the volume of h, w, radiuses
SYNTH_MEDIAL_DIST_THRESHOLD = 0.01 * math.sqrt((100 ** 2) + (100 ** 2) + (SYNTH_MAX_RAD - SYNTH_MIN_RAD + 1) ** 2)

# SYNTH_POS_FACTOR = 0.3
# SYNTH_UNI_FACTOR = 0.25
SYNTH_UNI_FACTOR = 2.  # we will use the number of true positives * this number to get the number of unitangent
# examples
SYNTH_NEG_FACTOR = 4.  # we will use the number of true positives * this number to get the numer of random negatives
if GLOBAL_TRAIN_ANGLES is None:
    SYNTH_TRAIN_NUM_ANGLES = 1
else:
    SYNTH_TRAIN_NUM_ANGLES = GLOBAL_TRAIN_ANGLES
SYNTH_TRAIN_ANGLES = np.linspace(0, 360, SYNTH_TRAIN_NUM_ANGLES, endpoint=False)
# original image, processed hickle, unwraps for training/val, and number of clusters
SYNTH_TRAIN = {
    'data/rectangle.png': ('data/rectangle_processed.hkl', 'data/rectangle_unwraps.hkl', 2),
    'data/rectangle_90.png': ('data/rectangle_90_processed.hkl', 'data/rectangle_90_unwraps.hkl', 2),
    'data/rectangle_15.png': ('data/rectangle_15_processed.hkl', 'data/rectangle_15_unwraps.hkl', 2),
    'data/rectangle_45.png': ('data/rectangle_45_processed.hkl', 'data/rectangle_45_unwraps.hkl', 2),
    'data/rectangle_crescent.png': ('data/rectangle_crescent_processed.hkl', 'data/rectangle_crescent_unwraps.hkl', 2),

    'data/rectangle_fat.png': ('data/rectangle_fat_processed.hkl', 'data/rectangle_fat_unwraps.hkl', 2),
    # 'data/rectangle_thin.png': ('data/rectangle_thin_processed.hkl', 'data/rectangle_thin_unwraps.hkl', 2),
    'data/rectangle_wedge.png': ('data/rectangle_wedge_processed.hkl', 'data/rectangle_wedge_unwraps.hkl', 2),
    'data/ellipse_rectangle.png': ('data/ellipse_rectangle_processed.hkl', 'data/ellipse_rectangle_unwraps.hkl', 3),
    'data/ellipse.png': ('data/ellipse_processed.hkl', 'data/ellipse_unwraps.hkl', 2),

    'data/rectangle_red.png': ('data/rectangle_red_processed.hkl', 'data/rectangle_red_unwraps.hkl', 2),
    'data/rectangle_gray.png': ('data/rectangle_gray_processed.hkl', 'data/rectangle_gray_unwraps.hkl', 2),
    'data/rectangle_ultramarine.png': ('data/rectangle_ultramarine_processed.hkl',
                                       'data/rectangle_ultramarine_unwraps.hkl', 2),
    'data/rectangle_inverted.png': ('data/rectangle_inverted_processed.hkl', 'data/rectangle_inverted_unwraps.hkl', 2),
    'data/rectangle_bw.png': ('data/rectangle_bw_processed.hkl', 'data/rectangle_bw_unwraps.hkl', 2),

    'data/rectangles_red.png': ('data/rectangles_red_processed.hkl', 'data/rectangles_red_unwraps.hkl', 11),
    'data/triangles.png': ('data/triangles_processed.hkl', 'data/triangles_unwraps.hkl', 2),
    'data/rectangles_bwrgb.png': ('data/rectangles_bwrgb_processed.hkl', 'data/rectangles_bwrgb_unwraps.hkl', 5),
    'data/combo.png': ('data/combo_processed.hkl', 'data/combo_unwraps.hkl', 6),
}


# params for all Google logo training-------------------------------------------
GOOGLE_FILENAME = 'data/google.jpg'
GOOGLE_PROCESSED_FILENAME = 'data/google_processed.hkl'
# GOOGLE_FILENAME = 'data/google_text.png'
GOOGLE_CHROME_FILENAME = 'data/chrome.png'
GOOGLE_CHROME_PROCESSED_FILENAME = 'data/chrome_processed.hkl'
GOOGLE_UNWRAPPED_DIR = BMAX_UNWRAPPED_DIR
GOOGLE_UNWRAPPED_FILENAME = BMAX_UNWRAPPED_DIR + '/google_{suffix}.hkl'
GOOGLE_TRAIN_SIZE = 8192  # approx number of training unwrappings
GOOGLE_VAL_SIZE = 1024  # approx number of validation unwrappings
GOOGLE_POS_FACTOR = 0.25  # approximate fraction of positive training examples
GOOGLE_UNI_FACTOR = 0.25  # approximate fraction of unitangent negative training examples
if GLOBAL_TRAIN_ANGLES is None:
    GOOGLE_FIXED_CUTS_PER_PIXEL = 64  # number of cuts for every pixel selected
else:
    GOOGLE_FIXED_CUTS_PER_PIXEL = GLOBAL_TRAIN_ANGLES
GOOGLE_FIXED_CUT_ANGLES = np.linspace(0, 360, GOOGLE_FIXED_CUTS_PER_PIXEL, endpoint=False)
GOOGLE_MIN_RAD = 10
GOOGLE_MAX_RAD = 80


# SVM training params-----------------------------------------------------------
SVM_DIR = 'data/models_svm'
SVM_BEST_FILENAME = SVM_DIR + '/best_{suffix}.hkl'
# read more about HOG params in the documentation of skimage.feature.hog.
HOG_CELL_SIZE = (4, 4)
HOG_CELLS_PER_BLOCK = (2, 2)  # had to decrease this from (3, 3) when decreasing the size of the unwrapping
HOG_NUM_ORIENTATIONS = 9
HOG_REGULARIZATION = 'L2'  # empirically determined to give best CV accuracy


# training params---------------------------------------------------------------
if GLOBAL_SMALL_BATCH:
    TRAIN_BATCH_SIZE = 8
else:
    TRAIN_BATCH_SIZE = 256
if GLOBAL_TRAIN_ANGLES is None:
    TRAIN_FIXED_CUTS_PER_PIXEL = 1  # number of cuts for every pixel selected
else:
    TRAIN_FIXED_CUTS_PER_PIXEL = GLOBAL_TRAIN_ANGLES
# print('training on {0} cuts per unwrap'.format(TRAIN_FIXED_CUTS_PER_PIXEL))
TRAIN_FIXED_CUT_ANGLES = np.linspace(0, 360, TRAIN_FIXED_CUTS_PER_PIXEL, endpoint=False)
TRAIN_FIXED_VOTES_PER_PIXEL = 8  # number of cuts for voting
TRAIN_FIXED_VOTES_ANGLES = np.linspace(0, 360, TRAIN_FIXED_VOTES_PER_PIXEL, endpoint=False)
TRAIN_NUM_EPOCHS = 1000
TRAIN_LEARNING_RATE = 1e-2  # PyTorch default is 0.001
TRAIN_WEIGHT_DECAY = 1e-4
TRAIN_LOSS_IMPROVE_THRESHOLD = 0.0001  # any relative improvement in training loss below this threshold is considered
# trivial
TRAIN_SCHED_FACTOR = 0.1  # after patience is exhausted, decrease learning rate by this factor
TRAIN_SCHED_PATIENCE = 3  # number of epochs to wait for improvement before decreasing learning rate
TRAIN_DIR = 'data/models_cnn'
TRAIN_LATEST_FILENAME = TRAIN_DIR + '/latest_{size}_{angle}_{mining}.pt'
TRAIN_SECOND_LATEST_FILENAME = TRAIN_DIR + '/latest_2_{size}_{angle}_{mining}.pt'
TRAIN_BEST_FILENAME = TRAIN_DIR + '/best_{suffix}.pt'
TRAIN_POS_LABEL = 2
TRAIN_UNI_LABEL = 1
TRAIN_NEG_LABEL = 0
# TRAIN_F_BETA = 0.3  # the beta used in the f beta measure for training and validation
TRAIN_CLASS_WEIGHTS = [1, 1, 1]  # loss weights for classes random negative, unitangent negative, and positive

# hard mining params------------------------------------------------------------
# if GLOBAL_SMALL_DATASET:
#     MINING_NUM = 10000  # number of unwrappings in the hard mining dataset, approx
# else:
#     MINING_NUM = 100000
# MINING_BATCH_SIZE = MINING_NUM // 10
# MINING_IMAGES_PER_BATCH = 10  # draw from this many different images every mini-batch
# if GLOBAL_TRAIN_RAND:
#     MINING_PIXELS_PER_IMAGE = int(MINING_BATCH_SIZE / MINING_IMAGES_PER_BATCH / BMAX_FIXED_CUTS_PER_PIXEL
#                                   * BMAX_FIXED_CUTS_PER_PIXEL)
# else:
#     MINING_PIXELS_PER_IMAGE = int(MINING_BATCH_SIZE / MINING_IMAGES_PER_BATCH / BMAX_FIXED_CUTS_PER_PIXEL)

# eval: test the CNN on an entire image-----------------------------------------
EVAL_UNWRAP_DIR = Path('data/unwraps_eval')
EVAL_UNWRAP_FILENAME = str(EVAL_UNWRAP_DIR / 'bmax_test_{0}_{1}.hkl')
if GLOBAL_EVAL_IMAGE is None:
    GLOBAL_EVAL_IMAGE = 78
if GLOBAL_GOOGLE:
    EVAL_MIN_RAD = 6  # the lowest radius we'll try to evaluate on
    EVAL_MAX_RAD = 57  # the highest radius we'll try to evaluate on
elif GLOBAL_SYNTH:
    EVAL_MIN_RAD = SYNTH_MIN_RAD
    EVAL_MAX_RAD = SYNTH_MAX_RAD
else:
    if socket.gethostname() == 'OhDear':
        EVAL_MIN_RAD = 5
        EVAL_MAX_RAD = 80
        # print('because we\'re on the home computer, using small radiuses of {0} and {1}'.format(
        #     EVAL_MIN_RAD, EVAL_MAX_RAD))
    else:
        EVAL_MIN_RAD = 5
        EVAL_MAX_RAD = 80

# EVAL_BORDER = 2  # number of extra pixels away from the border to start searching.
EVAL_STRIDE = 1
EVAL_RAD_STRIDE = 1
EVAL_BATCH_SIZE = 256
# EVAL_VOTING_NUM = 8  # number of angles to use for voting
EVAL_VOTING_NUM = 1
EVAL_VOTING_ANGLES = np.linspace(0, 360, EVAL_VOTING_NUM, endpoint=False)
if GLOBAL_GOOGLE:
    EVAL_THRESHOLD = 0.50
elif GLOBAL_SYNTH:
    EVAL_THRESHOLD = 0.50
else:
    # EVAL_THRESHOLD = 0.95  # on BMAX image 45, too high. No true positives
    # EVAL_THRESHOLD = 0.7853  # on BMAX image 45, precision 95.00% and recall 84.76%
    EVAL_THRESHOLD = 1. / 3.
EVAL_CONVNET_SMOOTH_SIGMA = 2.
EVAL_SVM_SMOOTH_SIGMA = 2.

# Params for evaluating the two-problem approach--------------------------------
# commented because we're no longer using the two-problem approach
# EVAL_01_THRESHOLD = 0.35
# EVAL_12_THRESHOLD = 0.35

# Cnn params--------------------------------------------------------------------
CNN_CONV_DROPOUT_P = 0.1
CNN_FC_DROPOUT_P = 0.5
CNN_FC_NUM = 1024
CNN_KERNEL_SIZE = 3
CNN_PADDING = (CNN_KERNEL_SIZE - 1) // 2
CNN_DECISION_BOUNDARY = 0  # this depends on the final layer. Eg. should be 0.5 if logistic final layer
CNN_GRAYSCALE = False  # Whether or not we're in grayscale mode

# misc--------------------------------------------------------------------------
# if MINING_PIXELS_PER_IMAGE == 0:
#     print('warning: MINING_NUM is set too small')
# logical_core_count = multiprocessing.cpu_count()
# print('setting number of torch CPU threads to', logical_core_count)
# torch.set_num_threads(logical_core_count)
EPS = 10 ** -9  # just a really small number
EPS_BIG = 10 ** -2
assert not GLOBAL_USE_CUDA or ('CUDA_VISIBLE_DEVICES' in os.environ)  # if using CUDA, make sure we've specified a GPU
