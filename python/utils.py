"""Just some utils"""

import math
import pickle
import random
# import sys
import time

import hickle
import imgaug
# from matplotlib import pyplot as plt
import numpy as np
from scipy import ndimage
import scipy.io
import skimage.feature
import torch

from python import const
from python import nn_models


class Timer:
    def __init__(self, name=None):
        if name is None:
            name = 'operation'
        self.name = name

    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *_):
        self.end = time.clock()
        if const.PRINT_TIMES:
            print('{0} took {1:.2f}s'.format(self.name, self.end - self.start))


def round_int(x):
    """Return the integer rounding of x"""
    return int(x + 0.5)


def load_bmax_train():
    """Return the chunk of BMAX500 marked by gen_unwrappings.split_bmax as training images.

    You MUST have run gen_unwrappings before using this function!
    """
    bmax = scipy.io.loadmat(const.BMAX_FILENAME)
    train_images = hickle.load(const.BMAX_TRAIN_FILENAME)
    bmax = bmax['BMAX500'][0, 0]
    bmax_train = bmax['train'][0]
    bmax_train = bmax_train[train_images]
    return bmax_train


def load_bmax_val():
    """Return the chunk of BMAX500 marked by gen_unwrappings.split_bmax as val images.

    You MUST have run gen_unwrappings before using this function!
    """
    bmax = scipy.io.loadmat(const.BMAX_FILENAME)
    val_images = hickle.load(const.BMAX_VAL_FILENAME)
    bmax = bmax['BMAX500'][0, 0]
    bmax_train = bmax['train'][0]
    bmax_val = bmax_train[val_images]
    return bmax_val


def load_bmax_test():
    """Return the test split of BMAX500.
    """
    bmax = scipy.io.loadmat(const.BMAX_FILENAME)
    bmax_test = bmax['BMAX500'][0, 0]['val'][0]
    return bmax_test


def logistic(x, x_0=0, L=1, k=1):
    """Just the logistic function on x with params as defined on Wikipedia."""
    return L / (1 + np.exp(-k * (x - x_0)))


def unwrap(img, ys, xs, rads, angles=None):
    """Unwrap image img (MxNx3, floating point format in interval [0, 1]) into an annulus at the points specified by the
    other params. We assume xs, ys uses numpy conventions, so [0, 0, :] represents the top-leftmost pixel.
    Return value has dimension num_points * M * N * 3"""
    assert img.dtype == np.float32
    # assert np.max(img) <= 1. + const.EPS_BIG
    # assert np.min(img) >= 0. - const.EPS_BIG
    img_max, img_min = np.max(img), np.min(img)
    if img_max >= 1. + const.EPS_BIG:
        print('warning: max of image is greater than 1. Max:', img_max)
    if img_min <= 0 - const.EPS_BIG:
        print('warning: min of image is less than 0. Min:', img_min)
    if angles is None:
        angles = np.zeros(ys.shape)
    inner_rads, outer_rads = rads_2_inner_outer(rads)
    return unwrap_specific_optimized(img, ys, xs, inner_rads, outer_rads, angles, const.UNWRAP_SHAPE)


def unwrap_specific(img, ys, xs, inner_rads, outer_rads, angles, unwrap_shape):
    """A version of the above function where the radiuses and the unwrap shape themselves can be
    specified"""
    H, W, C = img.shape
    assert C == 3
    num_points = ys.shape[0]
    assert (ys.shape == xs.shape == inner_rads.shape == outer_rads.shape == angles.shape == (num_points,))
    angles_offset = np.linspace(0, 360, unwrap_shape[1], False)
    result_shape = (num_points,) + unwrap_shape + (C,)  # num_points*shape[0]*shape[1]*C
    result = np.zeros(result_shape, dtype=np.float32)
    for i in range(num_points):
        rads_point = np.linspace(outer_rads[i], inner_rads[i], unwrap_shape[0])
        angles_point = (angles[i] + angles_offset) * math.pi / 180
        assert len(rads_point.shape) == len(angles_point.shape) == 1  # both of them are vectors
        # print('rads_point:', rads_point)
        # print('angles_point:', angles_point)
        angles_point, rads_point = np.meshgrid(angles_point, rads_point)
        # print('rads_point:', rads_point)
        # print('angles_point:', angles_point)
        xs_point, ys_point = pol2cart(rads_point, angles_point)
        xs_point += xs[i]
        ys_point += ys[i]
        assert (angles_point.shape == rads_point.shape == xs_point.shape
                == ys_point.shape == unwrap_shape)
        unwrapping = np.zeros(unwrap_shape + (C,))
        for channel in range(C):
            unwrap_channel = ndimage.map_coordinates(img[:, :, channel], [ys_point, xs_point], order=1)
            assert unwrap_channel.shape == unwrap_shape
            unwrapping[:, :, channel] = unwrap_channel
        assert unwrapping.shape == unwrap_shape + (C,)
        result[i] = unwrapping
    assert result.shape == result_shape
    return result


def unwrap_specific_optimized(img, ys, xs, inner_rads, outer_rads, angles, unwrap_shape):
    """An optimized version of the above function"""
    if len(img.shape) == 3:  # multi-channel (eg. RGB) format
        num_channels = img.shape[2]
        img = np.transpose(img, (2, 0, 1))  # for speed
    elif len(img.shape) == 2:  # grayscale format
        num_channels = 1
        img = np.expand_dims(img, 0)
    else:
        raise ValueError('img.shape: {0}'.format(img.shape))
    num_points = ys.shape[0]
    assert (ys.shape == xs.shape == inner_rads.shape == outer_rads.shape == angles.shape == (num_points,))
    angles_offset = np.linspace(0, 360, unwrap_shape[1], False)
    result_shape = (num_points, num_channels) + unwrap_shape   # order tweaked for speed
    result = np.empty(result_shape, np.float32)
    degs_to_rads = math.pi / 180.
    for i in range(num_points):
        rads_point = np.linspace(outer_rads[i], inner_rads[i], unwrap_shape[0])
        angles_point = (angles[i] + angles_offset) * degs_to_rads
        assert len(rads_point.shape) == len(angles_point.shape) == 1  # both of them are vectors
        angles_point, rads_point = np.meshgrid(angles_point, rads_point)
        xs_point, ys_point = pol2cart(rads_point, angles_point)
        xs_point += xs[i]
        ys_point += ys[i]
        assert (angles_point.shape == rads_point.shape == xs_point.shape == ys_point.shape == unwrap_shape)
        for channel in range(num_channels):
            unwrap_channel = ndimage.map_coordinates(img[channel, :, :], [ys_point, xs_point], order=1)
            assert unwrap_channel.shape == unwrap_shape
            result[i, channel, :, :] = unwrap_channel
    assert result.shape == result_shape
    result = np.transpose(result, (0, 2, 3, 1))
    assert result.shape == (num_points,) + unwrap_shape + (num_channels,)
    return result


def unwrap_specific_no_angle(img, ys, xs, inner_rads, outer_rads, angles, unwrap_shape):
    """A version of the above function that assumes all angles are 0"""
    if not np.all(angles == 0):
        print('warning: used unwrap_specific_no_angle on non-zero angles')
    if len(img.shape) == 3:  # multi-channel (eg. RGB) format
        num_channels = img.shape[2]
        img = np.transpose(img, (2, 0, 1))  # for speed
    elif len(img.shape) == 2:  # grayscale format
        num_channels = 1
        img = np.expand_dims(img, 0)
    else:
        raise ValueError('img.shape: {0}'.format(img.shape))
    num_points = ys.shape[0]
    assert (ys.shape == xs.shape == inner_rads.shape == outer_rads.shape == angles.shape == (num_points,))
    angles_offset = np.linspace(0, 360, unwrap_shape[1], False)
    result_shape = (num_points, num_channels) + unwrap_shape   # order tweaked for speed
    result = np.empty(result_shape, np.float32)
    degs_to_rads = math.pi / 180.
    for i in range(num_points):
        rads_point = np.linspace(outer_rads[i], inner_rads[i], unwrap_shape[0])
        angles_point = (angles[i] + angles_offset) * degs_to_rads
        assert len(rads_point.shape) == len(angles_point.shape) == 1  # both of them are vectors
        angles_point, rads_point = np.meshgrid(angles_point, rads_point)
        xs_point, ys_point = pol2cart(rads_point, angles_point)
        xs_point += xs[i]
        ys_point += ys[i]
        assert (angles_point.shape == rads_point.shape == xs_point.shape == ys_point.shape == unwrap_shape)
        result_point = np.empty((num_channels,) + unwrap_shape, np.float32)
        for channel in range(num_channels):
            unwrap_channel = ndimage.map_coordinates(img[channel, :, :], [ys_point, xs_point], order=1)
            assert unwrap_channel.shape == unwrap_shape
            result_point[channel, :, :] = unwrap_channel
        result[i, :, :, :] = result_point
    assert result.shape == result_shape
    result = np.transpose(result, (0, 2, 3, 1))
    assert result.shape == (num_points,) + unwrap_shape + (num_channels,)
    return result


def get_labels(img, ys, xs, rads):
    """Params are the same as those of unwrap.

    For each point in the given image, determine whether the point is medial or
    not. Return a numpy array of bools.
    """
    results = []
    for y, x, rad in zip(ys, xs, rads):
        results.append(np.any(img['seg'][y, x, :]) and np.any(img['rad'][y, x, :] == rad))
    return np.asarray(results)


def pol2cart(rho, phi):
    """Given polar coords (numpy arrays), return cartesian coords. Taken from:
    https://stackoverflow.com/questions/20924085/python-conversion-between-coordinates
    """
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return x, y


def rads_2_inner_outer(rads):
    """Given a numpy array of radiuses, convert them to inner and outer radiuses and return the two separate arrays"""
    inner_rads = rads * const.UNWRAP_RADIUS_INNER_FACTOR
    outer_rads = rads * const.UNWRAP_RADIUS_OUTER_FACTOR
    assert rads.shape == inner_rads.shape == outer_rads.shape
    return inner_rads, outer_rads


def overlap(a, b):
    """Given two numpy arrays, find the indices of the intersection between a
    and b along the first dimension.

    Return a tuple:
        -iterator of indices of elements in a, sorted
        -iterator of indices of elements in b, sorted
        -the intersection values themselves
    """
    with Timer('overlap calculation'):
        length_a, length_b = a.shape[0], b.shape[0]
        twod_a, twod_b = np.reshape(a, (length_a, -1)), np.reshape(b, (length_b, -1))
        del a, b
        set_a, set_b = set((tuple(x) for x in twod_a)), set((tuple(x) for x in twod_b))
        values = set_a & set_b
        del set_a, set_b
        in_a, in_b = set(), set()
        for value in values:
            for i, row in enumerate(twod_a):
                if value == tuple(row):
                    in_a.add(i)
                    # print('match found in a at index', i)
            for i, row in enumerate(twod_b):
                if value == tuple(row):
                    in_b.add(i)
                    # print('match found in b at index', i)
        in_a, in_b = sorted(in_a), sorted(in_b)
        return in_a, in_b


def init_mining_dataset(bmax_train):
    """Generate an initial mining dataset consisting entirely of unwrappings of random points from the training set.
    Param bmax_train is as returned by utils.load_bmax_train.
    Return value is a numpy array of unwrappings and corresponding labels.
    """
    all_unwraps, all_labels = [], []
    for _ in range(0, const.MINING_NUM, const.MINING_BATCH_SIZE):
        unwraps, labels = gen_mining_batch(bmax_train)
        all_unwraps.append(unwraps)
        all_labels.append(labels)
    all_unwraps, all_labels = np.concatenate(all_unwraps, axis=0), np.concatenate(all_labels, axis=0)
    return all_unwraps, all_labels


def gen_mining_batch(bmax_train):
    """Generate a hard mining batch of unwrappings and labels and return it as a tuple:
        -unwrappings have format like those generated by unwrap()
        -labels are a numpy vector of labels as they correspond to each unwrap
    """
    all_unwraps, all_labels = [], []
    image_indices = random.sample(range(bmax_train.shape[0]), const.MINING_IMAGES_PER_BATCH)
    sz = (const.MINING_PIXELS_PER_IMAGE,)
    for index in image_indices:
        img = bmax_train[index]
        h, w, _ = img['img'].shape
        # select a random point
        ys = np.random.randint(h, size=sz)
        xs = np.random.randint(w, size=sz)
        rads = np.random.randint(const.BMAX_MIN_RAD, const.BMAX_MAX_RAD, size=sz)
        if const.GLOBAL_TRAIN_RAND:
            angles = np.random.random(size=sz) * 360
        else:
            ys, angles = np.meshgrid(ys, const.BMAX_FIXED_CUT_ANGLES)
            xs, _ = np.meshgrid(xs, const.BMAX_FIXED_CUT_ANGLES)
            rads, _ = np.meshgrid(rads, const.BMAX_FIXED_CUT_ANGLES)
            ys, xs, rads, angles = ys.flatten(), xs.flatten(), rads.flatten(), angles.flatten()
        assert ys.shape == xs.shape == rads.shape == angles.shape
        assert len(ys.shape) == 1  # is a vector
        unwraps = unwrap(img['img'], ys, xs, rads, angles)
        all_unwraps.append(unwraps)
        labels = get_labels(img, ys, xs, rads)
        all_labels.append(labels)
    all_unwraps, all_labels = np.concatenate(all_unwraps, axis=0), np.concatenate(all_labels, axis=0)
    return all_unwraps, all_labels


def update_mining_set(bmax_train, probs, labels):
    """Given probabilities and labels which correspond to the current mining dataset, select unwrappings to replace
    and return a tuple:
        -a np array listing the indices of the current mining dataset to replace
        -the new unwrappings with which to replace the old ones
        -the labels corresponding to the unwrappings
    """
    new_unwraps_mining, new_labels_mining = gen_mining_batch(bmax_train)
    num_new_unwraps = new_labels_mining.shape[0]
    losses_mining = np.abs(probs - labels)
    # sort by loss
    argsorted_losses = sorted(range(len(losses_mining)), key=(lambda i: losses_mining[i]))
    # losses_mining = [losses_mining[i] for i in indexes]
    # weights = np.asarray([1.1 ** i for i in range(len(losses_mining))])  # the lower the loss, the more likely we'll
    # weights = weights / np.sum(weights)  # replace it
    # cut_indexes = np.random.choice(indexes, (num_new_unwraps,), replace=False, p=weights)
    cut_indexes = np.asarray(argsorted_losses[:num_new_unwraps])
    assert cut_indexes.shape[0] == new_unwraps_mining.shape[0] == new_labels_mining.shape[0]
    return cut_indexes, new_unwraps_mining, new_labels_mining


def shift_unwraps(unwraps, angles):
    """If unwraps is an array of n unwrappings (each unwrapping is hxwxc), and angles is an array of n angles (in
    degrees), shift the unwraps by their corresponding angles and return the new unwraps"""
    num_data, h, w, c = unwraps.shape
    assert angles.shape == (num_data,)
    pixels_shift = angles / 360 * w  # for each unwrap, how many pixels are we gonna shift it over
    # horizontally?
    assert pixels_shift.shape == (num_data,)
    new_unwraps = np.zeros(unwraps.shape)
    # with utils.Timer('shifting {0} unwraps'.format(num_data)):
    for i, shift in enumerate(pixels_shift):
        new_idxs = np.arange(0, w)
        new_idxs = ((new_idxs + shift) % w).astype(np.int)  # new indexes now contains a shifted list of indexes
        # for a given unwrapping
        old_unwrap = unwraps[i, :, :, :]
        new_unwrap = old_unwrap[:, new_idxs, :]
        assert old_unwrap.shape == new_unwrap.shape == (h, w, c)
        new_unwraps[i, :, :, :] = new_unwrap
    assert new_unwraps.shape == unwraps.shape == (num_data, h, w, c)
    return new_unwraps


def unwraps_to_torch(unwraps):
    """Given unwraps in classifier-agnostic format (as returned by unwrap()), return a torch tensor version
    with the following modifications:
        -transposed to batch x c x h x w order
        -converted to float32
        -contiguous
        -cuda'd if const.GLOBAL_USE_CUDA
    """
    result = torch.from_numpy(np.transpose(unwraps, (0, 3, 1, 2)).astype(np.float32)).contiguous()
    if const.GLOBAL_USE_CUDA:
        result = result.cuda()
    return result


def unwraps_torch_to_numpy(unwraps):
    """Reverse of unwraps_to_torch"""
    return np.transpose(unwraps.cpu().numpy(), (0, 2, 3, 1))


def unwraps_to_hogs(unwraps):
    result = []
    for unwrap in unwraps:
        hog = skimage.feature.hog(unwrap, const.HOG_NUM_ORIENTATIONS, const.HOG_CELL_SIZE, const.HOG_CELLS_PER_BLOCK,
                                  const.HOG_REGULARIZATION, feature_vector=True, multichannel=True)
        result.append(hog)
    return np.asarray(result)


def labels_to_torch(labels):
    """Given labels in classifier-agnostic format (as returned by unwrap()), return a torch tensor version with the
    following modifications:
        -converted to long
        -converted to n x 1 matrix from n-vector
        -contiguous
        -cuda'd if const.GLOBAL_USE_CUDA
    """
    result = torch.from_numpy(labels).long().contiguous()
    if const.GLOBAL_USE_CUDA:
        result = result.cuda()
    return result


def img_uint8_to_float32(img):
    """Return a version of img (which is in np.uint8 format) in a np.float32 version, values ranging
    from 0 to 1 instead of 0 to 255
    """
    assert img.dtype == np.uint8
    return img.astype(np.float32) / 255.


def img_float32_to_uint8(img):
    """Retusn a version of img (which is in np.float32 format) in a np.uint8 version, values ranging
     from 0 to 255 instead of 0 to 1.
     """
    assert img.dtype == np.float32
    return (img * 255.).astype(np.uint8)


def svm_eval_likelihoods(svm, hogs):
    """Eval likelihoods on the given hogs using the given svm"""
    confidences = svm.decision_function(hogs)
    # likelihoods = logistic(confidences, k=4)
    likelihoods = scipy.special.softmax(confidences, axis=1)
    assert likelihoods.shape[1] == 3  # three-class classification
    return likelihoods


# optimized version
def dist_to_nearest_medial(point_rad, all_medials):
    """Treat y, x, and radius as a 3d volume. Calculate the euclidean distance
    between point_rad and the nearest point in all_medials (an iterable of
    (y, x, rad))"""
    point_rad, all_medials = np.expand_dims(np.asarray(point_rad), 0), np.asarray(all_medials)
    # multi-line version
    # diffs = all_medials - point_rad
    # norms = np.linalg.norm(diffs, axis=1)
    # smallest_norm = np.min(norms)
    # return smallest_norm

    # one-line version
    return np.min(np.linalg.norm(all_medials - point_rad, axis=1))


def distance_nearest_boundary(bmax_datum_bnd, y, x):
    """Given a point (y, x), and the 'bnd' of a bmax datum, return the euclidean distance to
    the nearest boundary rounded to the nearest integer."""
    h, w, num_segs = bmax_datum_bnd.shape
    bmax_datum_bnd_all = np.any(bmax_datum_bnd, axis=2)
    assert bmax_datum_bnd_all.shape == (h, w)
    boundaries = np.argwhere(bmax_datum_bnd_all)
    num_boundaries = boundaries.shape[0]
    assert boundaries.shape == (num_boundaries, 2)
    point = np.expand_dims(np.asarray((y, x)), 0)
    assert point.shape == (1, 2)
    diffs = boundaries - point
    assert diffs.shape == (num_boundaries, 2)
    dists = np.linalg.norm(diffs, axis=1)
    assert dists.shape == (num_boundaries,)
    return np.min(dists)


def batch_generator(array_length, batch_size):
    """Given an array length, yields minibatches of arrays of indexes at a time
    no bigger than batch_size"""
    # i = 0
    # while True:
    #     if array_length - i > batch_size:
    #         yield np.arange(i, i + batch_size)
    #         i += batch_size
    #     else:
    #         yield np.arange(i, array_length)
    #         break
    # raise StopIteration
    i = 0
    while array_length - i > batch_size:
        yield np.arange(i, i + batch_size)
        i += batch_size
    yield np.arange(i, array_length)


def rgb2gray_matlab(image):
    """Returns a copy of the RGB image (h * w * c) converted to grayscale according to the same
    formula used by MATLAB.
    https://www.mathworks.com/help/matlab/ref/rgb2gray.html
    """
    h, w, c = image.shape
    assert c == 3
    result = 0.299 * image[:, :, 0] + 0.587 * image[:, :, 1] + 0.114 * image[:, :, 2]
    return result


def gray2rgb(image):
    """Returns a copy of the image converted to RGB. Does nothing if already RGB.

    Args:
        image: a h * w or h * w * 1 image

    Raises:
        ValueError if image is not of the correct shape
    """
    orig_shape = image.shape
    if len(image.shape) == 2:
        image = image[:, :, np.newaxis]
    h, w, c = image.shape
    if c == 1:
        image = np.repeat(image, (1, 1, 3))
    if c != 3:
        raise ValueError('Incorrect image dimensions: {0}'.format(orig_shape))
    return image


def load_convnet_pretrained(filename, no_gpu=False):
    """Returns the pretrained convnet identified by the given filename. Such files are generated by
    train_cnn.py.

    Convnet is set to eval mode. Uses CUDA or CPU as appropriate.
    """
    if const.GLOBAL_USE_CUDA and not no_gpu:
        map_location = 'cuda:0'
    else:
        map_location = 'cpu'
    state_dict, model, *_ = torch.load(filename, map_location)
    print('ignored attributes {0} when loading CNN'.format(_))
    cnn = nn_models.get_model(model, no_gpu)
    cnn.load_state_dict(state_dict)
    cnn.eval()
    return cnn, model


def load_svm_pretrained(filename):
    """Returns the pretrained SVM and HOG scaler (as a tuple) identified by the given filename.

    Such files are generated by train_svm.py.
    """
    with open(filename, 'rb') as f:
        svm, scaler, *_ = pickle.load(f)
    return svm, scaler


def gaussian_smooth(img, sigma):
    """Given a h * w * c ndimage, returns a smoothed copy (but only in the h and w dimensions)"""
    img = scipy.ndimage.gaussian_filter1d(img, sigma, axis=0, mode='constant')
    img = scipy.ndimage.gaussian_filter1d(img, sigma, axis=1, mode='constant')
    return img


def seed_randoms(seed=0):
    """Seeds all PRNGs:
    """
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    imgaug.seed(seed)
