"""Same as eval models, except ground truth instead of classifier evaluation"""

import hickle
# from matplotlib import pyplot
import numpy as np
import scipy.misc

from python import const
from python import utils


def main():
    """Same as eval_models, but ground truth instead"""
    PRED_FRACTION = 0.75  # fraction of the resulting image that will be "pred". The rest will be the image itself.
    if const.GLOBAL_GOOGLE:
        img_gt, img_skel_gt, img_rad_gt = hickle.load(const.GOOGLE_CHROME_PROCESSED_FILENAME)
        print('loaded Google Chrome logo')
        image_suffix = 'google'
    elif const.GLOBAL_SYNTH:
        img_gt, img_skel_gt, img_rad_gt = hickle.load(const.SYNTH_TRAIN[15][1])
        print('loaded synthetic image')
        image_suffix = 'synth_reds'
    else:
        bmax_datum = utils.load_bmax_test()[const.GLOBAL_EVAL_IMAGE]
        img_gt = utils.img_uint8_to_float32(bmax_datum['img'])
        img_rad_gt = bmax_datum['rad']
        img_skel_gt = bmax_datum['pts']
        print('loaded bmax image', const.GLOBAL_EVAL_IMAGE)
        image_suffix = 'bmax_{0}'.format(const.GLOBAL_EVAL_IMAGE)

    h, w, c = img_gt.shape
    for radius in range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE):
        all_preds = np.zeros((h, w, c))
        for y in range(0, h, const.EVAL_STRIDE):
            for x in range(0, w, const.EVAL_STRIDE):
                if len(img_rad_gt.shape) == 3:
                    rad_correct = np.logical_and(img_rad_gt[y, x, :] >= radius - const.EPS,
                                                 img_rad_gt[y, x, :] < radius + const.EVAL_RAD_STRIDE)
                    correct = np.logical_and(rad_correct, img_skel_gt[y, x] > 0)
                    pred = np.any(correct)
                else:
                    pred = (img_rad_gt[y, x] >= radius - const.EPS
                            and img_rad_gt[y, x] < radius + const.EVAL_RAD_STRIDE
                            and img_skel_gt[y, x] > 0)
                # print(pred)
                # try:
                #     if len(pred) > 1:
                #         pass
                # except TypeError:
                #     pass
                all_preds[y, x, :] = pred
            if y % 100 == 0:
                print('done row {0} out of {1}'.format(y, h - 1))
        all_preds[0, 0, :] = [1, 0, 0]  # colour the top-left pixels to avoid scipy's image normalization
        all_preds[0, 1, :] = [0, 1, 0]
        all_preds[0, 2, :] = [0, 0, 1]
        all_preds[0, 3, :] = [0, 0, 0]
        result = PRED_FRACTION * all_preds + (1 - PRED_FRACTION) * img_gt
        suffix = '{0}_{1}'.format(image_suffix, radius)
        filename = 'results/responses_{0}_gt.png'.format(suffix)
        scipy.misc.imsave(filename, result)
        print('saved to', filename)


if __name__ == '__main__':
    main()
