"""A script for counting how many non-tangent, uni-tangent, and bi-(or-more-)tangent points we have
in the BMAX500 validation set."""

import numpy as np

from python import const
from python import utils


def main():
    bmax_set = utils.load_bmax_test()
    num_non, num_uni, num_bi, num_total = 0, 0, 0, 0
    rad_range = range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE)
    # num_radii = len(rad_range)
    num_bmax_data = len(bmax_set)
    for i, bmax_datum in enumerate(bmax_set):
        h, w, _ = bmax_datum['pts'].shape
        pts = np.max(bmax_datum['pts'], axis=2)  # we take the OR of all of the segmentations
        rads = bmax_datum['rad']
        bnd = bmax_datum['bnd']
        # dists_to_bnds = np.empty((h, w))
        for y in range(h):
            for x in range(w):
                pts_y_x, rads_y_x = pts[y, x], rads[y, x]
                dist = utils.round_int(utils.distance_nearest_boundary(bnd, y, x))
                for radius in rad_range:
                    if pts_y_x and np.any(rads_y_x == radius):
                        num_bi += 1
                    elif dist == radius:
                        num_uni += 1
                    else:
                        num_non += 1
                    num_total += 1
        print('finished image {0} out of {1}'.format(i, num_bmax_data))
    print('number of bitangents: {0}; fraction of bitangents: {1:.4f}'.format(num_bi, num_bi / num_total))
    print('number of unitangents: {0}; fraction of unitangents: {1:.4f}'.format(num_uni, num_uni / num_total))
    print('number of nontangents: {0}; fraction of nontangents: {1:.4f}'.format(num_non, num_non / num_total))
    print('total number:', num_total)


if __name__ == '__main__':
    main()
