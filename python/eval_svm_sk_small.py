"""Evaluates a svm on the sk-small dataset."""


import multiprocessing
import os
from pathlib import Path
import shutil

import imageio
import numpy as np
import scipy.io

from python import const
from python import eval_svm
from python import utils


def eval_sk_small():
    images_dir = Path('data/SK506/images/test')
    # gts_dir = Path('data/SK506/groundTruth/test')
    responses_dir = mk_dirs()
    image_files = os.listdir(images_dir)
    num_images = len(image_files)
    try:
        start_image_index = int(input('which image to start at? (0-{0}) '.format(num_images - 1)))
    except ValueError:
        start_image_index = 0
    try:
        end_image_index = int(input('which image to end at? (1-{0}) '.format(num_images)))
    except ValueError:
        end_image_index = num_images
    # start_image_index = 0
    # end_image_index = 2

    radius_range = range(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD, const.EVAL_RAD_STRIDE)
    # print('before svm load')
    svm_scaler = utils.load_svm_pretrained(const.GLOBAL_EVAL_WEIGHTS)
    responses_image_dir = responses_dir / 'responses_sk_small_svm_{0}'
    filename_raw_template = str(responses_image_dir / '{1}_raw.png')
    filename_smoothed_template = str(responses_image_dir / '{1}_smoothed.png')
    filename_medials_img = str(responses_image_dir / 'medial.png')
    filename_medials_mat = str(responses_image_dir / 'medial.mat')
    # print('before for loop')
    for filename_img in image_files[start_image_index:end_image_index]:
        assert filename_img.endswith('.jpg')
        image_name = filename_img.split('.', maxsplit=1)[0]  # we call this image_name, but SK-SMALL
        # seems to name all of their images numbers eg. 121.jpg

        responses_this_image_dir = str(responses_image_dir).format(image_name)
        shutil.rmtree(responses_this_image_dir, ignore_errors=True)
        try:
            os.mkdir(responses_this_image_dir)
        except FileExistsError:
            pass

        img = utils.img_uint8_to_float32(imageio.imread(images_dir / filename_img))
        # responses, responses_smoothed = eval_convnet.eval_convnet_image_threading(
        #     img, radius_range, convnet)
        # print('before svm evaluation')
        _, _, responses, responses_smoothed = eval_svm.eval_svm_image(img, svm_scaler, use_threading=False)
        # print('after svme val')
        for i, responses_radius in enumerate(responses):
            radius = radius_range[i]
            filename_raw = filename_raw_template.format(image_name, radius)
            imageio.imwrite(filename_raw, utils.img_float32_to_uint8(responses_radius))
        del responses
        for i, responses_smoothed_radius in enumerate(responses_smoothed):
            radius = radius_range[i]
            filename_smoothed = filename_smoothed_template.format(image_name, radius)
            imageio.imwrite(filename_smoothed, utils.img_float32_to_uint8(responses_smoothed_radius))
        MEDIAL_CHANNEL = 2
        responses_medial = np.max(responses_smoothed[:, :, :, MEDIAL_CHANNEL], axis=0)
        del responses_smoothed
        imageio.imwrite(filename_medials_img.format(image_name), utils.img_float32_to_uint8(responses_medial))
        scipy.io.savemat(filename_medials_mat.format(image_name), {'responses': responses_medial})
        del responses_medial
        print('finished image {0}. Saved to: {1}, {2}, {3}, {4}'.format(
            filename_img, filename_raw, filename_smoothed, filename_medials_img.format(image_name),
            filename_medials_mat.format(image_name)))


def mk_dirs():
    """Deletes and remakes the responses directory and returns the Path of the dir."""
    sk_dir = Path('data/SK506')
    try:
        os.mkdir(str(sk_dir))
    except FileExistsError:
        pass
    responses_dir = sk_dir / 'responses'
    try:
        os.mkdir(str(responses_dir))
    except FileExistsError:
        pass
    return responses_dir


def main():
    multiprocessing.set_start_method('spawn')
    eval_sk_small()


if __name__ == '__main__':
    main()
