"""New script for training a CNN. Assumes you've already run gen_dataset"""

import datetime
import math
import os
# import statistics
import time

# import hickle
import numpy as np
# import shutil
import sklearn.metrics
import torch
import torch.nn
import torch.nn.functional
# from torch.nn import functional

from python import const
from python import load_unwrappings
from python import nn_models
from python import process_google
from python import process_synth
from python import utils


def train_cnn(train_loader, val_loader):
    """Params:
        -train_loader: an iterable which yields tuples of training images, labels, and indexes (see
        load_unwrappings.UnwrappingsLoader)
        -val_loader: same, but for validation.
    """
    # timestamp will be used to demark when a neural network started training in order to avoid
    # overwriting previous versions
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H%M')
    print('saving weights with timestamp: {0}'.format(timestamp))
    if const.GLOBAL_VAL_VOTE:
        print('using voting scheme')
    else:
        print('NOT using voting')

    # model, optimizer, loss
    cnn = nn_models.get_model()
    per_class_weights = torch.Tensor(const.TRAIN_CLASS_WEIGHTS)
    if const.GLOBAL_USE_CUDA:
        per_class_weights = per_class_weights.cuda()
    if const.GLOBAL_LABEL_SMOOTHING:
        criterion = nn_models.ConfidencePenaltyCrossEntropyLoss(reduction='sum')
    else:
        criterion = torch.nn.CrossEntropyLoss(weight=per_class_weights, reduction='sum')
    criterion_functional = lambda *args, **kwargs: torch.nn.functional.cross_entropy(
        *args, weight=per_class_weights, **kwargs)
    # criterion = torch.nn.BCEWithLogitsLoss()
    # criterion = nn_models.WeightedBceLogitsLoss()
    # criterion = nn_models.BalancedBceLogitsLoss()
    optimizer = torch.optim.Adam(cnn.parameters(), lr=const.TRAIN_LEARNING_RATE,
                                 weight_decay=const.TRAIN_WEIGHT_DECAY)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, 'min', factor=const.TRAIN_SCHED_FACTOR, patience=const.TRAIN_SCHED_PATIENCE,
        verbose=True, threshold=const.TRAIN_LOSS_IMPROVE_THRESHOLD,)
    if const.GLOBAL_USE_CUDA:
        criterion = criterion.cuda()
    start_epoch, best_loss, best_epoch = 0, math.inf, -1

    # train!
    for epoch in range(start_epoch, const.TRAIN_NUM_EPOCHS):
        t0 = time.time()
        labels_train, preds_train, train_loss = train_epoch(cnn, train_loader, optimizer, criterion)
        curr_lr = optimizer.param_groups[0]['lr']
        scheduler.step(train_loss)
        t1 = time.time()

        # validate the Model
        if const.GLOBAL_VAL_VOTE:
            # labels_val, preds_val, val_loss = eval_epoch_vote(cnn, val_loader,
            #     criterion_functional)
            assert False  # not currently implemented
        else:
            labels_val, likelihoods_val, preds_val, val_loss = eval_epoch_no_vote(cnn, val_loader,
                                                                       criterion_functional)
            threshold, f, precision, recall = find_best_threshold(labels_val, likelihoods_val)
        if val_loss < best_loss:
            best_loss = val_loss
            best_epoch = epoch
            save_best(cnn.state_dict(), timestamp)
            print('new best val loss {0:.4f}'.format(val_loss), datetime.datetime.now())
            print('training confusion matrix:')
            print(format_confusion(sklearn.metrics.confusion_matrix(labels_train, preds_train)))
            print('validation confusion matrix:')
            print(format_confusion(sklearn.metrics.confusion_matrix(labels_val, preds_val)))
        else:
            print('no improvement in {0} epochs. best is {1:.4f}'.
                  format(epoch - best_epoch, best_loss), datetime.datetime.now())
        print('Val loss: {0:.4f}; thresh: {1:.3f}; f: {2:.4f}; prec: {3:.4f}; rec: {4:.4f}. train loss: {5:.4f}. lr: {6}'.
              format(val_loss, threshold, f, precision, recall, train_loss, curr_lr))
        t2 = time.time()
        print('epoch {epoch:d} training took {train:.1f}s; val took {val:.1f}s'.
              format(epoch=epoch, train=t1 - t0, val=t2 - t1))
        if val_loss <= 0.:
            print('training stops because the validation loss is perfect')
            break


def format_confusion(confusion):
    """Returns a copy of the confusion matrix, formatted"""
    return confusion / np.sum(np.ravel(confusion))


def find_best_threshold(labels, likelihoods):
    """Finds the threshold that gives the best f score

    :param labels: a ndarray vector of n labels in {0, 1, 2}
    :param likelihoods: a ndarray vector of n likelihoods, where the likelihood represents mediality
    :return: a tuple of:
        -best threshold
        -best f score
        -precision according to the best threshold
        -recall according to the best threshold
    """
    labels = labels == const.TRAIN_POS_LABEL
    threshold_range = np.linspace(0., 1., 1001)
    best_threshold, best_f = -1, -1
    for threshold in threshold_range:
        preds = likelihoods > threshold
        f = sklearn.metrics.f1_score(labels, preds)
        if f > best_f:
            best_threshold, best_f = threshold, f
    assert best_threshold >= 0 and best_f >= 0
    preds = likelihoods > best_threshold
    precision = sklearn.metrics.precision_score(labels, preds)
    recall = sklearn.metrics.recall_score(labels, preds)
    return best_threshold, best_f, precision, recall


def train_epoch(cnn, loader_train, optimizer, criterion):
    """Run a training epoch"""
    cnn.train()
    all_labels, all_preds = [], []
    total_loss = 0
    for unwraps, labels, _ in loader_train:
        unwraps.requires_grad_()

        # Forward + Backward + Optimize
        optimizer.zero_grad()
        outputs = cnn(unwraps)
        loss = criterion(outputs, labels)
        loss.backward()
        total_loss += loss.item()
        optimizer.step()
        # The following line only works for two-class classification
        # predicted = outputs.data >= const.CNN_DECISION_BOUNDARY
        predicted = torch.argmax(outputs, dim=1)
        all_labels.append(labels.cpu().detach().numpy().astype(np.int64))
        all_preds.append(predicted.cpu().detach().numpy())
    all_labels, all_preds = np.concatenate(all_labels, axis=0), np.concatenate(all_preds, axis=0)
    total_loss /= len(all_labels)
    return all_labels, all_preds, total_loss


def eval_epoch_no_vote(cnn, loader_val, criterion):
    """Run an evaluation of the trained epoch"""
    cnn.eval()
    all_labels, all_likelihoods, all_preds = [], [], []
    total_loss = 0
    for images, labels, _ in loader_val:
        outputs = cnn(images)
        loss = criterion(outputs, labels, reduction='sum')
        total_loss += loss.item()
        # the following line only works for two-class classification
        # predicted = outputs.data >= const.CNN_DECISION_BOUNDARY
        likelihoods = torch.nn.functional.softmax(outputs, dim=1)[:, const.TRAIN_POS_LABEL]
        all_labels.append(labels.cpu().detach().numpy())
        all_likelihoods.append(likelihoods.cpu().detach().numpy())
        preds = torch.argmax(outputs, dim=1)
        all_preds.append(preds.cpu().detach().numpy())
    all_labels, all_likelihoods, all_preds = \
        np.concatenate(all_labels), np.concatenate(all_likelihoods), np.concatenate(all_preds)
    total_loss /= len(all_labels)
    return all_labels, all_likelihoods, all_preds, total_loss


# def cycle_mining_dataset(cnn, loader_mining, bmax_train, unwraps_mining, labels_mining):
#     cnn.eval()
#     all_preds = []
#     all_probs = []
#     for images, labels in loader_mining:
#         images.requires_grad, labels.requires_grad = False, False
#         outputs = np.squeeze(cnn(images).cpu().detach().numpy())
#         predicted = outputs >= const.CNN_DECISION_BOUNDARY
#         all_preds.append(predicted)
#         all_probs.append(outputs)
#     # all_labels, all_preds = np.asarray(all_labels, dtype=np.int64), np.asarray(all_preds)
#     all_preds = np.concatenate(all_preds, axis=0)
#     all_probs = np.concatenate(all_probs, axis=0)
#     mining_accuracy = sklearn.metrics.accuracy_score(labels_mining, all_preds)
#     cut_indexes, new_unwraps_mining, new_labels_mining = data_manip.update_mining_set(
#         bmax_train, all_probs, labels_mining)
#     new_unwraps_mining = new_unwraps_mining.transpose((0, 3, 1, 2))
#     unwraps_mining[cut_indexes, :, :, :] = new_unwraps_mining
#     labels_mining[cut_indexes] = new_labels_mining
#     return mining_accuracy


def save_best(cnn_state_dict, timestamp):
    """Save the state dict of the best CNN"""
    try:
        os.mkdir(const.TRAIN_DIR)
    except FileExistsError:
        pass
    if const.GLOBAL_GOOGLE:
        source = 'google'
    elif const.GLOBAL_SYNTH:
        source = 'synth'
    else:
        source = 'bmax'
    if const.GLOBAL_AUG:
        aug = 'aug'
    else:
        aug = 'noaug'
    result = (cnn_state_dict, const.GLOBAL_MODEL, const.GLOBAL_TRAIN_SIZE, const.GLOBAL_VAL_SIZE,
              const.GLOBAL_AUG)
    suffix = '_'.join(str(x) for x in (source, const.GLOBAL_MODEL, const.GLOBAL_TRAIN_SIZE,
                                       const.GLOBAL_VAL_SIZE, aug, timestamp))
    filename = const.TRAIN_BEST_FILENAME.format(suffix=suffix)
    print('Saving weights to {0}'.format(filename))
    torch.save(result, filename)


def main():
    """The main work of this script. Trains and validates a CNN."""
    utils.seed_randoms()
    shuffle_train = True
    shuffle_val = False
    process_unwraps_train = True
    process_labels_train = True
    process_unwraps_val = True
    process_labels_val = True
    val_aug = False
    if const.GLOBAL_GOOGLE:
        loader_train = process_google.GoogleSgdLoader(
            const.GOOGLE_UNWRAPPED_FILENAME.format(suffix='logo_8192'),
            const.TRAIN_FIXED_CUTS_PER_PIXEL, shuffle_train, process_unwraps_train,
            process_labels_train, const.GLOBAL_AUG)
        loader_val = process_google.GoogleSgdLoader(
            const.GOOGLE_UNWRAPPED_FILENAME.format(suffix='chrome_4096'),
            const.TRAIN_FIXED_VOTES_PER_PIXEL, shuffle_val, process_unwraps_val, process_labels_val,
            val_aug)
    elif const.GLOBAL_SYNTH:
        loader_train = process_synth.SynthLoader(
            const.SYNTH_TRAIN['data/rectangle_bw.png'][1], const.SYNTH_TRAIN_NUM_ANGLES,
            shuffle_train, process_unwraps_train, process_labels_train, const.GLOBAL_AUG)
        loader_val = process_synth.SynthLoader(
            const.SYNTH_TRAIN['data/rectangle_90.png'][1], 1, shuffle_val, process_unwraps_val,
            process_labels_val, val_aug)
    else:
        if const.GLOBAL_TRAIN_NEW:
            imgs_per_mb = 4  # the smaller this number, the longer each epoch
            loader_train = load_unwrappings.AllUnwrappingsLoader(
                const.TRAIN_BATCH_SIZE, imgs_per_mb, const.TRAIN_FIXED_CUTS_PER_PIXEL,
                process_unwraps_train, process_labels_train)
        else:
            suffix = 'train_' + str(const.GLOBAL_TRAIN_SIZE)
            filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
            loader_train = load_unwrappings.UnwrappingsLoader(
                filename, True, const.TRAIN_BATCH_SIZE, const.TRAIN_FIXED_CUTS_PER_PIXEL,
                shuffle_train, process_unwraps_train, process_labels_train, const.GLOBAL_AUG)
        suffix = 'val_' + str(const.GLOBAL_VAL_SIZE)
        filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
        loader_val = load_unwrappings.UnwrappingsLoader(
            filename, const.GLOBAL_VAL_VOTE, const.TRAIN_BATCH_SIZE,
            const.TRAIN_FIXED_VOTES_PER_PIXEL, shuffle_val, process_unwraps_val, process_labels_val,
            val_aug)
    train_cnn(loader_train, loader_val)


if __name__ == '__main__':
    main()
