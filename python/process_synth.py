"""A script for processing synthetic images into medial axis transforms and generating unwrappings
for training/testing models
"""

# import math
import random
import hickle
import imageio
from imgaug import augmenters as iaa
# from matplotlib import pyplot
import numpy as np
# from scipy import misc
from skimage.morphology import medial_axis
from sklearn.cluster import KMeans
import sklearn.preprocessing
# import torch

from python import const
from python import utils


def gen_unwrappings():
    print('synthetic medial rejection distance: {0:.4f}'.format(const.SYNTH_MEDIAL_DIST_THRESHOLD))
    for image_filename, (processed_filename, unwraps_filename, k) in const.SYNTH_TRAIN.items():
        print('processing', image_filename)
        # cluster the image
        img = utils.img_uint8_to_float32(imageio.imread(image_filename))
        h, w, c = img.shape
        if c > 3:  # ignore alpha channel
            img = img[:, :, :3]
            c = 3
        if const.CNN_GRAYSCALE:
            print('converting images to grayscale')
            img = utils.rgb2gray_matlab(img)
            c = 1
        img_flattened = np.reshape(img, (h * w, c))
        fitting = KMeans(k).fit(img_flattened)
        img_flattened_clustered = fitting.cluster_centers_[fitting.labels_]
        img_clustered = np.reshape(img_flattened_clustered, (h, w, c))

        # find the medial points and their radiuses
        all_skels = np.zeros((h, w), np.bool)
        all_dists = np.ones((h, w)) * np.inf
        for label in range(k):
            img_this_cluster = fitting.labels_ == label
            img_this_cluster = np.reshape(img_this_cluster, (h, w))
            skel, distance = medial_axis(img_this_cluster, return_distance=True)
            all_dists = np.where(np.logical_and(distance <= all_dists, distance != 0), distance,
                                 all_dists)
            all_skels = all_skels | skel

            # eliminate the points which are closer to the edge than their own radius
            for y in range(h):
                for x in range(w):
                    if all_skels[y, x]:
                        dist_to_nearest_edge = min((y, x, h - y, w - x), default=0)
                        if all_dists[y, x] >= dist_to_nearest_edge:
                            all_skels[y, x] = False

        all_dists = all_dists.astype(np.int)

        hickle.dump((img_clustered, all_skels, all_dists), processed_filename)
        # plot
        # pyplot.imshow(img_clustered / 255)
        # pyplot.show()
        # pyplot.imshow(all_skels)
        # pyplot.show()
        # pyplot.imshow(all_dists)
        # pyplot.show()

        ys, xs, rads, labels = choose_training_examples(img_clustered, all_skels, all_dists)
        unwraps = utils.unwrap(img_clustered, ys, xs, rads)
        hickle.dump((unwraps, labels), unwraps_filename)


def choose_training_examples(img, skels, dists):
    """
    choose positive and negative training examples. We are going to choose ALL positive examples.
    :param img: the image to choose_training_examples (h x w x c)
    :param skels: a bool image (h x w) which is True for a given pixel if that pixel is medial
    :param dists: a h x w array where each cell is that pixel's distance to the nearest boundary
    :return: a tuple of ndarrays ys, xs, rads, labels
    """
    h, w, _ = img.shape

    # choose positive examples
    medials = np.argwhere(skels)
    all_points_rads_pos = [(y, x, dists[y, x]) for (y, x) in medials]
    # choose only the points which are sufficiently far from the image edges
    points_rads_pos = []
    for y, x, rad in all_points_rads_pos:
        if not (const.SYNTH_MIN_RAD <= rad < const.SYNTH_MAX_RAD):
            continue
        dist_to_nearest_image_edge = min((y, x, h - y, w - x), default=0)
        if dist_to_nearest_image_edge > rad:
            points_rads_pos.append((y, x, rad))
    num_points_rads_pos = int(len(points_rads_pos))  # ALL positives
    num_points_rads_uni = int(num_points_rads_pos * const.SYNTH_UNI_FACTOR)
    num_points_rads_neg = int(num_points_rads_pos * const.SYNTH_NEG_FACTOR)

    print('number of positives: {0}; number of unitangents: {1}; number of random negatives: {2}'.
          format(num_points_rads_pos, num_points_rads_uni, num_points_rads_neg))

    # choose negative examples
    points_rads_uni, points_rads_neg = set(), set()
    num_outside_range, num_medials_rejected, num_edges_rejected = 0, 0, 0
    while len(points_rads_uni) < num_points_rads_uni or len(points_rads_neg) < num_points_rads_neg:
        # select a random point and make sure it's not medial
        y = random.randrange(h)
        x = random.randrange(w)
        dist_nearest_boundary = dists[y, x]
        if len(points_rads_uni) < num_points_rads_uni:  # fill up the unitangent list first
            rad = dist_nearest_boundary  # assign the distance to the nearest boundary as the radius
            neg_type = 'uni'
        else:
            while True:
                rad = random.randint(const.SYNTH_MIN_RAD, const.SYNTH_MAX_RAD)  # random radius
                if abs(rad - dist_nearest_boundary) > const.BMAX_NON_MEDIAL_RAD_THRESHOLD:
                    break
            neg_type = 'neg'
        assert rad % 1 == 0  # integer radius
        if not (const.SYNTH_MIN_RAD <= rad < const.SYNTH_MAX_RAD):
            num_outside_range += 1
            continue
        dist_to_nearest_image_edge = min((y, x, h - y, w - x), default=0)
        if dist_to_nearest_image_edge <= rad:
            # print('rejected negative too close to the edge of the image:', (y, x, rad))
            num_edges_rejected += 1
            continue
        curr_dist_to_nearest_medial = utils.dist_to_nearest_medial((y, x, rad), all_points_rads_pos)
        if curr_dist_to_nearest_medial <= const.SYNTH_MEDIAL_DIST_THRESHOLD:
            # print('rejected negative', (y, x, rad), 'too close to a medial point')
            num_medials_rejected += 1
            continue
        if neg_type == 'uni':
            points_rads_uni.add((y, x, rad))
        else:
            points_rads_neg.add((y, x, rad))
    print('rejected {0} points for being outside of the radius range, {1} points for being too '
          'close to medials, {2} for being too close to edges'.
          format(num_outside_range, num_medials_rejected, num_edges_rejected))
    # set isn't array_like, so we need to convert it to list
    points_rads_uni, points_rads_neg = list(points_rads_uni), list(points_rads_neg)

    # aggregate everything
    points_rads = np.asarray(points_rads_pos + points_rads_uni + points_rads_neg)
    labels = ([const.TRAIN_POS_LABEL for _ in points_rads_pos] +
              [const.TRAIN_UNI_LABEL for _ in points_rads_uni] +
              [const.TRAIN_NEG_LABEL for _ in points_rads_neg])
    labels = np.asarray(labels)
    ys, xs, rads = points_rads[:, 0], points_rads[:, 1], points_rads[:, 2]
    # calculate the unwrappings at the specified number of angles
    assert ys.shape == xs.shape == rads.shape == labels.shape == (
        num_points_rads_pos + num_points_rads_uni + num_points_rads_neg,)
    return ys, xs, rads, labels


class SynthLoader:
    """Iterable used for the purposes of training on the synthetic unwrappings with SGD. Yields
    tuples of:
        -unwrappings
        -labels
        -unique indexes of training data from which each unwrapping and label come
    """
    def __init__(self, filename_unwraps, cuts_per_pixel, shuffle=False, process_unwraps_torch=False,
                 process_labels_torch=False, augment_unwraps=False, process_unwraps_svm=False,
                 normalizer=None):
        """
        :param filename_unwraps: the filename of the unwrappings to load
        :param cuts_per_pixel: how many cuts to yield per pixel
        :param shuffle: whether to shuffle before iterating
        :param process_unwraps_torch: whether to process unwraps for torch (using
        utils.unwrap_to_torch) before yielding
        :param process_labels_torch: whether to process labels for torch before yielding
        :param augment_unwraps: whether to augment the unwraps before yielding
        :param process_unwraps_svm: exclusive with process_unwraps_torch. Preprocess the unwraps
        into HOGs to make them suitable for training with SVM. Normalize the HOGs to zero mean, one
        standard deviation
        :param normalizer: optional. If provided (eg. because we're evaluating and we want to use
        the same normalizer as the one used during training):
            -must be a sklearn.preprocessing.StandardScaler
            -process_unwraps_svm must be true
            -normalize the hogs using this normalizer
        if not provided, but process_unwraps_svm is True (eg. because we're training an SVM), then
        we will create our own normalizer.
        """
        print('working with file', filename_unwraps)
        self.all_unwraps, self.all_labels = hickle.load(filename_unwraps)
        self.num_data = self.all_unwraps.shape[0]
        assert self.all_labels.shape == (self.num_data,)
        self.cuts_per_pixel = cuts_per_pixel
        self.angles_per_pixel = np.linspace(0, 360, cuts_per_pixel, False)
        self.shuffle = shuffle
        self.process_unwraps_torch = process_unwraps_torch
        self.process_labels_torch = process_labels_torch
        self.aug_unwraps = augment_unwraps
        self.process_unwraps_svm = process_unwraps_svm
        self._apply_norm = False
        assert not (process_unwraps_torch and process_unwraps_svm)
        if process_unwraps_svm and normalizer is None:
            with utils.Timer('fitting normalization scaler'):
                self.normalizer = sklearn.preprocessing.StandardScaler()
                for hogs, _, _ in iter(self):
                    self.normalizer.partial_fit(hogs)
                self._apply_norm = True  # note that we don't set this to
                # True until after we've fit our normalizer. This is because we don't
                # want the hogs to already be normalized when we examine them for
                # normalization
        elif process_unwraps_svm:
            self.normalizer = normalizer
            self._apply_norm = True
        self.ordering = None
        self.used_idxs = None

    def __iter__(self):
        self.ordering = np.arange(self.num_data)
        if self.shuffle:
            np.random.shuffle(self.ordering)
        self.used_idxs = 0
        return self

    def __next__(self):
        if self.used_idxs >= self.num_data:
            raise StopIteration
        curr_num_idxs = min(const.TRAIN_BATCH_SIZE // self.cuts_per_pixel, self.num_data - self.used_idxs)
        curr_idxs = self.ordering[self.used_idxs: self.used_idxs + curr_num_idxs]
        curr_idxs = np.repeat(curr_idxs, self.cuts_per_pixel, axis=0)
        curr_unwraps = self.all_unwraps[curr_idxs, :, :, :]
        if self.aug_unwraps:
            curr_unwraps *= 255.  # augmenters expect image in range 0-255
            curr_unwraps = aug.augment_images(curr_unwraps)
            curr_unwraps /= 255.
        curr_labels = self.all_labels[curr_idxs]
        angles = np.tile(self.angles_per_pixel, (curr_num_idxs,))
        curr_unwraps = utils.shift_unwraps(curr_unwraps, angles)
        self.used_idxs += curr_num_idxs
        if self.process_unwraps_torch:
            curr_unwraps = utils.unwraps_to_torch(curr_unwraps)
        elif self.process_unwraps_svm:
            curr_unwraps = utils.unwraps_to_hogs(curr_unwraps)
            if self._apply_norm:
                curr_unwraps = self.normalizer.transform(curr_unwraps)
        if self.process_labels_torch:
            curr_labels = utils.labels_to_torch(curr_labels)
        return curr_unwraps, curr_labels, curr_idxs

    def idxs_to_labels(self):
        return self.all_labels


# Augment the dataset
aug = iaa.Sequential([
    # iaa.Fliplr(0.5),
    # iaa.Sometimes(0.1, iaa.ContrastNormalization((0.5, 1.5))),
    # iaa.Sometimes(0.1, iaa.GaussianBlur((0, 0.5))),
    iaa.Sometimes(0.2, iaa.Add((-32, 32), per_channel=0.8)),
    iaa.Sometimes(0.2, iaa.Multiply((0.9, 1.1), per_channel=0.8)),
    # iaa.Sometimes(0.1, iaa.AdditiveGaussianNoise(scale=1, per_channel=0.5),),
], random_order=False, deterministic=True)


if __name__ == '__main__':
    gen_unwrappings()
