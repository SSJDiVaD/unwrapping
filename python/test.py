"""Just random testing."""


import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import numpy as np
# import scipy.ndimage

from python import utils


def create_rgb():
    """Returns an image with red, green, blue stripes like a European flag"""
    red = np.asarray((1., 0., 0.))
    green = np.asarray((0., 1., 0.))
    blue = np.asarray((0., 0., 1.))
    black = np.asarray((0., 0., 0.))
    h, w = (100, 7)
    reds = np.tile(red[np.newaxis, np.newaxis, :], (h, w, 1))
    greens = np.tile(green[np.newaxis, np.newaxis, :], (h, w, 1))
    blues = np.tile(blue[np.newaxis, np.newaxis, :], (h, w, 1))
    blacks = np.tile(black[np.newaxis, np.newaxis, :], (h, w, 1))
    img = np.concatenate((blacks, reds, blacks, greens, blacks, blues, blacks), axis=1)
    return img


def main():
    img = create_rgb()
    plt.imshow(img)
    plt.show()
    SIGMA = 1
    img = utils.gaussian_smooth(img, SIGMA)
    print(np.max(img, axis=(0, 1)))
    plt.imshow(img)
    plt.show()


if __name__ == '__main__':
    main()