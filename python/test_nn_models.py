"""Tests for nn_models"""

# import random
import time

import matplotlib
from matplotlib import pyplot as plt
import numpy as np
import torch

from python import const
from python import nn_models
from python import utils


def load_model():
    """Returns the CNN model with weights loaded from the first command line argument.

    CNN is set to eval mode."""
    filename = const.GLOBAL_ARGS_OTHER[0]
    if const.GLOBAL_USE_CUDA:
        map_location = 'cuda:0'
    else:
        map_location = 'cpu'
    state_dict, model, *_ = torch.load(filename, map_location)
    print('ignored attributes {0} when loading CNN'.format(_))
    cnn = nn_models.get_model(model)
    cnn.load_state_dict(state_dict)
    cnn.eval()
    return cnn


def test_zeros():
    """Passes a Tensor of zeros through the user-selected Pytorch model in order to see what it
    spits out. Prints and returns the result.
    """
    cnn = load_model()
    if const.CNN_GRAYSCALE:
        torch_zeros = (1, 1)
    else:
        torch_zeros = (1, 3)
    torch_zeros = torch.zeros(torch_zeros + const.UNWRAP_SHAPE)
    if const.GLOBAL_USE_CUDA:
        torch_zeros = torch_zeros.cuda()
    output = cnn(torch_zeros)
    output = torch.nn.functional.softmax(output, dim=1)
    print(output)
    return output


def test_invariant_randoms():
    """Passes "unwrappings" (randomly generated) through the user-selected Pytorch model in order
    to check whether the model is actually horizontally invariant.
    """
    cnn = load_model()
    if const.CNN_GRAYSCALE:
        num_channels = 1
    else:
        num_channels = 3
    unwrap_shape = const.UNWRAP_SHAPE + (num_channels,)
    # unwraps = torch.rand(*unwraps_shape)
    np.random.seed(int(time.time()))
    unwrap = np.random.rand(*unwrap_shape)
    unwraps = shift_unwraps_test(unwrap)
    unwraps = utils.unwraps_to_torch(unwraps)
    output = cnn(unwraps)
    output = torch.nn.functional.softmax(output, dim=1).cpu().detach().numpy()
    print('output:')
    print(output)
    print('means:', np.mean(output, 0))
    print('stds:', np.std(output, 0))
    print('coefficient of variation:', np.std(output, 0) / np.mean(output, 0))
    print('---------------------------------------------')
    # print('sorted output:')
    # print(np.sort(output, axis=0))
    print(check_opposites_equal(output))
    return output


def shift_unwraps_test(unwrap):
    """Shifts the given unwrap (h * w * c) by all possible horizontal shifts, giving a minibatch of
    (w * h * w * c) unwraps.

    The reason w is the number of possible horizontal shifts is because we want to avoid
    interpolation/antialiasing artifacts, so we only shift by exact pixels.
    """
    height, width, num_channels = unwrap.shape
    result = np.empty((width, height, width, num_channels))
    result[0, :, :, :] = unwrap
    for i in range(1, width):
        unwrap_shifted = shift_unwrap_once(unwrap)
        result[i, :, :, :] = unwrap_shifted
        unwrap = unwrap_shifted
    return result


def shift_unwrap_once(unwrap):
    """Returns a copy of unwrap, shifted by one pixel to the right. Unwrap is of shape (h * w * c).

    Helper to shift_unwraps_test.
    """
    unwrap_shape = unwrap.shape
    rightmost = unwrap[:, -1:, :]
    rest = unwrap[:, :-1, :]
    result = np.concatenate((rightmost, rest), axis=1)
    assert result.shape == unwrap_shape
    # plt.imshow(result)
    # plt.show()
    return result


def check_opposites_equal(outputs):
    """Given a ndarray of outputs from the CNN, checks whether the outputs on opposite sides of the
    unwrap shifts (ie. for 64-pixel-wide unwrappings, the outputs 32 pixels away from each other)
    are equal.
    """
    half_size_outputs = outputs.shape[0] // 2
    assert outputs.shape[0] == half_size_outputs * 2
    problem_found = False
    for i in range(half_size_outputs):
        if (outputs[i, :] != outputs[i + half_size_outputs, :]).any():
            print('shifts {0} and {1} don\'t match: {2} and {3}'
                  .format(i, i + half_size_outputs, outputs[i, :],
                          outputs[i + half_size_outputs, :]))
            problem_found = True
    if not problem_found:
        print('all shifts were ok')
    return problem_found


# incomplete
# def test_invariant_reflection():
#     cnn = load_model()
#     if const.CNN_GRAYSCALE:
#         num_channels = 1
#     else:
#         num_channels = 3
#     unwrap_shape = const.UNWRAP_SHAPE + (num_channels,)
#     # unwraps = torch.rand(*unwraps_shape)
#     np.random.seed(int(time.time()))
#     unwrap = np.random.rand(*unwrap_shape)
#     unwraps = shift_unwraps_test(unwrap)
#     unwraps = utils.unwraps_to_torch(unwraps)
#     output = cnn(unwraps)
#     output = torch.nn.functional.softmax(output, dim=1).cpu().detach().numpy()
#     print('output:')
#     print(output)
#     print('means:', np.mean(output, 0))
#     print('stds:', np.std(output, 0))
#     print('coefficient of variation:', np.std(output, 0) / np.mean(output, 0))
#     print('---------------------------------------------')
#     # print('sorted output:')
#     # print(np.sort(output, axis=0))
#     print(check_opposites_equal(output))
#     return output


if __name__ == '__main__':
    matplotlib.use('Qt5Agg')
    test_zeros()
    test_invariant_randoms()
