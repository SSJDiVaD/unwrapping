"""
New version for generating dataset as of June 22, 2018. Basically, want to store the data in such a
way that:
    1) we can spit out points in a minibatch fashion for training CNN
    2) we can spit out all points at once for training SVM
    3) we can spit out points in a labelled minibatch fashion for evaluating. The model should be
    able to implement a voting scheme for each point

Notes:
    1) note that we are no longer concerned with fixed vs rand. The loader will worry about that.
    2) we are only generating one unwrapping per point-radius. If we want a different orientation,
    we'll shift the image.
"""

import datetime
import math
import multiprocessing
import os
import queue
import random
import shutil
import socket
import time

import hickle
from imgaug import augmenters as iaa
# import math
SHOW_UNWRAPS = False
if SHOW_UNWRAPS and socket.gethostname() == 'OhDear':
    import matplotlib
    matplotlib.use('TkAgg')
    from matplotlib import pyplot as plt
import numpy as np
import scipy

from python import const
from python import utils

FLIPPER = iaa.Sequential([
    iaa.Fliplr(1),
])


def max_gen_processes():
    """Returns the number of generation processes to use"""
    hostname = socket.gethostname()
    if hostname == 'OhDear':
        return 7
    elif hostname == 'viscomp4':
        return 8
    elif hostname == 'viscomp3':
        return 8
    else:
        # Each process seems to use up to 4 cores on the viscomp4 machine
        num_processes = math.ceil(multiprocessing.cpu_count() / 4. - const.EPS_BIG)
        print('unrecognized computer: {0}. Using {1} processes'.format(hostname, num_processes))
        return num_processes


def gen_datasets():
    """The gen_skeletonization work behind this script."""
    print('started at', datetime.datetime.now())
    make_dirs()

    bmax_train, bmax_val, train_images, val_images = split_bmax()
    hickle.dump(train_images, const.BMAX_TRAIN_FILENAME)
    hickle.dump(val_images, const.BMAX_VAL_FILENAME)
    print('saved splits to {0} and {1}'.format(const.BMAX_TRAIN_FILENAME, const.BMAX_VAL_FILENAME))

    # These values are MAXIMUMS, not actual.
    dataset_train_sizes = [
        2 ** 13,  # 8192
        # 2 ** 16,  # 65536
        # 2 ** 17,  # 131072
        2 ** 18,  # 262144
        # 2 ** 19,  # 524288
        # 2 ** 20,  # 1048576
    ]
    dataset_val_sizes = [
        2 ** 13,  # 8192
        # 2 ** 16,  # 65536
    ]
    TIME_BETWEEN_PROCESS_CHECKS = 1.  # seconds
    # generate the validation sets first because they're smaller and take less time
    for val_size in dataset_val_sizes:
        with utils.Timer('val {0}'.format(val_size)):
            utils.seed_randoms()
            all_unwraps, all_labels = [], []
            num_val_images = len(bmax_val)
            points_rads_per_image = int(val_size / num_val_images)
            num_processes = max_gen_processes()
            processes = [None for _ in range(num_processes)]
            batch_generator = iter(utils.batch_generator(num_val_images, batch_size=3))
            all_done = False
            unwraps_labels_queue = multiprocessing.Queue()
            while not all_done:
                all_done = True
                work_done = False
                for i, process in enumerate(processes):
                    if process is None or not process.is_alive():
                        # If we still have images to process, and the current process is done the
                        # image it was assigned, assign it a new one
                        images_remaining = True
                        try:
                            image_indices = next(batch_generator)
                        except StopIteration:
                            images_remaining = False
                        if images_remaining:
                            print('starting a minibatch at image {0} out of {1}'.
                                  format(image_indices[0], num_val_images))
                            bmax_minibatch = [copy_bmax_datum(bmax_datum) for bmax_datum in bmax_val[image_indices]]
                            process = multiprocessing.Process(
                                None, get_unwrappings_image_multithreaded, None,
                                (bmax_minibatch, points_rads_per_image, unwraps_labels_queue, False))
                            process.start()
                            processes[i] = process
                            # We just started a process, so we're definitely not done yet
                            all_done = False
                            work_done = True
                    elif process is not None and process.is_alive():
                        all_done = False
                # If the queue has anything in it, clear it out
                while True:
                    try:
                        unwraps, labels = unwraps_labels_queue.get_nowait()
                        all_unwraps.append(unwraps)
                        all_labels.append(labels)
                        work_done = True
                    except queue.Empty:
                        break
                # If we've done any work, we don't need to wait because the work itself causes a
                # delay
                if not work_done:
                    time.sleep(TIME_BETWEEN_PROCESS_CHECKS)
            all_unwraps, all_labels = np.concatenate(all_unwraps), np.concatenate(all_labels)
            if SHOW_UNWRAPS:
                for unwrap in all_unwraps:
                    plt.imshow(unwrap)
                    plt.show()
            print('{0} examples'.format(all_labels.shape[0]))
            suffix = 'val_' + str(val_size)
            filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
            hickle.dump((all_unwraps, all_labels), filename)
        print('val {0} finished at {1}'.format(val_size, datetime.datetime.now()))
    for train_size in dataset_train_sizes:
        with utils.Timer('training {0}'.format(train_size)):
            utils.seed_randoms()
            all_unwraps, all_labels = [], []
            points_rads_per_image = int(train_size / const.BMAX_NUM_TRAIN)
            num_processes = max_gen_processes()
            processes = [None for _ in range(num_processes)]
            # I chose a batch size of 11 because 176 = 11 * 2 ^ 4
            batch_generator = iter(utils.batch_generator(const.BMAX_NUM_TRAIN, batch_size=11))
            all_done = False
            unwraps_labels_queue = multiprocessing.Queue()
            while not all_done:
                all_done = True
                work_done = False
                for i, process in enumerate(processes):
                    if process is None or not process.is_alive():
                        # If we still have images to process, and the current process is done the
                        # image it was assigned, assign it a new one
                        images_remaining = True
                        try:
                            image_indices = next(batch_generator)
                        except StopIteration:
                            images_remaining = False
                        if images_remaining:
                            print('starting a minibatch at image {0} out of {1}'.
                                  format(image_indices[0], const.BMAX_NUM_TRAIN))
                            bmax_minibatch = [copy_bmax_datum(bmax_datum) for bmax_datum in bmax_train[image_indices]]
                            process = multiprocessing.Process(
                                None, get_unwrappings_image_multithreaded, None,
                                (bmax_minibatch, points_rads_per_image, unwraps_labels_queue, True))
                            process.start()
                            processes[i] = process
                            # We just started a process, so we're definitely not done yet
                            all_done = False
                            work_done = True
                    elif process is not None and process.is_alive():
                        all_done = False
                # If the queue has anything in it, clear it out
                while True:
                    try:
                        unwraps, labels = unwraps_labels_queue.get_nowait()
                        all_unwraps.append(unwraps)
                        all_labels.append(labels)
                        work_done = True
                    except queue.Empty:
                        break
                # If work was done, we don't need to delay because the work itself causes delay
                if not work_done:
                    time.sleep(TIME_BETWEEN_PROCESS_CHECKS)
            all_unwraps, all_labels = np.concatenate(all_unwraps), np.concatenate(all_labels)
            print('{0} examples'.format(all_labels.shape[0]))
            suffix = 'train_' + str(train_size)
            filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
            hickle.dump((all_unwraps, all_labels), filename)
        print('train {0} finished at {1}'.format(train_size, datetime.datetime.now()))


def copy_bmax_datum(bmax_datum):
    """Returns a copy of bmax_datum as a dict containing keys:
        -'bnd'
        -'iid'
        -'img'
        -'pts'
        -'rad'
        -'seg'

    This function avoids a crash when a bmax datum is passed to a subprocess.
    """
    result = {}
    for key in ('bnd', 'iid', 'img', 'pts', 'rad', 'seg'):
        result[key] = bmax_datum[key]
    return result


def split_bmax():
    """Split bmax into training, validation, and testing images and return the three splits.

    Returns a tuple of each split (train, val). Each split is a numpy array of images,
    where each image has the following fields:
        -'img': the RGB image
        -'seg': 5-7 different segmentations of the image
        -'pts': the medial points according to each segmentation
        -'rad': if a pixel is medial according to a given segmentation, its radius
        -a few more that I can't remember. See the BMAX500 dataset for more details
    Also returns a np array listing train and val images
    """
    bmax = scipy.io.loadmat(const.BMAX_FILENAME)
    bmax = bmax['BMAX500'][0, 0]
    bmax_train = bmax['train'][0]
    train_images = np.random.choice(200, (const.BMAX_NUM_TRAIN, ), replace=False)
    val_images = np.asarray([i for i in range(200) if i not in train_images])
    bmax_train, bmax_val = bmax_train[train_images], bmax_train[val_images]
    return bmax_train, bmax_val, train_images, val_images


def make_dirs(delete_old=False):
    """Create the relevant directories to save stuff in.

    Return nothing."""
    if delete_old:
        # This version deletes everything before regenerating.
        try:
            shutil.rmtree(const.BMAX_UNWRAPPED_DIR)
        except FileNotFoundError:
            pass
        os.mkdir(const.BMAX_UNWRAPPED_DIR)
        try:
            shutil.rmtree(const.BMAX_SPLIT_DIR)
        except FileNotFoundError:
            pass
        os.mkdir(const.BMAX_SPLIT_DIR)
    else:
        # This version only attempts to create the folders.
        try:
            os.mkdir(const.BMAX_UNWRAPPED_DIR)
        except FileExistsError:
            pass
        try:
            os.mkdir(const.BMAX_SPLIT_DIR)
        except FileExistsError:
            pass


def get_unwrappings_image_multithreaded(bmax_minibatch, num_points_rads, unwraps_labels_queue,
                                        use_train):
    """A wrapper of get_unwrappings_image for multithreading.

    Arg unwraps_labels_queue is a multiprocessing.Queue set up to receive 2-tuples of:
        -unwrappings
        -labels
    """
    for bmax_datum in bmax_minibatch:
        result = get_unwrappings_image(bmax_datum, num_points_rads, use_train)
        unwraps_labels_queue.put(result, block=True)


def get_unwrappings_image(bmax_datum, num_points_rads, use_train):
    """Given a BMAX500 image, grab a bunch of positive and negative unwrappings and return them.
    Return value is a tuple:
        -unwraps: as returned by utils.unwrap
        -labels: a numpy array of labels corresponding to each unwrap
    """
    ys, xs, rads, labels = gen_points_labels(bmax_datum, num_points_rads // 2, use_train)
    angles = np.zeros(labels.shape)
    img = utils.img_uint8_to_float32(bmax_datum['img'])
    unwraps = utils.unwrap(img, ys, xs, rads, angles)
    flipped_unwraps = FLIPPER.augment_images(unwraps)
    unwraps = np.concatenate((unwraps, flipped_unwraps), axis=0)
    labels = np.concatenate((labels, labels), axis=0)
    return unwraps, labels


def gen_points_labels(bmax_datum, num_points_rads, use_train):
    """Generate a dataset from the given image for use with a SVM or CNN.
    Input image is a single datum from BMAX500, including all of its annotations.
    Return value will be a tuple of numpy arrays. Every array will have the same
    length:
        -ys
        -xs
        -rads
        -angles
        -labels
    """
    if use_train:
        num_points_rads_pos = int(num_points_rads * const.BMAX_POS_FACTOR)
        num_points_rads_uni = int(num_points_rads * const.BMAX_UNI_FACTOR)
    else:
        num_points_rads_pos = int(num_points_rads * const.BMAX_VAL_POS_FACTOR)
        num_points_rads_uni = int(num_points_rads * const.BMAX_VAL_UNI_FACTOR)
    if num_points_rads_pos == 0:
        print('warning: num_points_rads_pos is 0')
    num_points_rads_neg = num_points_rads - num_points_rads_pos - num_points_rads_uni
    # each bmax image comes with several segmentations. Thus, a pixel may be identified as medial
    # under one segmentation but not another.
    if len(bmax_datum['seg'].shape) == 2:
        bmax_datum['bnd'] = np.expand_dims(bmax_datum['bnd'], 2)
        bmax_datum['pts'] = np.expand_dims(bmax_datum['pts'], 2)
        bmax_datum['rad'] = np.expand_dims(bmax_datum['rad'], 2)
        bmax_datum['seg'] = np.expand_dims(bmax_datum['seg'], 2)
        num_segs = 1
    else:
        assert len(bmax_datum['seg'].shape) == 3
        num_segs = bmax_datum['seg'].shape[2]
    h, w, *_ = bmax_datum['img'].shape
    # build a list of tuples of (y, x, radius) of all medial points
    all_points_rads_pos = []
    for seg in range(num_segs):
        medials = np.argwhere(bmax_datum['pts'][:, :, seg])
        img_rad = bmax_datum['rad'][:, :, seg]
        curr_points_rads = []
        for (y, x) in medials:
            rad = img_rad[y, x]
            if not (const.BMAX_MIN_RAD <= rad < const.BMAX_MAX_RAD):
                continue  # reject medials which are outside of our range of interest
            dist_to_nearest_edge = min((y, x, h - y, w - x))
            if dist_to_nearest_edge <= rad * const.UNWRAP_RADIUS_OUTER_FACTOR:
                # print('medial rejected:', (y, x, rad))
                pass  # reject medials which are too close to the edge
            else:
                curr_points_rads.append((y, x, rad))
        all_points_rads_pos.extend(curr_points_rads)
    # choose positive examples
    if num_points_rads_pos > len(all_points_rads_pos):
        print('error: not enough medials')
        assert False
    points_rads_pos = random.sample(all_points_rads_pos, num_points_rads_pos)
    # choose negative examples
    points_rads_uni, points_rads_neg = set(), set()
    num_outside_range, num_medials_rejected, num_edges_rejected = 0, 0, 0
    while len(points_rads_uni) < num_points_rads_uni or len(points_rads_neg) < num_points_rads_neg:
        # select a random point and make sure it's not medial
        y = random.randrange(h)
        x = random.randrange(w)
        # iterate over all segmentations and find the smallest distance to the nearest boundary
        smallest_dist = utils.distance_nearest_boundary(bmax_datum['bnd'], y, x)
        if len(points_rads_uni) < num_points_rads_uni:
            rad = smallest_dist
            neg_type = 'uni'
        else:
            while True:
                rad = random.randint(const.BMAX_MIN_RAD, const.BMAX_MAX_RAD)
                # I don't think we can simplify the following because
                if abs(smallest_dist - rad) > const.BMAX_NON_MEDIAL_RAD_THRESHOLD:
                    break
            neg_type = 'neg'
        if not (const.BMAX_MIN_RAD <= rad < const.BMAX_MAX_RAD):
            num_outside_range += 1
            continue
        dist_to_nearest_edge = min((y, x, h - y, w - x), default=0)
        if dist_to_nearest_edge <= rad * const.UNWRAP_RADIUS_OUTER_FACTOR:
            num_edges_rejected += 1
            continue
        curr_dist_nearest_medial = utils.dist_to_nearest_medial((y, x, rad), all_points_rads_pos)
        if curr_dist_nearest_medial <= const.BMAX_MEDIAL_DIST_THRESHOLD:
            num_medials_rejected += 1
            continue
        if neg_type == 'uni':
            points_rads_uni.add((y, x, rad))
        else:
            points_rads_neg.add((y, x, rad))

    points_rads_uni, points_rads_neg = list(points_rads_uni), list(points_rads_neg)
    # print('number of positive: {0}; number of negative unitangent: {1}; number of negative random: {2}'
    #       .format(num_points_rads_pos, num_points_rads_uni, num_points_rads_neg))
    # print('rejected {0} points for being outside of the radius range, {1} points for being too '
    #       'close to medials, {2} for being too close to edges'.
    #       format(num_outside_range, num_medials_rejected, num_edges_rejected))

    points_rads = np.asarray(points_rads_pos + points_rads_uni + points_rads_neg)
    labels = ([const.TRAIN_POS_LABEL for _ in range(num_points_rads_pos)]
              + [const.TRAIN_UNI_LABEL for _ in range(num_points_rads_uni)]
              + [const.TRAIN_NEG_LABEL for _ in range(num_points_rads_neg)])
    labels = np.asarray(labels)
    ys, xs, rads = points_rads[:, 0], points_rads[:, 1], points_rads[:, 2]
    # calculate the unwrappings at the specified number of angles
    assert ys.shape == xs.shape == rads.shape == labels.shape == (num_points_rads,)
    return ys, xs, rads, labels


def main():
    gen_datasets()


if __name__ == '__main__':
    multiprocessing.set_start_method('spawn')
    main()
