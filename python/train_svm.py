"""New script for training a liblinear SVM. Assumes you've already run
gen_dataset"""

import os
import pickle
# import shutil
# import statistics
import time

import hickle
import numpy as np
# from skimage import feature
import sklearn.linear_model
import sklearn.metrics
import sklearn.svm

from python import const
from python import load_unwrappings
# from python import process_synth
from python import train_cnn
from python import utils


def train_svm(loader_train, loader_val):
    """Train a SGD SVM on the unwrappings spat out by loader_train and validate on loader_val. Loaders are those from
    load_unwrappings, process_synth, and process_google"""
    # class_weight = {const.TRAIN_NEG_LABEL: 1000000., const.TRAIN_UNI_LABEL: 1000., const.TRAIN_POS_LABEL: 1.}
    class_weight = None
    n_epoch_no_change = 5
    n_iter_no_change = int(const.GLOBAL_TRAIN_SIZE * n_epoch_no_change * const.TRAIN_FIXED_CUTS_PER_PIXEL / const.TRAIN_BATCH_SIZE)
    print('patience of', n_iter_no_change)
    svm = sklearn.linear_model.SGDClassifier(
        fit_intercept=False, tol=1e-3, learning_rate='adaptive', eta0=0.0001,
        shuffle=False, n_jobs=1, class_weight=class_weight, n_iter_no_change=n_iter_no_change)
    scaler = loader_train.normalizer
    best_val_f1, best_epoch = -1, -1
    classes = np.asarray([const.TRAIN_POS_LABEL, const.TRAIN_UNI_LABEL, const.TRAIN_NEG_LABEL])
    for epoch in range(const.TRAIN_NUM_EPOCHS):
        # train
        t0 = time.time()
        labels_train, likelihoods_train, preds_train = [], [], []
        for hogs, labels, _ in loader_train:
            svm.partial_fit(hogs, labels, classes)
            labels_train.append(labels)
            preds = svm.predict(hogs)
            preds_train.append(preds)
            likelihoods = utils.svm_eval_likelihoods(svm, hogs)[:, const.TRAIN_POS_LABEL]
            likelihoods_train.append(likelihoods)
        labels_train, likelihoods_train, preds_train = \
            np.concatenate(labels_train), np.concatenate(likelihoods_train), np.concatenate(preds_train)
        # train_f1 = sklearn.metrics.f1_score(labels_train, likelihoods_train, labels=[const.TRAIN_POS_LABEL],
        #                                     average=None)[0]
        _, train_f1, _, _ = train_cnn.find_best_threshold(labels_train, likelihoods_train)
        del likelihoods_train
        t1 = time.time()

        # validate
        labels_val, likelihoods_val, preds_val = [], [], []
        assert const.EVAL_VOTING_NUM == 1
        # MEDIAL_COLUMN = 2
        for hogs, labels, _ in loader_val:
            labels_val.append(labels)
            likelihoods = utils.svm_eval_likelihoods(svm, hogs)[:, const.TRAIN_POS_LABEL]
            likelihoods_val.append(likelihoods)
            preds_val.append(svm.predict(hogs))
        labels_val, likelihoods_val, preds_val = \
            np.concatenate(labels_val), np.concatenate(likelihoods_val), np.concatenate(preds_val)
        threshold, f, precision, recall = train_cnn.find_best_threshold(labels_val, likelihoods_val)
        if f > best_val_f1:
            best_val_f1 = f
            best_epoch = epoch
            save_best(svm, scaler)
            print('new best val f1 {f1:.4f} at threshold {thresh:.3f}'.format(f1=f, thresh=threshold))
            print('training confusion matrix:')
            print(format_confusion(sklearn.metrics.confusion_matrix(labels_train, preds_train)))
            print('validation confusion matrix:')
            print(format_confusion(sklearn.metrics.confusion_matrix(labels_val, preds_val)))
        else:
            print('no improvement in {0} epochs. best is {1:.4f}'.format(epoch - best_epoch, best_val_f1))
        # precision = sklearn.metrics.precision_score(labels_val, preds_val, labels=[const.TRAIN_POS_LABEL],
        #                                     average=None)[0]
        # recall = sklearn.metrics.recall_score(labels_val, preds_val, labels=[const.TRAIN_POS_LABEL],
        #                                     average=None)[0]
        print('Val f1: {0:.4f}; threshold: {1:.3f}; prec: {2:.4f}; rec: {3:.4f}. Train f1: {4:.4f}'.
              format(f, threshold, precision, recall, train_f1))
        t2 = time.time()
        print('epoch {epoch:d} training took {train:.1f}s; val took {val:.1f}s'.
              format(epoch=epoch, train=t1 - t0, val=t2 - t1))
        if f >= 1.:
            print('training stops because the validation f1 score is perfect')
            break


def train_svm_no_sgd():
    """Train a linear SVM in one go. Currently only implemented for the synth dataset"""
    assert const.GLOBAL_TRAIN_ANGLES == 1
    with utils.Timer('loading hogs and labels'):
        # load hogs and labels
        # unwraps_train, labels_train = hickle.load(const.SYNTH_TRAIN[0][2])
        suffix = 'train_' + str(const.GLOBAL_TRAIN_SIZE)
        unwraps_train, labels_train = hickle.load(const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl'))
        num_unwraps_train = unwraps_train.shape[0]
        hogs_train = utils.unwraps_to_hogs(unwraps_train)
        del unwraps_train
        print('hog dimension:', hogs_train.shape[1])
        assert const.TRAIN_FIXED_CUTS_PER_PIXEL == 1
        assert hogs_train.shape[0] == num_unwraps_train * const.TRAIN_FIXED_CUTS_PER_PIXEL
        labels_train = np.repeat(labels_train, const.TRAIN_FIXED_CUTS_PER_PIXEL, axis=0)
        suffix = 'val_' + str(const.GLOBAL_VAL_SIZE)
        unwraps_val, labels_val = hickle.load(const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl'))
        hogs_val = utils.unwraps_to_hogs(unwraps_val)
        del unwraps_val  # ditto

    # scale the hogs
    if not const.GLOBAL_SYNTH and not const.GLOBAL_GOOGLE:
        with utils.Timer('scaling HOGs'):
            scaler = sklearn.preprocessing.StandardScaler()
            scaler.fit(hogs_train)
            # print(np.max(hogs_train), np.min(hogs_train))
            hogs_train, hogs_val = scaler.transform(hogs_train), scaler.transform(hogs_val)
            # print(np.max(hogs_train), np.min(hogs_train))
        print('after normalization, train set had mean: {0:.6f}; std: {1:.6f}'.format(np.mean(hogs_train), np.std(hogs_train)))
        print('after normalization, val set had mean: {0:.6f}; std: {1:.6f}'.format(np.mean(hogs_val), np.std(hogs_val)))
    else:
        scaler = None

    with utils.Timer('training'):
        # We shouldn't need to use fit_intercept because the hogs should be scaled
        svm = sklearn.svm.LinearSVC(fit_intercept=False,
                                    # 10000 is too low, but performance doesn't seem to get better anyway
                                    max_iter=1000,
                                    verbose=True,
                                    # class_weight='balanced',
                                    class_weight=None,
                                    )
        svm.fit(hogs_train, labels_train)

    with utils.Timer('validating'):
        responses_train, responses_val = utils.svm_eval_likelihoods(svm, hogs_train), utils.svm_eval_likelihoods(svm, hogs_val)
        del hogs_train, hogs_val
        # print(responses_train.shape)
        # print(responses_val.shape)
        outputs_train, outputs_val = np.argmax(responses_train, axis=1), np.argmax(responses_val, axis=1)
        del responses_train, responses_val
        print('training confusion matrix:')
        print(format_confusion(sklearn.metrics.confusion_matrix(labels_train, outputs_train)))
        print('validation confusion matrix:')
        print(format_confusion(sklearn.metrics.confusion_matrix(labels_val, outputs_val)))
        f1_train = sklearn.metrics.f1_score(labels_train, outputs_train, labels=[const.TRAIN_POS_LABEL], average=None)[0]
        f1_val = sklearn.metrics.f1_score(labels_val, outputs_val, labels=[const.TRAIN_POS_LABEL], average=None)[0]
        prec_val = sklearn.metrics.precision_score(labels_val, outputs_val, labels=[const.TRAIN_POS_LABEL], average=None)[0]
        rec_val = sklearn.metrics.recall_score(labels_val, outputs_val, labels=[const.TRAIN_POS_LABEL], average=None)[0]
        print('training f1: {0:.4f}; val f1: {1:.4f}; precision: {2:.4f}; recall: {3:.4f}'.
              format(f1_train, f1_val, prec_val, rec_val))
        filename = const.SVM_BEST_FILENAME.format(suffix='bmax')
        with open(const.SVM_BEST_FILENAME.format(suffix='bmax'), 'bw') as f:
            pickle.dump((svm, scaler), f)
        print('saved to', filename)


def format_confusion(confusion):
    """Returns a copy of the confusion matrix, formatted"""
    return confusion / np.sum(np.ravel(confusion))


def make_dir():
    """Make the directory we'll be saving the SVM in."""
    # Version that deletes everything beforehand
    # try:
    #     shutil.rmtree(const.SVM_DIR)
    # except FileNotFoundError:
    #     pass
    # os.mkdir(const.SVM_DIR)
    # Version that only makes the directory
    try:
        os.mkdir(const.SVM_DIR)
    except FileExistsError:
        pass


def save_best(svm, scaler):
    """Saves the SVM and HOG normalizer"""
    result = (svm, scaler, const.GLOBAL_TRAIN_SIZE, const.GLOBAL_VAL_SIZE)
    suffix = 'bmax'
    filename = const.SVM_BEST_FILENAME.format(suffix=suffix)
    with open(filename, 'wb') as f:
        pickle.dump(result, f)


def main():
    utils.seed_randoms()
    make_dir()
    shuffle_train = True
    shuffle_val = False
    process_unwraps_train = False
    process_labels_train = False
    process_unwraps_val = False
    process_labels_val = False
    process_unwraps_svm = True
    val_aug = False
    suffix = 'train_' + str(const.GLOBAL_TRAIN_SIZE)
    filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
    loader_train = load_unwrappings.UnwrappingsLoader(
        filename, True, const.TRAIN_BATCH_SIZE, const.TRAIN_FIXED_CUTS_PER_PIXEL,
        shuffle_train, process_unwraps_train, process_labels_train, const.GLOBAL_AUG,
        process_unwraps_svm
    )
    suffix = 'val_' + str(const.GLOBAL_VAL_SIZE)
    filename = const.BMAX_UNWRAPPED_FILENAME.format(suffix=suffix, ext='hkl')
    loader_val = load_unwrappings.UnwrappingsLoader(
        filename, const.GLOBAL_VAL_VOTE, const.TRAIN_BATCH_SIZE,
        const.TRAIN_FIXED_VOTES_PER_PIXEL, shuffle_val, process_unwraps_val, process_labels_val,
        val_aug, process_unwraps_svm, normalizer=loader_train.normalizer)
    train_svm(loader_train, loader_val)
    # train_svm_no_sgd()


if __name__ == '__main__':
    main()
