"""A GUI used to visualize a model"""

# from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.figure import Figure
from matplotlib import pyplot
import math
import numpy as np
import pickle
from PyQt5 import QtCore, QtGui, QtWidgets
import random
from skimage import draw
import sys
import time
import torch
from torch import nn

from python import const
from python import nn_models
from python import train_svm
from python import utils
from python.visualizer.designer import model_visualizer


class MainWindowVisualizer(QtWidgets.QMainWindow):

    def __init__(self):
        super(QtWidgets.QMainWindow, self).__init__()
        setter_upper = model_visualizer.Ui_MainWindow()
        setter_upper.setupUi(self)
        self.push_cnn = self.findChild(QtWidgets.QPushButton, 'pushCnn')
        self.push_svm = self.findChild(QtWidgets.QPushButton, 'pushSvm')
        self.push_seed = self.findChild(QtWidgets.QPushButton, 'pushSeed')
        self.line_rad_min = self.findChild(QtWidgets.QLineEdit, 'lineRadiusMin')
        self.line_rad_max = self.findChild(QtWidgets.QLineEdit, 'lineRadiusMax')
        self.line_image = self.findChild(QtWidgets.QLineEdit, 'lineImage')
        self.push_image = self.findChild(QtWidgets.QPushButton, 'pushRandomImage')
        self.push_medial = self.findChild(QtWidgets.QPushButton, 'pushRandomMedial')
        self.push_non_medial = self.findChild(QtWidgets.QPushButton, 'pushRandomNonMedial')
        self.line_angle = self.findChild(QtWidgets.QLineEdit, 'lineAngle')
        self.push_angle_fixed = self.findChild(QtWidgets.QPushButton, 'pushRandomFixedAngle')
        self.push_angle = self.findChild(QtWidgets.QPushButton, 'pushRandomAngle')
        self.line_x = self.findChild(QtWidgets.QLineEdit, 'lineX')
        self.line_y = self.findChild(QtWidgets.QLineEdit, 'lineY')
        self.line_threshold = self.findChild(QtWidgets.QLineEdit, 'lineThreshold')
        self.check_vote = self.findChild(QtWidgets.QCheckBox, 'checkVote')
        self.check_simplest_seg = self.findChild(QtWidgets.QCheckBox, 'checkSimplestSeg')
        self.label_image = self.findChild(QtWidgets.QLabel, 'labelImage')
        self.label_unwrap = self.findChild(QtWidgets.QLabel, 'labelUnwrap')
        self.label_unwrap_annot = self.findChild(QtWidgets.QLabel, 'labelUnwrapAnnotated')
        self.loaded_svm = None
        self.loaded_cnn = None
        all_children = [self.push_cnn, self.push_svm, self.push_seed, self.line_image,
                        self.push_image, self.push_medial, self.push_non_medial, self.line_angle,
                        self.push_angle_fixed, self.push_angle, self.line_x, self.line_y,
                        self.line_threshold, self.check_vote, self.check_simplest_seg,
                        self.label_image, self.label_unwrap, self.label_unwrap_annot]
        assert all(child is not None for child in all_children)
        self.bmax_test = utils.load_bmax_test()
        self.bmax_datum = None
        self.img = None  # the image to draw stuff on
        self.medial_set = None  # set of all medial points
        self.connect_signals()
        self.on_image_changed()

    def connect_signals(self):
        self.push_cnn.clicked.connect(self.load_cnn)
        self.push_svm.clicked.connect(self.load_svm)
        self.push_seed.clicked.connect(self.new_seed)
        self.push_image.clicked.connect(self.on_random_image)
        self.line_rad_min.editingFinished.connect(self.refresh)
        self.line_rad_max.editingFinished.connect(self.refresh)
        self.line_image.editingFinished.connect(self.on_image_changed)
        self.push_medial.clicked.connect(self.on_random_medial)
        self.push_non_medial.clicked.connect(self.on_random_non_medial)
        self.line_angle.editingFinished.connect(self.refresh)
        self.push_angle_fixed.clicked.connect(self.on_random_angle_fixed)
        self.push_angle.clicked.connect(self.on_random_angle)
        self.line_x.editingFinished.connect(self.refresh)
        self.line_y.editingFinished.connect(self.refresh)
        self.line_threshold.editingFinished.connect(self.refresh)
        self.check_vote.clicked.connect(self.refresh)
        self.check_simplest_seg.clicked.connect(self.calc_medials)
        self.check_simplest_seg.clicked.connect(self.refresh)
        # self.image_canvas.mpl_connect('button_press_event', self.on_img_clicked)

    @staticmethod
    def new_seed():
        """Reseed the PRNGs with a new seed"""
        np.random.seed(time.time())
        random.seed(time.time())
        torch.manual_seed(int(time.time() * 100))

    def angle(self):
        """Return the current angle, [0, 360) as a float or None if there's a problem"""
        try:
            result = float(self.line_angle.text())
            if not (0 <= result < 360):
                raise ValueError
            return result
        except ValueError:
            QtWidgets.QMessageBox.warning(self, 'error', 'must be a number from 0 (inclusive) to 360 (exclusive)')
            return None

    def coords(self):
        """Return a tuple of y, x based on the values in the current LineY, LineX"""
        try:
            return int(self.line_y.text()), int(self.line_x.text())
        except ValueError:
            QtWidgets.QMessageBox.warning(self, 'invalid', 'x, y must be an int')
            return None, None

    def set_coords(self, y, x):
        """Set the line edits with the given values of y and x (converted to int and then str)"""
        self.line_y.setText(str(int(y)))
        self.line_x.setText(str(int(x)))

    def threshold(self):
        """Return the threshold as a float from 0 to 1"""
        try:
            result = float(self.line_threshold.text())
            if not (0. <= result <= 100.):
                raise ValueError
            return result / 100.
        except ValueError:
            QtWidgets.QMessageBox.warning(self, 'invalid', 'threshold must be a number between 0 and 100')
            return None

    def use_voting(self):
        """Return whether the voting checkbox is checked"""
        return self.check_vote.isChecked()

    def on_image_changed(self, refresh=True):
        new_image = self.line_image.text()
        try:
            new_image = int(new_image)
            assert 0 <= new_image < 100
        except (ValueError, AssertionError):
            QtWidgets.QMessageBox.warning(self, 'invalid image number', 'invalid image number')
            return
        self.bmax_datum = self.bmax_test[new_image]
        self.calc_medials()
        if refresh:
            self.refresh()

    def calc_medials(self):
        num_segs = self.bmax_datum['seg'].shape[2]
        all_medials = set()
        show_simplest_seg = self.check_simplest_seg.checkState()
        if show_simplest_seg:
            min_num_pts, simplest_seg = math.inf, None
            for seg in range(num_segs):
                num_pts = np.sum(self.bmax_datum['pts'][:, :, seg])
                if num_pts < min_num_pts:
                    min_num_pts = num_pts
                    simplest_seg = seg
            assert simplest_seg is not None
            medials = np.argwhere(self.bmax_datum['pts'][:, :, simplest_seg])
            all_medials.update((y, x) for (y, x) in medials)
        else:
            for seg in range(num_segs):
                medials = np.argwhere(self.bmax_datum['pts'][:, :, seg])
                all_medials.update((y, x) for (y, x) in medials)
        self.medial_set = all_medials

    def on_img_clicked(self, event, refresh=True):
        """This function is called when the mouse is clicked on the image. Set the y and x coordinates and redraw.
        """
        y, x = event.ydata, event.xdata
        if y is None or x is None:
            return
        self.set_coords(y, x)
        if refresh:
            self.refresh()

    def on_random_image(self, *_, refresh=True, **__):
        """Choose a random image"""
        image = random.randrange(100)
        self.line_image.setText(str(image))
        self.on_image_changed(False)
        if refresh:
            self.refresh()

    def on_random_angle(self, *_, refresh=True, **__, ):
        """Called when "random angle" is clicked. Choose a random angle"""
        angle = random.random() * 360
        self.line_angle.setText(str('{0:.1f}'.format(angle)))
        if refresh:
            self.refresh()

    def on_random_angle_fixed(self, *_, refresh=True, **__, ):
        """Called when "random fixed angle" is clicked. Choose a random angle out of the possibilities allowed in
        const"""
        angle = np.random.choice(const.EVAL_VOTING_ANGLES, 1)[0]
        self.line_angle.setText(str('{0:.1f}'.format(angle)))
        if refresh:
            self.refresh()

    def on_random_medial(self, *_, refresh=True, **__):
        """Pick a random medial point and assign it to x, y"""
        y, x = random.sample(self.medial_set, 1)[0]
        seg = np.argwhere(self.bmax_datum['rad'][y, x, :])[0][0]
        radius = self.bmax_datum['rad'][y, x, seg]
        self.line_y.setText(str(y))
        self.line_x.setText(str(x))
        self.line_rad_min.setText(str(math.floor(radius * const.UNWRAP_RADIUS_INNER_FACTOR)))
        self.line_rad_max.setText(str(math.ceil(radius * const.UNWRAP_RADIUS_OUTER_FACTOR)))
        if refresh:
            self.refresh()

    def on_random_non_medial(self, *_, refresh=True, **__):
        """Pick a random non-medial point and assign it to x, y"""
        while True:
            h, w, _ = self.bmax_datum['img'].shape
            y, x = random.randrange(h), random.randrange(w)
            if (y, x) in self.medial_set:
                print('rejected:', y, x)
                continue
            break
        self.line_y.setText(str(y))
        self.line_x.setText(str(x))
        if refresh:
            self.refresh()

    def refresh(self):
        """Draw the annotations on self.img:
            -the currently selected point
            -the minimum radius around the point
            -the maximum radius around the point
            -the cut
            -medial points
            -the model's classification of the radii around the point (TODO)
        """
        img_orig = self.bmax_datum['img']
        h, w, c = shape = img_orig.shape
        self.img = img = np.copy(img_orig)
        y, x = self.coords()
        if y is None or x is None:
            return
        if y >= h:
            y = h - 1
        if x >= w:
            x = w - 1
        BOUNDARIES_COLOUR = (255, 0, 0)
        MEDIALS_COLOUR = (0, 0, 255)

        angle = self.angle()
        likelihoods, rads = self.draw_classifier_results(y, x, angle, shape)
        # medials
        num_segs = self.bmax_datum['seg'].shape[2]
        show_simplest_seg = self.check_simplest_seg.checkState()
        if show_simplest_seg:
            min_num_pts, simplest_seg = math.inf, None
            for seg in range(num_segs):
                num_pts = np.sum(self.bmax_datum['pts'][:, :, seg])
                if num_pts < min_num_pts:
                    min_num_pts = num_pts
                    simplest_seg = seg
            assert simplest_seg is not None
            rr, cc = np.nonzero(self.bmax_datum['pts'][:, :, simplest_seg])
            img[rr, cc, :] = MEDIALS_COLOUR
        else:
            for seg in range(num_segs):
                segmentation = self.bmax_datum['pts'][:, :, seg]
                rr, cc = np.nonzero(segmentation)
                img[rr, cc, :] = MEDIALS_COLOUR
        # point
        img[y, x] = BOUNDARIES_COLOUR
        # inner circle
        try:
            circle_min_rad = int(self.line_rad_min.text())
        except ValueError:
            circle_min_rad = const.EVAL_MIN_RAD - 1
        try:
            circle_max_rad = int(self.line_rad_max.text())
        except ValueError:
            circle_max_rad = const.EVAL_MAX_RAD + 1
        rr, cc = draw.circle_perimeter(y, x, circle_min_rad, shape=img_orig.shape)
        img[rr, cc, :] = BOUNDARIES_COLOUR
        # outer circle
        rr, cc = draw.circle_perimeter(y, x, circle_max_rad, shape=img_orig.shape)
        img[rr, cc, :] = BOUNDARIES_COLOUR
        # cut
        angle_radian = angle * math.pi / 180
        x_inner, y_inner = utils.pol2cart(circle_min_rad, angle_radian)
        x_outer, y_outer = utils.pol2cart(circle_max_rad, angle_radian)
        x_inner, y_inner = utils.round_int(x_inner + x), utils.round_int(y_inner + y)
        x_outer, y_outer = utils.round_int(x_outer + x), utils.round_int(y_outer + y)
        rr, cc = draw.line(y_inner, x_inner, y_outer, x_outer)
        rr, cc = self.clamp_to_shape(rr, cc, img_orig.shape)
        img[rr, cc, :] = BOUNDARIES_COLOUR

        # show everything
        self.label_image.setPixmap(np_img_to_qpixmap(self.img))
        self.draw_unwraps(y, x, angle, likelihoods, rads)

    def draw_unwraps(self, y, x, angle, likelihoods, rads):
        """Draw the unwraps"""
        # inner circle
        try:
            circle_min_rad = int(self.line_rad_min.text())
        except ValueError:
            circle_min_rad = const.EVAL_MIN_RAD
        try:
            circle_max_rad = int(self.line_rad_max.text())
        except ValueError:
            circle_max_rad = const.EVAL_MAX_RAD
        unwrap = utils.unwrap_specific_optimized(
            self.bmax_datum['img'], np.asarray((y,)), np.asarray((x,)), np.asarray((circle_min_rad,)),
            np.asarray((circle_max_rad,)), np.asarray((angle,)),
            # (circle_max_rad - circle_min_rad + 1, 360)
            const.UNWRAP_SHAPE
        )[0, :, :, :]
        unwrap = np.repeat(unwrap, 4, 0)
        unwrap = np.repeat(unwrap, 4, 1)
        pixmap = np_img_to_qpixmap(unwrap)
        print(pixmap.size())
        self.label_unwrap.setPixmap(pixmap)
        # self.label_unwrap.


        if likelihoods is not None and rads is not None:
            # draw the annotated choose_training_examples
            unwrap_annot = np.flip(unwrap.copy(), 0)
            SUB_THRESH_COLOUR = (255, 0, 0)
            SUPER_THRESH_COLOUR = (0, 255, 0)
            threshold = self.threshold()
            for likelihood, rad in zip(likelihoods, rads):
                height = rad - circle_min_rad
                if likelihood < threshold:
                    colour = SUB_THRESH_COLOUR
                else:
                    colour = SUPER_THRESH_COLOUR
                colour = tuple(channel * likelihood for channel in colour)
                unwrap_annot[height, :, :] = colour
            unwrap_annot = np.flip(unwrap_annot, 0)
            self.label_unwrap_annot.setPixmap(np_img_to_qpixmap(unwrap_annot))
        else:
            # self.label_unwrap_annot.setPixmap(np_img_to_qpixmap(unwrap))
            pass

    def load_cnn(self, refresh=True):
        """load a cnn and assign it to self.loaded_cnn. Assign self.loaded_svm to None"""
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self)
        if not filename:
            return

        if const.GLOBAL_USE_CUDA:
            map_location = 'cuda:0'
        else:
            map_location = 'cpu'
        try:
            state_dict, model_name, *_ = torch.load(filename, map_location)
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, 'Exception', str(e))
            return
        cnn = nn_models.get_model(model_name)
        cnn.load_state_dict(state_dict)
        cnn.eval()
        self.loaded_cnn = cnn
        self.loaded_svm = None
        if refresh:
            self.refresh()

    def load_svm(self, refresh=True):
        """load a svm and assign it to self.loaded_svm. Assign self.loaded_cnn to None"""
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(self)
        if not filename:
            return
        with open(filename, 'rb') as f:
            model = pickle.load(f)
        self.loaded_svm = model
        self.loaded_cnn = None
        if refresh:
            self.refresh()

    @staticmethod
    def clamp_to_shape(rr, cc, shape):
        """If rr and cc are numpy indexes to an array, clamp them to the given shape"""
        idxs = 0 <= rr
        rr = rr[idxs]
        cc = cc[idxs]
        idxs = rr < shape[0]
        rr = rr[idxs]
        cc = cc[idxs]
        idxs = 0 <= cc
        rr = rr[idxs]
        cc = cc[idxs]
        idxs = cc < shape[1]
        rr = rr[idxs]
        cc = cc[idxs]
        return rr, cc

    def draw_classifier_results(self, y, x, angle, shape):
        """Draw the results of the currently selected classifier (if any) on self.img, but DON'T update the image.

        Return the likelihoods and rads as a tuple."""
        if self.loaded_svm is None and self.loaded_cnn is None:
            return None, None
        assert self.loaded_cnn is None or self.loaded_svm is None  # only one selected

        likelihoods, rads = self.get_classifier_results(y, x, angle)

        SUB_THRESH_COLOUR = (255, 0, 0)
        SUPER_THRESH_COLOUR = (0, 255, 0)
        img = self.img
        threshold = self.threshold()
        for likelihood, rad in zip(likelihoods, rads):
            if likelihood < threshold:
                colour = SUB_THRESH_COLOUR
            else:
                colour = SUPER_THRESH_COLOUR
            rr, cc = draw.circle_perimeter(y, x, rad, shape=shape)
            colour = tuple(channel * likelihood for channel in colour)
            # img[rr, cc, :] = (1 - likelihood) * img[rr, cc, :] + colour
            img[rr, cc, :] = colour
        self.img = img
        # print('classifier likelihoods:', ' '.join(str(utils.round_int(x * 100)) for x in likelihoods))
        return likelihoods, rads

    def get_classifier_results(self, y, x, angle):
        """Get the classifier results for the given point. Behaviour depends on self.use_voting()"""
        rads = np.arange(const.EVAL_MIN_RAD, const.EVAL_MAX_RAD + 1, const.EVAL_RAD_STRIDE)
        num_rads = len(rads)
        ys = np.repeat(y, (num_rads,), axis=0)
        xs = np.repeat(x, (num_rads,), axis=0)
        if self.use_voting():
            raise NotImplementedError('voting not implemented anymore')
            with utils.Timer('unwrapping and shifting'):
                angles = np.zeros(rads.shape)
                idxs = np.arange(num_rads)  # used to keep track of which point-radius generated which unwrapping
                assert rads.shape == ys.shape == xs.shape == angles.shape == idxs.shape == (num_rads,)
                unwraps = utils.unwrap(self.bmax_datum['img'], ys, xs, rads, angles)

                unwraps = np.repeat(unwraps, (const.EVAL_VOTING_NUM,), axis=0)
                idxs = np.repeat(idxs, (const.EVAL_VOTING_NUM,), axis=0)
                angles = np.tile(const.EVAL_VOTING_NUM, (num_rads,))
                num_rads_angles = unwraps.shape[0]
                assert idxs.shape == angles.shape == (num_rads_angles,)
                unwraps = utils.shift_unwraps(unwraps, angles)

            with utils.Timer('evaluating'):
                if self.loaded_svm is not None:
                    raise NotImplementedError('SVM evaluation not implemented')
                    hogs = train_svm.unwraps_to_hogs(unwraps)
                    likelihoods = train_svm.eval_likelihoods(self.loaded_svm, hogs)
                else:  # use the CNN
                    unwraps = utils.unwraps_to_torch(unwraps)
                    if const.GLOBAL_USE_CUDA:
                        unwraps = unwraps.cuda()
                    unwraps.requires_grad_(False)
                    raw_outputs = self.loaded_cnn.forward(unwraps)
                    likelihoods = nn.functional.sigmoid(raw_outputs)
                    likelihoods = likelihoods.detach().cpu().numpy()
                    likelihoods = np.squeeze(likelihoods, 1)

            with utils.Timer('aggregating'):
                likelihoods_aggregated = np.zeros((num_rads,))
                for idx in range(num_rads):
                    likelihoods_rad = likelihoods[idxs == idx]
                    assert likelihoods_rad.shape == (const.EVAL_VOTING_NUM,)
                    likelihoods_aggregated[idx] = np.mean(likelihoods_rad)
                likelihoods = likelihoods_aggregated
                assert likelihoods.shape == (num_rads,)
        else:
            with utils.Timer('evaluation'):
                angles = np.ones(rads.shape) * angle
                assert rads.shape == ys.shape == xs.shape == angles.shape == (num_rads,)
                img = self.bmax_datum['img']
                unwraps = utils.unwrap(img, ys, xs, rads, angles)

                if self.loaded_svm is not None:
                    hogs = train_svm.unwraps_to_hogs(unwraps)
                    likelihoods = train_svm.eval_likelihoods(self.loaded_svm, hogs)
                else:  # use the CNN
                    unwraps = utils.unwraps_to_torch(unwraps)
                    if const.GLOBAL_USE_CUDA:
                        unwraps = unwraps.cuda()
                    unwraps.requires_grad_(False)
                    raw_outputs = self.loaded_cnn.forward(unwraps)
                    likelihoods = nn.functional.sigmoid(raw_outputs)
                    likelihoods = likelihoods.detach().cpu().numpy()
                    likelihoods = np.squeeze(likelihoods, 1)
        assert likelihoods.shape == rads.shape == (num_rads,)

        # determine ground truth for the given pixel. This is only for printing to stdout
        gt_rads = self.bmax_datum['rad'][y, x, :]
        print('ground truth:', [rad for rad in gt_rads if rad != 0])
        idxs = np.argsort(likelihoods)
        idxs = np.flip(idxs, 0)  # decreasing order
        sorted_rads = rads[idxs]
        sorted_likelihoods = likelihoods[idxs]
        print('radius sorted by decreasing likelihood:', ' '.
              join('({0}, {1:.8f})'.format(rad, like) for (rad, like) in zip(sorted_rads, sorted_likelihoods)))
        return likelihoods, rads


def np_img_to_qpixmap(img):
    """Return a QPixmap version of the given numpy image (h x w x c)."""
    h, w, c = img.shape
    assert c == 3
    # img = np.transpose(img, (1, 0, 2)).copy()
    img = np.require(img.copy(), np.uint8, 'C')
    # img = QtGui.QImage(img, w, h, QtGui.QImage.Format_RGB888)
    img = QtGui.QImage(img, w, h, w * 3, QtGui.QImage.Format_RGB888)
    # size = img.size()
    img = QtGui.QPixmap.fromImage(img)
    # size = img.size()
    return img


def main():
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindowVisualizer()
    main_window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
