function [odsP, odsR, odsF, odsT, oisP, oisR, oisF, AP] = evalConvnetBmax()
    [responses, gts] = loadConvnetResponses();
    addpath(genpath('matlab/spb-mil/'))
    % thresholds = 1 / 3;
%     numThresholds = 101;
%     thresholds = linspace(0, 1, numThresholds);
    thresholds = 0.01:0.01:0.99;
    numThresholds = size(thresholds, 2);
    
    numImages = size(responses, 1);
    stats_cntP = zeros(numImages, numThresholds);
    stats_sumP = zeros(numImages, numThresholds);
    stats_cntR = zeros(numImages, numThresholds);
    stats_sumR = zeros(numImages, numThresholds);
    options = struct('thresh', thresholds, 'visualize', false);
    options.maxDist = 0.01;
    for i = 1:numImages
        fprintf('started image %d\n', i)
        responsesImage = responses{i, 1};
        gtsImage = gts{i, 1};
%         imageDiag = norm(size(responsesImage));
%         assert(isscalar(imageDiag));
%         options.maxDist = 0.01 * imageDiag;
        [cntP, sumP, cntR, sumR, ~] = computeImageStats(responsesImage, gtsImage, options);
        stats_cntP(i, :) = cntP;
        stats_sumP(i, :) = sumP;
        stats_cntR(i, :) = cntR;
        stats_sumR(i, :) = sumR;
%         fprintf('after iteration %d, prec: %d, %d; rec: %d, %d', i, stats.cntP(i, 1), stats.sumP(i, 1), stats.cntR(i, 1), stats.sumR(i, 1))
    end
    assert(isequal(size(stats_cntP), size(stats_sumP), size(stats_cntR), size(stats_sumR), [numImages numThresholds]))
    stats = struct();
    stats.cntP = sum(stats_cntP, 1);
    stats.sumP = sum(stats_sumP, 1);
    stats.cntR = sum(stats_cntR, 1);
    stats.sumR = sum(stats_sumR, 1);
    assert(isequal(size(stats.cntP), size(stats.sumP), size(stats.cntR), size(stats.sumR), [1 numThresholds]))
    [odsP, odsR, odsF, odsT, oisP, oisR, oisF, AP] = computeDatasetStats(stats, options);
    odsP
    odsR
    odsF
    odsT
    AP
end


function [responses, gts] = loadConvnetResponses()
    % Returns: cell arrays of responses and ground truths for each image
    filename_in = 'data/BMAX500-symcomp17.mat';

    BMAX500 = load(filename_in);
    bmaxVal = BMAX500.BMAX500.val;

    numImages = size(bmaxVal, 2);
    assert(numImages == 100);
    responses = cell(numImages, 1);
    gts = cell(numImages, 1);
    for i = 1:numImages
%         filename_responses = sprintf('results/responses_bmax_convnet_iv3_c4_f32_fc512_%d/responses_medial.mat', i - 1);  % rotationally invariant convnet
        filename_responses = sprintf('results/responses_bmax_convnet_gen_c4_f32_fc512_%d/responses_medial.mat', i - 1);  % regular convnet
%         filename_responses = sprintf('results/responses_bmax_svm_%d/responses_medial.mat', i - 1);  % svm
        responsesImage = load(filename_responses);
        responsesImage = responsesImage.responses;
        responses{i, 1} = responsesImage;

        gtsImage = bmaxVal(1, i).pts;
        gts{i, 1} = gtsImage;
    end
end


% Copied from Stavros Tsogkas' spb-mil
% -------------------------------------------------------------------------
function [cntP,sumP,cntR,sumR,scores] = computeImageStats(pb,gt,opts)
% -------------------------------------------------------------------------
if size(pb,3) > 1
    [cntP,sumP,cntR,sumR,scores] = computeImageStatsHuman(gt,opts);
    return
end

% For levinshtein's method we do not need to threshold
if islogical(pb)
    thresh = 0.5; pb = double(pb);
else
    thresh = opts.thresh;
end

% Initialize
cntP = zeros(size(thresh));
sumP = zeros(size(thresh));
cntR = zeros(size(thresh));
sumR = zeros(size(thresh));

% Compute numerator (cntX) and denominator (sumX) for precision and recall.
% Note that because there are multiple groundtruth maps to compare with a
% single machine-generated response, the number of true positives, false
% positives etc. used for computing precision is different than the ones
% that are used for computing recall.
for t = 1:numel(thresh)
    % Threshold probability map and thin to 1-pixel width.
    bmap = (pb >= thresh(t));
    bmap = bwmorph(bmap,'thin',Inf);
    
    % Compute matches between symmetry map and all groundtruth maps
    accP = 0;
    for s=1:size(gt,3)
        gt(:,:,s) = bwmorph(gt(:,:,s), 'thin',inf);
        [match1,match2] = correspondPixels(double(bmap),double(gt(:,:,s)),opts.maxDist);
        if opts.visualize, plotMatch(1,bmap,gt(:,:,s),match1,match2); end
        % accumulate machine matches
        accP = accP | match1;
        cntR(t) = cntR(t) + nnz(match2>0); % tp (for recall)
    end
    cntP(t) = nnz(accP); % tp (for precision)
    sumP(t) = nnz(bmap); % tp + fp (for precision)
    sumR(t) = nnz(gt);   % tp + fn (for recall)
%     fprintf('finished threshold %d\n', t)
end

% Compute precision (P), recall (R) and f-measure (F). 
P = cntP ./ max(eps, sumP);
R = cntR ./ max(eps, sumR);

% Use linear interpolation to find best P,R,F combination.
% scores contains the image-specific optimal P,R,F, after computing optimal
% thresholds using linear interpolation.
[bestP,bestR,bestF,bestT] = findBestPRF(P,R,thresh);
scores = [bestP,bestR,bestF,bestT];
end


% -------------------------------------------------------------------------
function [bestP,bestR,bestF,bestT] = findBestPRF(P,R,T)
% -------------------------------------------------------------------------
if numel(T) == 1
    bestT = T; bestR = R; bestP = P; bestF = fmeasure(P,R); return
end

bestF = -1;
a = linspace(0,1,100); b = 1-a;
for t = 2:numel(T)
    Rt = a.*R(t) + b.*R(t-1);
    Pt = a.*P(t) + b.*P(t-1);
    Tt = a.*T(t) + b.*T(t-1);
    Ft = fmeasure(Pt,Rt);
    [f,indMax] = max(Ft); 
    if f > bestF
        bestF = f; bestT = Tt(indMax);
        bestP = Pt(indMax); bestR = Rt(indMax); 
    end
end
end


% -------------------------------------------------------------------------
function [odsP, odsR, odsF, odsT, oisP, oisR, oisF, AP] = computeDatasetStats(stats,opts)
% Two standard F-based performance metrics are computed:
% i)  ODS: F-measure for a dataset-wide specified optimal threshold.
% ii) OIS: F-measure for an image-specific optimal threshold.
% iii)AP:  Average precision - equivalent to AUC (area under curve).

P = sum(stats.cntP,1) ./ max(eps, sum(stats.sumP,1));
R = sum(stats.cntR,1) ./ max(eps, sum(stats.sumR,1));
stairs(R, P)

% David: get rid of duplicate precisions
[P, indexes, ~] = unique(P, 'stable');
R = R(indexes);
opts.thresh = opts.thresh(indexes);

if length(P) > 1 % soft probability maps
    % ODS scores (scalars)
    [odsP,odsR,odsF,odsT] = findBestPRF(P,R,opts.thresh);

    % OIS scores (scalars)
    Pi = stats.cntP ./ max(eps, stats.sumP);
    Ri = stats.cntR ./ max(eps, stats.sumR);
    [Pi, indexes, ~] = unique(Pi, 'stable');
    Ri = Ri(indexes);
    [~,indMaxF] = max(fmeasure(Pi,Ri),[],2);
    indMaxF = sub2ind(size(Pi), (1:size(Pi,1))', indMaxF);
    oisP = sum(stats.cntP(indMaxF)) ./ max(eps, sum(stats.sumP(indMaxF)));
    oisR = sum(stats.cntR(indMaxF)) ./ max(eps, sum(stats.sumR(indMaxF)));
    oisF = fmeasure(oisP,oisR);

    % AP score (scalar)
    AP = interp1(R,P, 0:0.01:1); 
    AP = sum(AP(~isnan(AP)))/100;
else    % binary symmetry maps
    odsP = P; odsR = R; odsF = fmeasure(P,R); odsT = 0.5;
    oisP = odsP; oisR = odsR; oisF = odsF; AP = odsP;
end
end


% -------------------------------------------------------------------------
function F = fmeasure(P,R), F = 2 .* P .* R ./ max(eps, P+R);
end
