tic;
addpath(genpath('L0smoothing'))

filename_in = 'data/BMAX500-symcomp17.mat';
filename_out = 'data/bmax_smoothed.mat';

BMAX500 = load(filename_in);
BMAX500 = BMAX500.BMAX500;

% set = [BMAX500.train, BMAX500.val, BMAX500.test];
set = [BMAX500.train, BMAX500.val];
% for i=1:size(set, 2)
parfor i=1:size(set, 2)
    fprintf('working on image %d\n', i);
    img = set(1, i).img;
    img = L0Smoothing(img);
    % clamp img to [0, 1]
    img = max(0, min(1, img));
    img = img * 255.;
    img = uint8(img);
    set(1, i).img = img;
    assert(any(img(:) > 1));
end
BMAX500.train = set(1, 1:200);
BMAX500.val = set(1, 201:300);
% BMAX500.test = set(1, 301:500);
save(filename_out, 'BMAX500')
toc;
