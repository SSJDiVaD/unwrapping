tic;
addpath(genpath('L0smoothing'))

filename_in = 'data/BMAX500-symcomp17.mat';
filename_out = 'data/bmax_smoothed_one_seg.mat';

BMAX500 = load(filename_in);
BMAX500 = BMAX500.BMAX500;

% set = [BMAX500.train, BMAX500.val, BMAX500.test];
set = [BMAX500.train, BMAX500.val];
parfor i=1:size(set, 2)
    i
    % Smooth image
    img = set(1, i).img;
    img = L0Smoothing(img);
    % clamp img to [0, 1]
    img = max(0, min(1, img));
    img = img * 255;
    img = uint8(img);
    set(1, i).img = img;
    assert(any(img(:) > 1));
    
    % Get rid of all segs but the simplest one for each image
    pts = set(1, i).seg;
    [h, w, num_segs] = size(pts);
%     num_medials = zeros(num_segs, 1);
%     for j = 1:num_segs
%         curr_pts = pts(:, :, j);
%         num_medials(j, 1) = sum(curr_pts(:));
%     end
    num_medials = squeeze(sum(sum(pts, 2), 1));
    assert(isequal(size(num_medials), [num_segs, 1]));
    [dummy, simplest_seg] = min(num_medials);
    assert((1 <= simplest_seg) && (simplest_seg <= num_segs));
    set(1, i).pts = pts(:, :, simplest_seg);
    set(1, i).rad = set(1, i).rad(:, :, simplest_seg);
    set(1, i).seg = set(1, i).seg(:, :, simplest_seg);
    set(1, i).bnd = set(1, i).bnd(:, :, simplest_seg);
end
BMAX500.train = set(1, 1:200);
BMAX500.val = set(1, 201:300);
% BMAX500.test = set(1, 301:500);
save(filename_out, 'BMAX500')
toc;
